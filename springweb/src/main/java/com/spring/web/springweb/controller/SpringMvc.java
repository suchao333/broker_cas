﻿package com.spring.web.springweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class SpringMvc {

    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    public ModelAndView saveUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mv = new ModelAndView("/user/save/result");//默认为forward模式
//      ModelAndView mv = new ModelAndView("redirect:/user/save/result");//redirect模式
        mv.addObject("message","保存用户成功！");
        return mv;
    }
}
