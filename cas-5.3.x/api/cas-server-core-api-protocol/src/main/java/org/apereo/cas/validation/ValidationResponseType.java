package org.apereo.cas.validation;

/**
 * Enumerates the list of response types
 * that CAS may produce as a result of
 * service being validated.
 * *枚举响应类型列表
 * * CAS可能产生的结果
 * *正在验证服务。
 * @author Misagh Moayyed
 * @since 4.2
 */
public enum ValidationResponseType {
    /**
     * Default CAS XML response.
     */
    XML,
    /**
     * Render response in JSON.
     */
    JSON
}
