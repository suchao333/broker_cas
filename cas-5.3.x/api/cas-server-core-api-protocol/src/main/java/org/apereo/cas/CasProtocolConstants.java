package org.apereo.cas;

/**
 * Class that exposes relevant constants and parameters to
 * the CAS protocol. These include attribute names, pre-defined
 * values and expected request parameter names as is specified
 * by the protocol.
 * 该类将相关的常量和参数公开给
 * *核证机关协议。这些包括预定义的属性名称
 * *指定的值和期望的请求参数名
 * *根据议定书。
 *
 * @author Misagh Moayyed
 * @since 4.1.0
 */
public interface CasProtocolConstants {

    /* CAS Protocol Validation Fields. **/

    /**
     * Constant representing the PGT in the cas validation model.
     * 表示cas验证模型中PGT的常数。
     */
    String VALIDATION_CAS_MODEL_PROXY_GRANTING_TICKET = "proxyGrantingTicket";

    /**
     * Constant representing the PGTIOU in the cas validation model.
     *常数，表示cas验证模型中的PGTIOU。
     */
    String VALIDATION_CAS_MODEL_PROXY_GRANTING_TICKET_IOU = "pgtIou";

    /**
     * Constant representing the remember-me long term token in the validation payload.
     * 表示验证有效负载中记忆长期令牌的常量。
     */
    String VALIDATION_REMEMBER_ME_ATTRIBUTE_NAME = "longTermAuthenticationRequestTokenUsed";

    /**
     * Represents the collection of attributes in the view.
     * 表示视图中的属性集合。
     */
    String VALIDATION_CAS_MODEL_ATTRIBUTE_NAME_ATTRIBUTES = "attributes";

    /**
     * Represents the pre-rendered collection of attributes in the view.
     * 表示视图中预先呈现的属性集合。
     */
    String VALIDATION_CAS_MODEL_ATTRIBUTE_NAME_FORMATTED_ATTRIBUTES = "formattedAttributes";

    /**
     * Represents the authentication date object in the view.
     * 表示视图中的身份验证日期对象。
     */
    String VALIDATION_CAS_MODEL_ATTRIBUTE_NAME_AUTHENTICATION_DATE = "authenticationDate";

    /**
     * Represents the flag to note whether assertion is backed by new login.
     * 表示标记，用于说明新登录是否支持断言。
     */
    String VALIDATION_CAS_MODEL_ATTRIBUTE_NAME_FROM_NEW_LOGIN = "isFromNewLogin";

    /* CAS Protocol Parameters. **/

    /**
     * Constant representing the proxy callback url parameter in the request.
     * 常量，表示请求中的代理回调url参数。
     */
    String PARAMETER_PROXY_CALLBACK_URL = "pgtUrl";

    /**
     * Constant representing the renew parameter in the request.
     * 常量，表示请求中的更新参数。
     */
    String PARAMETER_RENEW = "renew";

    /**
     * Constant representing the gateway parameter in the request.
     * 常量，表示请求中的网关参数。
     */
    String PARAMETER_GATEWAY = "gateway";

    /**
     * Constant representing the service parameter in the request.
     * 常量，表示请求中的服务参数。
     */
    String PARAMETER_SERVICE = "service";

    /**
     * Constant representing the ticket parameter in the request.
     * 常量，表示请求中的票据参数。
     */
    String PARAMETER_TICKET = "ticket";

    /**
     * Constant representing the format parameter in the request.
     * 常量，表示请求中的格式参数。
     */
    String PARAMETER_FORMAT = "format";

    /**
     * Constant representing the targetService parameter in the request.
     * 常量，表示请求中的targetService参数。
     */
    String PARAMETER_TARGET_SERVICE = "targetService";

    /**
     * Constant representing the method parameter in the request.
     * 常量，表示请求中的方法参数。
     */
    String PARAMETER_METHOD = "method";

    /**
     * Constant representing the pgtId parameter in the request.
     * 常量，表示请求中的pgtId参数。
     */
    String PARAMETER_PROXY_GRANTING_TICKET_ID = "pgtId";

    /**
     * Constant representing the pgt parameter in the request.
     * 常数，表示请求中的pgt参数。
     */
    String PARAMETER_PROXY_GRANTING_TICKET = "pgt";

    /**
     * Constant representing the pgtIou parameter in the request.
     * 常量，表示请求中的pgtIou参数。
     */
    String PARAMETER_PROXY_GRANTING_TICKET_IOU = "pgtIou";

    /**
     * Constant representing the pgtUrl parameter in the request.
     * 常量，表示请求中的pgtUrl参数。
     */
    String PARAMETER_PROXY_GRANTING_TICKET_URL = "pgtUrl";
    
    /* CAS Protocol Error Codes. **/

    /**
     * Constant representing an invalid request for validation.
     * 常量，表示无效的验证请求。
     */
    String ERROR_CODE_INVALID_REQUEST = "INVALID_REQUEST";

    /**
     * Constant representing an invalid proxy callback for validation.
     * 常量，表示用于验证的无效代理回调。
     */
    String ERROR_CODE_INVALID_PROXY_CALLBACK = "INVALID_PROXY_CALLBACK";

    /**
     * Constant representing an invalid ticket for validation.
     * 常量，表示用于验证的无效票据。
     */
    String ERROR_CODE_INVALID_TICKET = "INVALID_TICKET";

    /**
     * Constant representing an invalid pgt request.
     * 常量，表示无效的pgt请求。
     */
    String ERROR_CODE_INVALID_REQUEST_PROXY = "INVALID_REQUEST_PROXY";

    /**
     * Constant representing an invalid service request.
     * 常量，表示无效的服务请求。
     */
    String ERROR_CODE_UNAUTHORIZED_SERVICE = "UNAUTHORIZED_SERVICE";

    /**
     * Constant representing an invalid service proxy request.
     * 常量，表示无效的服务代理请求。
     */
    String ERROR_CODE_UNAUTHORIZED_SERVICE_PROXY = "UNAUTHORIZED_SERVICE_PROXY";

    /**
     * Constant representing login.
     * 常数代表登录。
     */
    String ENDPOINT_LOGIN = "/login";

    /**
     * Constant representing logout.
     * 常数代表注销。
     */
    String ENDPOINT_LOGOUT = "/logout";
    
    /**
     * Constant representing proxy validate.
     * 表示代理验证的常量。
     */
    String ENDPOINT_PROXY_VALIDATE = "/proxyValidate";

    /**
     * Constant representing v3 proxy validate.
     * 表示v3代理验证的常量。
     */
    String ENDPOINT_PROXY_VALIDATE_V3 = "/p3/proxyValidate";

    /**
     * Constant representing legacy validate.
     * 表示遗留验证的常量。
     */
    String ENDPOINT_VALIDATE = "/validate";

    /**
     * Constant representing service validate.
     * 常量表示服务验证。
     */
    String ENDPOINT_SERVICE_VALIDATE = "/serviceValidate";

    /**
     * Constant representing v3 service validate.
     * 表示v3服务验证的常量。
     */
    String ENDPOINT_SERVICE_VALIDATE_V3 = "/p3/serviceValidate";

}
