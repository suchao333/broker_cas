package org.apereo.cas.web.support;

import javax.servlet.http.HttpServletRequest;

import org.apereo.cas.authentication.principal.ServiceFactory;
import org.apereo.cas.authentication.principal.WebApplicationService;

import java.util.List;

/**
 * Strategy interface for retrieving services from the request.
 * 用于从请求中检索服务的策略接口。
 * @author Scott Battatglia
 * @since 3.1
 */
public interface ArgumentExtractor {
    /**
     * Retrieve the service from the request.
     * 从请求中检索服务。
     * @param request the request context.
     * @return the fully formed Service or null if it could not be found.
     */
    WebApplicationService extractService(HttpServletRequest request);

    /**
     * Gets service factories.
     * 被服务的工厂。
     * @return the service factories
     */
    List<ServiceFactory<? extends WebApplicationService>> getServiceFactories();
}
