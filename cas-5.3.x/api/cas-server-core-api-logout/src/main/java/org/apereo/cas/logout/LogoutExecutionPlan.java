package org.apereo.cas.logout;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This is {@link LogoutExecutionPlan} that describes how various CAS modules
 * must respond to the CAS logout events. A simple example of this may be OAuth
 * or OpenID Connect where Access Tokens and Refresh Tokens may need to be cleaned up
 * once the associated TGT is perhaps removed.
 **这是{@link LogoutExecutionPlan}，它描述了不同的CAS模块
 *
 * *必须响应CAS注销事件。一个简单的例子就是OAuth
 *
 * *或OpenID Connect，其中可能需要清理访问令牌和刷新令牌
 *
 * *一旦相关的TGT被移除。
 * @author Misagh Moayyed
 * @since 5.1.0
 */
public interface LogoutExecutionPlan {

    /**
     * Register logout handler.
     *
     * @param handler the handler
     */
    default void registerLogoutHandler(final LogoutHandler handler) {
    }

    /**
     * Gets logout handlers.
     *
     * @return the logout handlers
     */
    default Collection<LogoutHandler> getLogoutHandlers() {
        return new ArrayList<>(0);
    }
}
