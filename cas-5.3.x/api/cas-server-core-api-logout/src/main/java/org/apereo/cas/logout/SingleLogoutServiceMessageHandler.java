package org.apereo.cas.logout;

import org.apereo.cas.authentication.principal.WebApplicationService;

import java.util.Collection;

/**
 * This is {@link SingleLogoutServiceMessageHandler} which defines how a logout message
 * for a service that supports SLO should be handled.
 *这是{@link SingleLogoutServiceMessageHandler}，它定义了注销消息的方式
 *对于一个支持SLO的服务应该被处理。
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@FunctionalInterface
public interface SingleLogoutServiceMessageHandler {

    /**
     * Handle logout for slo service.
     *
     * @param singleLogoutService the service
     * @param ticketId the ticket id
     * @return the logout request
     */
    Collection<LogoutRequest> handle(WebApplicationService singleLogoutService, String ticketId);
}
