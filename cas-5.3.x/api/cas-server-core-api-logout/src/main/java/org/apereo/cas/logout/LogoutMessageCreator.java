package org.apereo.cas.logout;

/**
 * Contract that defines the format of the logout message sent to a client to indicate
 * that an SSO session has terminated.
 * *定义发送到客户端以指示的注销消息格式的协定
 * *SSO会话已终止。
 * @author Misagh Moayyed
 * @since 4.0.0
 */
@FunctionalInterface
public interface LogoutMessageCreator {
    /**
     * Builds the logout message to be sent.
     *
     * @param request the request
     * @return the message. Message may or may not be encoded.
     */
    String create(LogoutRequest request);
}
