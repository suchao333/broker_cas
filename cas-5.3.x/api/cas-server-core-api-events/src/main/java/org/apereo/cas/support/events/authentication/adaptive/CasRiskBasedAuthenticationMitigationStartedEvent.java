package org.apereo.cas.support.events.authentication.adaptive;

import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.authentication.Authentication;
import org.apereo.cas.services.RegisteredService;
import org.apereo.cas.support.events.AbstractCasEvent;

/**
 * This is {@link CasRiskBasedAuthenticationMitigationStartedEvent}.
 * 基于Cas风险的身份验证缓解启动事件
 * Mitigation（缓和缓解）
 * @author Misagh Moayyed
 * @since 5.1.0
 */
@Slf4j
public class CasRiskBasedAuthenticationMitigationStartedEvent extends AbstractCasEvent {

    private static final long serialVersionUID = 123568299766263298L;

    private final Authentication authentication;
    private final RegisteredService service;
    private final Object score;

    /**
     * Instantiates a new Cas risk based authentication mitigation started event.
     * 实例化一个新的基于Cas风险的身份验证缓解启动事件。
     * @param source         the source
     * @param authentication the authentication
     * @param service        the service
     * @param score          the score
     */
    public CasRiskBasedAuthenticationMitigationStartedEvent(final Object source, final Authentication authentication, 
                                                            final RegisteredService service, final Object score) {
        super(source);
        this.authentication = authentication;
        this.service = service;
        this.score = score;
    }

    public Authentication getAuthentication() {
        return authentication;
    }

    public RegisteredService getService() {
        return service;
    }

    public Object getScore() {
        return score;
    }
}
