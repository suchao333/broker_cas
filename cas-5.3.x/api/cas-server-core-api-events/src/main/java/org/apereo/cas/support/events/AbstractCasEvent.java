package org.apereo.cas.support.events;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.authentication.Authentication;
import org.springframework.context.ApplicationEvent;

/**
 * Base Spring {@code ApplicationEvent} representing a abstract single sign on action executed within running CAS server.
 * This event encapsulates {@link Authentication} that is associated with an SSO action
 * executed in a CAS server and an SSO session
 * token in the form of ticket granting ticket id.
 * More concrete events are expected to subclass this abstract type.
 **basespring{@code ApplicationEvent}表示在运行的CAS服务器中执行的抽象单点登录操作。
 *
 * *此事件封装与SSO操作关联的{@link Authentication}
 *
 * *在CAS服务器和SSO会话中执行
 *
 * *票证授予票证id形式的令牌。
 *
 * *更具体的事件应该是这个抽象类型的子类。
 * @author Dmitriy Kopylenko
 * @since 4.2
 */
@Slf4j
@ToString
public abstract class AbstractCasEvent extends ApplicationEvent {

    private static final long serialVersionUID = 8059647975948452375L;

    /**
     * Instantiates a new Abstract cas sso event.
     *
     * @param source the source
     */
    public AbstractCasEvent(final Object source) {
        super(source);
    }
}
