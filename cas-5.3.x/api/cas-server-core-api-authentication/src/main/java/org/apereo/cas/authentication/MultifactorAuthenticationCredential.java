package org.apereo.cas.authentication;

/**
 * Interface for credentials created by MFA providers to match up which provider created them.
 * MFA提供程序创建的凭据的接口，以匹配创建它们的提供程序。类似将军令
 * @author Travis Schmidt
 * @since 6.0
 */
public interface MultifactorAuthenticationCredential {

    /**
     * Returns the unique id of the provider that created the credential.
     * 返回创建凭据的提供程序的唯一id。
     * @return - the mark
     */
    default String getProviderId() {
        return null;
    }

    /**
     * Sets the unique mark of the provider that created the credential.
     * 设置创建凭据的提供程序的唯一标记。
     * @param providerId - the id
     */
    default void setProviderId(final String providerId) {
        // sets provider id
    }

}
