package org.apereo.cas.authentication.principal;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.Map;

/**
 * Defines operations required for retrieving principal attributes.
 * Acts as a proxy between the external attribute source and CAS,
 * executing such as additional processing or caching on the set
 * of retrieved attributes. Implementations may simply decide to
 * do nothing on the set of attributes that the principal carries
 * or they may attempt to refresh them from the source, etc.
 * 定义检索主体属性所需的操作。
 * 充当外部属性源和CAS之间的代理，
 * 在集合上执行额外的处理或缓存
 * 检索的属性。实现可以简单地决定
 * 对主体携带的属性集不执行任何操作
 * 或者他们可能会尝试从源刷新它们，等等。
 * @author Misagh Moayyed
 * @see PrincipalFactory
 * @since 4.1
 */
@FunctionalInterface
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface PrincipalAttributesRepository extends Serializable {

    /**
     * Gets attributes for the given principal id.
     *
     * @param p the principal whose attributes need to be retrieved.
     * @return the attributes
     */
    Map<String, Object> getAttributes(Principal p);
}
