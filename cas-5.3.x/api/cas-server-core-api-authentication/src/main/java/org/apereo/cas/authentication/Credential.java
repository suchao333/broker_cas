package org.apereo.cas.authentication;

import java.io.Serializable;

/**
 * Describes an authentication credential. Implementations SHOULD also implement {@link CredentialMetaData} if
 * no sensitive data is contained in the credential; conversely, implementations MUST NOT implement
 * {@link CredentialMetaData} if the credential contains sensitive data (e.g. password, key material).
 **描述身份验证凭据。实现还应该实现{@link CredentialMetaData}if
 *
 * *凭证中不包含敏感数据；相反，实现不能实现
 *
 * *{@link CredentialMetaData}如果凭证包含敏感数据（例如密码、密钥材料）。
 * @author William G. Thompson, Jr.
 * @author Marvin S. Addison
 * @see CredentialMetaData
 * @since 3.0.0
 */
@FunctionalInterface
public interface Credential extends Serializable {
    /** Credential type, collected as metadata for authentication.
     * 凭据类型，收集为身份验证的元数据。
     * */
    String CREDENTIAL_TYPE_ATTRIBUTE = "credentialType";

    /** An ID that may be used to indicate the credential identifier is unknown.
     * 可用于指示凭据标识符的ID未知。*/
    String UNKNOWN_ID = "unknown";

    /**
     * Gets a credential identifier that is safe to record for logging, auditing, or presentation to the user.
     * In most cases this has a natural meaning for most credential types (e.g. username, certificate DN), while
     * for others it may be awkward to construct a meaningful identifier. In any case credentials require some means
     * of identification for a number of cases and implementers should make a best effor to satisfy that need.
     * *获取可安全记录以供记录、审核或向用户演示的凭据标识符。
     *
     * *在大多数情况下，这对大多数凭证类型（例如用户名、证书DN）都有自然含义，而
     *
     * *对于其他人来说，构造一个有意义的标识符可能会很尴尬。在任何情况下，证书都需要某种手段
     *
     * *对于一些案例的识别和实施者应该尽最大努力满足这一需求。
     * @return Non-null credential identifier. Implementers should return {@link #UNKNOWN_ID} for cases where an ID
     * is not readily available or meaningful.
     * 非空凭据标识符。对于ID为
     *
     * 没有现成的或有意义的。
     */
    String getId();
}
