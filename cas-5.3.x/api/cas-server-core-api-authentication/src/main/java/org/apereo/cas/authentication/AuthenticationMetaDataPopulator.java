package org.apereo.cas.authentication;

import org.springframework.core.Ordered;

/**
 * An extension point to the Authentication process that allows CAS to provide
 * additional attributes related to the overall Authentication (such as
 * authentication type) that are specific to the Authentication request versus
 * the Principal itself.
 * *身份验证过程的扩展点，允许CAS提供
 *
 * *与整个身份验证相关的附加属性（例如
 *
 * *验证类型）特定于身份验证请求的
 *
 * *Principal本身。
 * 身份验证元数据填充器
 * @author Scott Battaglia
 * @author Marvin S. Addison
 * @since 3.0.0
 */
public interface AuthenticationMetaDataPopulator extends Ordered {

    /**
     * Adds authentication metadata attributes on successful authentication of the given credential.
     * 在成功验证给定凭据时添加身份验证元数据属性。
     * @param builder     Builder object that temporarily holds authentication metadata.
     * @param transaction The authentication transaction.
     */
    void populateAttributes(AuthenticationBuilder builder, AuthenticationTransaction transaction);

    /**
     * Determines whether the populator has the capability to perform tasks on the given credential.
     * In practice, the {@link #populateAttributes(AuthenticationBuilder, AuthenticationTransaction)} needs to be able
     * to operate on said credentials only if the return result here is {@code true}.
     **确定填充器是否有能力在给定凭据上执行任务。
     *
     * *实际上，{@link#populateAttributes（AuthenticationBuilder，AuthenticationTransaction）}需要能够
     *
     * *仅在返回结果为{@codetrue}时对所述凭证进行操作。
     * @param credential The credential to check.
     * @return True if populator supports the Credential, false otherwise.
     * @since 4.1.0
     */
    boolean supports(Credential credential);

    @Override
    default int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
