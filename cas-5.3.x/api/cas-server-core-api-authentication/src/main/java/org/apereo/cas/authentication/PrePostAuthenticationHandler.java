package org.apereo.cas.authentication;

/**
 * This is {@link PrePostAuthenticationHandler}.
 *
 * @author Misagh Moayyed
 * @since 5.1.0
 */
@FunctionalInterface
public interface PrePostAuthenticationHandler extends AuthenticationHandler {

    /**
     * Template method to perform arbitrary pre-authentication actions.
     *执行任意预身份验证操作的模板方法。
     * @param credential the Credential supplied
     * @return true if authentication should continue, false otherwise.
     */
    default boolean preAuthenticate(final Credential credential) {
        return true;
    }

    /**
     * Template method to perform arbitrary post-authentication actions.
     * 执行任意身份验证后操作的模板方法。
     * @param credential the supplied credential
     * @param result     the result of the authentication attempt.
     * @return An authentication handler result that MAY be different or modified from that provided.
     */
    default AuthenticationHandlerExecutionResult postAuthenticate(final Credential credential, final AuthenticationHandlerExecutionResult result) {
        return result;
    }
}
