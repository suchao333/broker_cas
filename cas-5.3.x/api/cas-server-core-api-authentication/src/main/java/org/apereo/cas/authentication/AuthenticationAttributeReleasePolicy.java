package org.apereo.cas.authentication;

import java.util.Map;

/**
 * This component is used to handle release of authentication attributes in validation responses.
 *此组件用于处理验证响应中身份验证属性的发布。
 * @author Daniel Frett
 * @since 5.2.0
 */
@FunctionalInterface
public interface AuthenticationAttributeReleasePolicy {
    /**
     * This method will return the Authentication attributes that should be released.
     *此方法将返回应发布的身份验证属性。
     * @param authentication The authentication object we are processing.
     * @return The attributes to be released
     */
    Map<String, Object> getAuthenticationAttributesForRelease(Authentication authentication);
}
