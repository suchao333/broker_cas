package org.apereo.cas.authentication;

import lombok.extern.slf4j.Slf4j;

/**
 * Describes an error condition where authentication was prevented for some reason, e.g. communication
 * error with back-end authentication store.
 **描述由于某种原因（例如通信）而阻止身份验证的错误情况
 *
 * *后端身份验证存储出错。
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@Slf4j
public class PreventedException extends Exception {

    private static final long serialVersionUID = 4702274165911620708L;

    /**
     * Creates a new instance with the exception that prevented authentication.
     *
     * @param cause Error that prevented authentication.
     */
    public PreventedException(final Throwable cause) {
        super(cause);
    }

    /**
     * Creates a new instance with an explanatory message and the exception that prevented authentication.
     *
     * @param message Descriptive error message.
     * @param cause Error that prevented authentication.
     */
    public PreventedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
