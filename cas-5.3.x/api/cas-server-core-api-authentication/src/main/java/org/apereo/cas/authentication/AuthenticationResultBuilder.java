package org.apereo.cas.authentication;

import org.apereo.cas.authentication.principal.Service;

import java.io.Serializable;
import java.util.Optional;

/**
 * This is {@link AuthenticationResultBuilder}. It attempts to collect authentication objects
 * and will put the computed finalized primary {@link Authentication} into {@link AuthenticationResult}.
 * <strong>Concurrency semantics: implementations MUST be thread-safe.</strong>
 * Instances of this class should never be declared as a field. Rather they should always be passed around to methods that need them.
 **这是{@link AuthenticationResultBuilder}。它尝试收集身份验证对象
 *
 * *并将计算完成的主{@link Authentication}放入{@link AuthenticationResult}。
 *
 * *<strong>并发语义：实现必须是线程安全的。</strong>
 *
 * *此类的实例永远不应声明为字段。相反，它们应该始终传递给需要它们的方法。
 * @author Misagh Moayyed
 * @since 4.2.0
 */
public interface AuthenticationResultBuilder extends Serializable {

    /**
     * Gets the initial authentication.
     *获取初始身份验证。
     * @return the initial authentication
     */
    Optional<Authentication> getInitialAuthentication();

    /**
     * Collect authentication objects from any number of processed authentication transactions.
     *从任意数量的已处理身份验证事务收集身份验证对象。
     * @param authentication the authentication
     * @return the authentication result builder
     */
    AuthenticationResultBuilder collect(Authentication authentication);

    /**
     * Provided credentials immediately by the user.
     *用户立即提供凭据。
     * @param credential the credential
     * @return the authentication context builder
     */
    AuthenticationResultBuilder collect(Credential credential);

    /**
     * Build authentication result.
     * 生成身份验证结果。
     * @param principalElectionStrategy a principalElectionStrategy to use
     * @return the authentication result
     */
    AuthenticationResult build(PrincipalElectionStrategy principalElectionStrategy);

    /**
     * Build authentication result.
     *  生成身份验证结果。
     * @param principalElectionStrategy a principalElectionStrategy to use
     * @param service the service
     * @return the authentication result
     */
    AuthenticationResult build(PrincipalElectionStrategy principalElectionStrategy, Service service);
}
