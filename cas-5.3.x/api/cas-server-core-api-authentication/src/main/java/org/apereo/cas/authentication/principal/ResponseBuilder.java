package org.apereo.cas.authentication.principal;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apereo.cas.authentication.Authentication;
import org.springframework.core.Ordered;

import java.io.Serializable;

/**
 * Represents the task of building a CAS response
 * that is returned by a service.
 **表示生成CAS响应的任务
 *
 * *由服务返回。
 * @param <T> the type parameter
 * @author Misagh Moayyed
 * @since 4.2.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface ResponseBuilder<T extends WebApplicationService> extends Serializable, Ordered {

    /**
     * Build response. The implementation must produce
     * a response that is based on the type passed. Given
     * the protocol used, the ticket id may be passed
     * as part of the response. If the response type
     * is not recognized, an error must be thrown back.
     * 生成响应。实施必须产生
     *
     * 基于传递的类型的响应。鉴于
     *
     * 使用的协议，可以传递票证id
     *
     * 作为回应的一部分。如果响应类型
     *
     * *无法识别，必须返回错误。
     * @param service         the service
     * @param serviceTicketId the service ticket id
     * @param authentication  the authentication
     * @return the response
     */
    Response build(T service, String serviceTicketId, Authentication authentication);

    /**
     * Supports this service.
     *
     * @param service the service
     * @return true /false
     */
    boolean supports(T service);
}
