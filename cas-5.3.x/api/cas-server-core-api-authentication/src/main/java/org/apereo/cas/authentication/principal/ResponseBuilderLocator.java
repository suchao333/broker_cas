package org.apereo.cas.authentication.principal;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * This is {@link ResponseBuilderLocator} which attempts to locate {@link ResponseBuilder}
 * objects registered in the application context. This is an abstraction that is separated
 * from the actual service and response API to remove issues with serialization and such.
 * Services are no longer responsible for producing a response and their response builder
 * will need to be located via this class. This frees up the service API quite a bit to
 * inject all sorts of components into builders to accommodate for various use cases
 * and not have to worry about whether a given field in a builder is serialization friendly.
 **这是{@link ResponseBuilderLocator}，它试图定位{@link ResponseBuilder}
 *
 * *在应用程序上下文中注册的对象。这是一个被分离的抽象
 *
 * *从实际的服务和响应API中删除序列化等问题。
 *
 * *服务不再负责生成响应及其响应生成器
 *
 * *需要通过这个类定位。这将大大释放服务API，从而
 *
 * *向构建器中注入各种组件，以适应各种用例
 *
 * *而且不必担心构建器中的给定字段是否是序列化友好的。
 * @author Misagh Moayyed
 * @param <T> the type parameter
 * @since 5.1.0
 */
@FunctionalInterface
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface ResponseBuilderLocator<T extends WebApplicationService> extends Serializable {

    /**
     * Locate response builder appropriate for the given service.
     *
     * @param service the service
     * @return the response builder
     */
    ResponseBuilder<T> locate(T service);
}
