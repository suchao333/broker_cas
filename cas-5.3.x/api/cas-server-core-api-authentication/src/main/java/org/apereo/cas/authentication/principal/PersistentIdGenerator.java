package org.apereo.cas.authentication.principal;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * Generates a unique consistent Id based on the principal.
 *基于主体生成唯一的一致Id。
 * @author Scott Battaglia
 * @since 3.1
 */
//解决多态反序列化的问题
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface PersistentIdGenerator extends Serializable {

    /**
     * Generates a PersistentId based on some algorithm plus the principal.
     *根据一些算法和主体生成一个持久性。
     * @param principal the principal to generate the id for.
     * @param service   the service for which the id may be generated.
     * @return the generated persistent id.
     */
    String generate(String principal, String service);

    /**
     * Generates a PersistentId based on some algorithm plus the principal.
     *根据一些算法和主体生成一个持久性。
     * @param principal the principal to generate the id for.
     * @param service   the service for which the id may be generated.
     * @return the generated persistent id.
     */
    String generate(Principal principal, Service service);

    /**
     * Generate string.
     *
     * @param principal the principal
     * @return the string
     */
    default String generate(final Principal principal) {
        return generate(principal, null);
    }
}
