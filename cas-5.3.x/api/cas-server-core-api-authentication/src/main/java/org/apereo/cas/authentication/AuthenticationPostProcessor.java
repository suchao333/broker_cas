package org.apereo.cas.authentication;

import org.springframework.core.Ordered;

/**
 * This is {@link AuthenticationPostProcessor}. Authentication post processors
 * run as the very last step in CAS authentication where authentication event is internally
 * processed, validated, principal resolved and all attributes for principal and authentication
 * are collected.
 **这是{@link AuthenticationPostProcessor}。身份验证后处理程序
 *
 * *作为CAS身份验证的最后一步运行，其中身份验证事件是内部的
 *
 * *处理、验证、主体解析以及主体和身份验证的所有属性
 *
 * *收集。
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@FunctionalInterface
public interface AuthenticationPostProcessor extends Ordered {

    @Override
    default int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    /**
     * Process the authentication event.
     *
     * @param builder     Builder object that temporarily holds authentication metadata.
     *                    临时保存身份验证元数据的生成器对象。
     * @param transaction The authentication transaction. 身份验证事务。
     * @throws AuthenticationException the authn security exception
     */
    void process(AuthenticationBuilder builder, AuthenticationTransaction transaction) throws AuthenticationException;

    /**
     * Determines whether the processor has the capability to perform tasks on the given credential.
     * 确定处理器是否有能力在给定凭据上执行任务。
     * @param credential The credential to check. 要检查的凭据。
     * @return True if processor supports the Credential, false otherwise.
     */
    default boolean supports(final Credential credential) {
        return true;
    }
}
