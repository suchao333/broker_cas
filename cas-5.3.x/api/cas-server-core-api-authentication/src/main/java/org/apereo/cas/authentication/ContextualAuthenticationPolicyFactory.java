package org.apereo.cas.authentication;

/**
 * A factory for producing (stateful) authentication policies based on arbitrary context data.
 * This component provides a way to inject stateless factories into components that produce stateful
 * authentication policies that can leverage arbitrary contextual information to evaluate security policy.
 *
 * *基于任意上下文数据生成（有状态）身份验证策略的工厂。
 *
 * *此组件提供了一种将无状态工厂注入生成有状态的组件的方法
 *
 * *可以利用任意上下文信息来评估安全策略的身份验证策略。
 *
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@FunctionalInterface
public interface ContextualAuthenticationPolicyFactory<T> {

    /**
     * Creates a contextual (presumably stateful) authentication policy based on provided context data.
     * 基于提供的上下文数据创建上下文（可能是有状态的）身份验证策略。
     *
     * @param context Context data used to create an authentication policy. 用于创建身份验证策略的上下文数据。
     *
     * @return Contextual authentication policy object. The returned object should be assumed to be stateful
     * and not thread safe unless explicitly noted otherwise.
     * 上下文身份验证策略对象。应该假定返回的对象是有状态的
     *
     * *除非另有明确说明，否则不是线程安全的。
     */
    ContextualAuthenticationPolicy<T> createPolicy(T context);
}
