package org.apereo.cas.authentication;

import org.apereo.cas.authentication.principal.Service;

import java.io.Serializable;

/**
 * The {@link AuthenticationResult} is an abstraction representing final authentication outcome for any number of processed
 * authentication transactions.
 * *{@linkauthenticationresult}是一个抽象，表示任意数量的已处理的最终身份验证结果
 *
 * *身份验证事务。
 * <p>
 * An authentication result carries the primary composite authentication event, collected
 * from all authentication transactions. The principal and attributes associated with this authentication
 * are also collected out of all authentication transactions.

 *身份验证结果携带收集的主复合身份验证事件

 *所有身份验证事务。与此身份验证关联的主体和属性

 *也从所有身份验证事务中收集。
 * @author Misagh Moayyed
 * @since 4.2
 */
public interface AuthenticationResult extends Serializable {

    /**
     * Obtains the finalized primary authentication for this result.
     * 获取此结果的最终主身份验证。
     * @return the authentication
     */
    Authentication getAuthentication();

    /**
     * Gets the service for which this authentication result is relevant.
     * The service MAY be null, as an authentication result in CAS
     * can be established without providing a service/destination.
     **获取与此身份验证结果相关的服务。
     *
     * *服务可能为空，这是CAS中的身份验证结果
     *
     * *可以在不提供服务/目的地的情况下建立。
     * @return the service
     */
    Service getService();

    /**
     * Indicates whether the authentication event
     * was established as part of user providing credentials.
     **指示身份验证事件是否
     *
     * *作为提供凭据的用户的一部分建立。
     * @return true if credentials are provided. false is authn was established as part of SSO.
     * true 如果提供了凭据。false是作为SSO的一部分建立的。
     */
    boolean isCredentialProvided();

}
