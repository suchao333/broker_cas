package org.apereo.cas.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * This is {@link AuthenticationHandlerResolver} which decides which set of
 * authentication handlers shall be chosen for a given authN event.
 **这是{@link AuthenticationHandlerResolver}，它决定了
 *
 * *应为给定的authN事件选择身份验证处理程序。
 * @author Misagh Moayyed
 * @since 5.0.0
 */
public interface AuthenticationHandlerResolver extends Ordered {
    Logger LOGGER = LoggerFactory.getLogger(AuthenticationHandlerResolver.class);

    /**
     * Resolve set of authentication handlers.
     * 解析身份验证处理程序集。
     * @param candidateHandlers the candidate handlers
     * @param transaction       the transaction
     * @return the set
     */
    default Set<AuthenticationHandler> resolve(Set<AuthenticationHandler> candidateHandlers, AuthenticationTransaction transaction) {
        final String handlers = candidateHandlers.stream().map(AuthenticationHandler::getName).collect(Collectors.joining(","));
        LOGGER.debug("Default authentication handlers used for this transaction are [{}]", handlers);
        return candidateHandlers;
    }

    @Override
    default int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    /**
     * Supports this transaction?
     *是否支持此事务处理？
     * @param handlers    the handlers
     * @param transaction the transaction
     * @return the boolean
     */
    default boolean supports(final Set<AuthenticationHandler> handlers, final AuthenticationTransaction transaction) {
        return !handlers.isEmpty() && transaction != null;
    }
}
