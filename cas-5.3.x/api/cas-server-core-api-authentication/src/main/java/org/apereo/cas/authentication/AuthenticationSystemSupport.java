package org.apereo.cas.authentication;

import org.apereo.cas.authentication.principal.Service;

import java.util.Collection;

/**
 * This is {@link AuthenticationSystemSupport} - a facade that exposes a high level authentication system API to CAS core.
 * 这是{@linkauthenticationsystemsupport}-一个向CAS核心公开高级身份验证系统API的facade。
 * <p>
 * This component is to be injected into others where authentication subsystem interaction needs to happen - like performing single
 * authentication transaction, performing a finalized authentication transaction, or finalizing an all authentication transactions
 * that might have been processed and collected.
 * *这个组件将被注入到需要进行身份验证子系统交互的其他组件中，比如执行单个
 *
 * *身份验证事务、执行已完成的身份验证事务或完成所有身份验证事务
 *
 * *可能已经被处理和收集了。
 * <p>
 * This facade also exposes lower level components that implementations use to perform necessary authentication steps, so that clients of
 * this API have the ability to use those components directly if they choose so.
 * *这个facade还公开了实现用来执行必要的身份验证步骤的较低级别组件，以便
 *
 * *如果他们愿意，这个API可以直接使用这些组件。
 * @author Misagh Moayyed
 * @author Dmitriy Kopylenko
 * @since 4.2.0
 */
public interface AuthenticationSystemSupport {

    /**
     * Gets authentication transaction manager.
     * 获取身份验证事务管理器。
     * @return the authentication transaction manager
     */
    AuthenticationTransactionManager getAuthenticationTransactionManager();

    /**
     * Gets principal election strategy.
     * 获取主要选举策略。
     * @return the principal election strategy
     */
    PrincipalElectionStrategy getPrincipalElectionStrategy();

    /**
     * Initiate potential multi-transaction authentication event by handling the initial authentication transaction.
     *通过处理初始身份验证事务来启动潜在的多事务身份验证事件。
     * @param authentication a pre-established authentication object in a multi-legged authentication flow.
     *                       多足身份验证流中预先建立的身份验证对象。
     * @param credential     a credential for the authentication transaction.
     *                       身份验证事务的凭据。
     * @return authentication result builder used to accumulate authentication transactions in this authentication event.
     *         用于在此身份验证事件中累积身份验证事务的身份验证结果生成器。
     * @since 5.0.0
     */
    AuthenticationResultBuilder establishAuthenticationContextFromInitial(Authentication authentication, Credential credential);

    /**
     * Initiate potential multi-transaction authentication event by handling the initial authentication transaction.
     *  通过处理初始身份验证事务来启动潜在的多事务身份验证事件。
     * @param service    the service
     * @param credential a credential for the initial authentication transaction.
     *                    身份验证事务的凭据。
     * @return authentication result builder used to accumulate authentication transactions in this authentication event.
               用于在此身份验证事件中累积身份验证事务的身份验证结果生成器。
     * @throws AuthenticationException exception to indicate authentication processing failure.
     * @since 5.0.0
     */
    AuthenticationResultBuilder handleInitialAuthenticationTransaction(Service service, Credential... credential) throws AuthenticationException;

    /**
     * Handle single authentication transaction within potential multi-transaction authentication event.
     * 在潜在的多事务身份验证事件中处理单个身份验证事务。
     * @param service                     the service
     * @param authenticationResultBuilder builder used to accumulate authentication transactions in this authentication event.
     *                                    用于在此身份验证事件中累积身份验证事务的生成器。
     * @param credential                  a credential used for this authentication transaction.
     *                                    用于此身份验证事务的凭据。
     * @return authentication result builder used to accumulate authentication transactions in this authentication event.
     *         用于在此身份验证事件中累积身份验证事务的身份验证结果生成器。
     * @throws AuthenticationException exception to indicate authentication processing failure.
     * @since 5.0.0
     */
    AuthenticationResultBuilder handleAuthenticationTransaction(Service service,
                                                                AuthenticationResultBuilder authenticationResultBuilder,
                                                                Credential... credential) throws AuthenticationException;

    /**
     * Finalize all authentication transactions processed and collected for this authentication event.
     * 完成为此身份验证事件处理和收集的所有身份验证事务。
     * @param authenticationResultBuilder builder used to accumulate authentication transactions in this authentication event.
     *                                    用于在此身份验证事件中累积身份验证事务的生成器。
     * @param service                     a service for this authentication event.
     *
     * @return authentication result representing a final outcome of the authentication event.
     * 表示身份验证事件的最终结果的身份验证结果。
     * @since 5.0.0
     */
    AuthenticationResult finalizeAllAuthenticationTransactions(AuthenticationResultBuilder authenticationResultBuilder, Service service);

    /**
     * Handle a single-transaction authentication event and immediately produce a finalized {@link AuthenticationResult}.
     *处理单个事务身份验证事件并立即生成最终的{@link AuthenticationResult}。
     * @param service    a service for this authentication event.
     *                   此身份验证事件的服务。
     * @param credential a credential used for this single-transaction authentication event.
     *                   用于此单事务身份验证事件的凭据。
     * @return authentication result representing a final outcome of the authentication event.
     *         表示身份验证事件的最终结果的身份验证结果。
     * @throws AuthenticationException exception to indicate authentication processing failure.
     * @since 5.0.0
     */
    AuthenticationResult handleAndFinalizeSingleAuthenticationTransaction(Service service, Credential... credential) throws AuthenticationException;

    /**
     * Handle a single-transaction authentication event and immediately produce a finalized {@link AuthenticationResult}.
     *  处理单个事务身份验证事件并立即生成最终的{@link AuthenticationResult}。
     *
     * @param service    a service for this authentication event.
     *                   此身份验证事件的服务。
     * @param credentials credentials used for this single-transaction authentication event.
     *                    用于此单事务身份验证事件的凭据。
     * @return authentication result representing a final outcome of the authentication event.
     *         表示身份验证事件的最终结果的身份验证结果。
     * @throws AuthenticationException exception to indicate authentication processing failure.
     * @since 5.3.0
     */
    default AuthenticationResult handleAndFinalizeSingleAuthenticationTransaction(final Service service,
                                                                                  final Collection<Credential> credentials) throws AuthenticationException {
        return handleAndFinalizeSingleAuthenticationTransaction(service, credentials.toArray(new Credential[]{}));
    }
}
