package org.apereo.cas.authentication.adaptive;

import org.apereo.cas.authentication.adaptive.geo.GeoLocationRequest;

/**
 * This is {@link AdaptiveAuthenticationPolicy}.
 *Adaptive（适应的，适合的）
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@FunctionalInterface
public interface AdaptiveAuthenticationPolicy {
    
    /**
     * Apply the strategy to figure out whether this authentication attempt can proceed.
     * 应用该策略来确定此身份验证尝试是否可以继续进行。
     * @param userAgent the user agent
     * @param location  the location
     * @return true/false
     */
    boolean apply(String userAgent, GeoLocationRequest location);
}
