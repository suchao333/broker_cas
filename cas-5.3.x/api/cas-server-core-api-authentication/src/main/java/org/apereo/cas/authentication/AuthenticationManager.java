package org.apereo.cas.authentication;

/**
 * Authenticates one or more credentials.
 * 验证一个或多个凭据。
 * @author Scott Battaglia
 * @author Marvin S. Addison
 *
 * @since 3.0.0
 */
@FunctionalInterface
public interface AuthenticationManager {

    /** Authentication method attribute name. **/
    String AUTHENTICATION_METHOD_ATTRIBUTE = "authenticationMethod";

    /**
     * Authenticates the provided credentials. On success, an {@link Authentication} object
     * is returned containing metadata about the result of each authenticated credential.
     * Note that a particular implementation may require some or all credentials to be
     * successfully authenticated. Failure to authenticate is considered an exceptional case, and
     * an AuthenticationException is thrown.
     * *验证提供的凭据。成功时，{@link Authentication}对象
     *
     * *返回包含有关每个已验证凭据结果的元数据。
     *
     * *请注意，特定实现可能需要一些或所有凭据
     *
     * *已成功验证。未能认证被视为例外情况，并且
     *
     * *引发AuthenticationException。
     *
     * @param authenticationTransaction Process a single authentication transaction
     *
     * @return Authentication object on success that contains metadata about credentials that were authenticated.
     *
     * @throws AuthenticationException On authentication failure. The exception contains details
     * on each of the credentials that failed to authenticate.
     */
    Authentication authenticate(AuthenticationTransaction authenticationTransaction) throws AuthenticationException;
}
