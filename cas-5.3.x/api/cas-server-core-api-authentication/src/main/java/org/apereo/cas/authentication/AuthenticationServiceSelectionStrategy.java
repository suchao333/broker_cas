package org.apereo.cas.authentication;

import org.apereo.cas.authentication.principal.Service;
import org.springframework.core.Ordered;

import java.io.Serializable;

/**
 * This is {@link AuthenticationServiceSelectionStrategy} which attempts to
 * resolve and nominate a service during a validation event. By default
 * most services provided to CAS are taken and resolved verbatim. However,
 * in scenarios where a given module ends up inserting target service identifiers
 * into the service parameter as query strings where the final service URL is
 * designated to be a callback url to CAS which carries the real service identifier,
 * (such as SAML2 support where callbacks are used to route the request back to CAS),
 * an implementation of this strategy may be used to detect the query parameters
 * inside the URL to use the real service for user attribute processing and more.
 * Services resolved via this strategy must still be vetted against the service registry.
 *Strategy(策略，战略)
 * *这是{@link AuthenticationServiceSelectionStrategy}，它试图
 *
 * *在验证事件期间解析和指定服务。默认情况下
 *
 * *向CAS提供的大多数服务都是逐字执行和解析的。然而，
 *
 * *在给定模块最终插入目标服务标识符的情况下
 *
 * *作为查询字符串传入服务参数，其中最终的服务URL位于
 *
 * *指定为CAS的回调url，其中包含真实的服务标识符，
 *
 * *（例如SAML2支持，其中回调用于将请求路由回cas），
 *
 * *此策略的实现可以用来检测查询参数
 *
 * *在URL中使用真正的服务来处理用户属性等。
 *
 * *通过此策略解决的服务仍然必须根据服务注册中心进行审核。
 * @author Misagh Moayyed
 * @since 5.0.0
 */
public interface AuthenticationServiceSelectionStrategy extends Serializable, Ordered {
        
    /**
     * Resolves the real service from the provided service, if appropriate.
     * 从提供的服务中解析实际服务（如果合适）。
     * @param service the provided service by the caller
     * @return the resolved service
     */
    Service resolveServiceFrom(Service service);

    /**
     * Indicates whether this strategy supports service selection.
     * 指示此策略是否支持服务选择。
     * @param service the service
     * @return true/false
     */
    boolean supports(Service service);
}
