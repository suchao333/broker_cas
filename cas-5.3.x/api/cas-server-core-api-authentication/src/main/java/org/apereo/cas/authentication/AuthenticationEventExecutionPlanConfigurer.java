package org.apereo.cas.authentication;

/**
 * This is {@link AuthenticationEventExecutionPlanConfigurer}.
 * Passes on an authentication execution plan to implementors
 * to register authentication handlers, etc.
 * *这是{@link AuthenticationEventExecutionPlanConfigurer}。
 *
 *将身份验证执行计划传递给实现者
 *
 *注册身份验证处理程序等。
 * <p>
 * Since this interface conforms to a functional interface requirement, typical implementors
 * are {@code @Conditional} beans expressed as compact lambda expressions inside of various CAS modules that
 * contribute to the overall CAS authentication subsystem.
 * 由于此接口符合功能接口需求，典型的实现者
 *
 * {@code@Conditional}bean是否在不同的CAS模块中表示为紧凑的lambda表达式
 *
 * CAs对整个CAs子系统有贡献。
 * <p>
 * Note: Existing configuration classes that are injected authentication-related functionality
 * such as the transaction manager or the authentication support components need to be refactored
 * to isolate those changes into the configurer. Otherwise, circular dependency issues may appear.
 *注意：注入身份验证相关功能的现有配置类
 *
 * *例如事务管理器或身份验证支持组件需要重构
 *
 * *将这些更改隔离到配置器中。否则，可能会出现循环依赖问题。
 * @author Misagh Moayyed
 * @author Dmitriy Kopylenko
 * @since 5.1.0
 */
@FunctionalInterface
public interface AuthenticationEventExecutionPlanConfigurer {

    /**
     * configure the plan.
     *
     * @param plan the plan
     */
    void configureAuthenticationExecutionPlan(AuthenticationEventExecutionPlan plan);
}
