package org.apereo.cas.authentication.handler;


/**
 * Strategy pattern component for transforming principal names in the authentication pipeline.
 *用于在身份验证管道中转换主体名称的策略模式组件。
 * @author Howard Gilbert
 * @since 3.3.6
 */
@FunctionalInterface
public interface PrincipalNameTransformer {

    /**
     * Transform the string typed into the login form into a tentative Principal Name to be
     * validated by a specific type of Authentication Handler.
     *将输入到登录表单中的字符串转换为暂定的主名称
     * 由特定类型的身份验证处理程序进行验证。
     * <p>The Principal Name eventually assigned by the PrincipalResolver may
     * be unqualified ("AENewman"). However, validation of the Principal name against a
     * particular backend source represented by a particular Authentication Handler may
     * require transformation to a temporary fully qualified format such as
     * AENewman@MAD.DCCOMICS.COM or MAD\AENewman. After validation, this form of the
     * Principal name is discarded in favor of the choice made by the Resolver.
     *最终由Principal resolver指定的主名称可以
     * 不合格(“AENewman”)。但是，根据a对主体名称进行验证
     * 由特定身份验证处理程序表示的特定后端源可以
     * 需要转换为临时的完全限定格式，如
     * AENewman@MAD.DCCOMICS.COM或疯狂\ AENewman。验证后，这种形式的
     * 为了支持解析器所做的选择，主名称被放弃。
     * @param formUserId The raw userid typed into the login form
     * @return the string that the Authentication Handler should lookup in the backend system
     */
    String transform(String formUserId);
}

