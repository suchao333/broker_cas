package org.apereo.cas.authentication;

import org.apereo.cas.authentication.principal.PrincipalResolver;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This is {@link AuthenticationEventExecutionPlan}.
 * A higher-level abstraction to encapsulate the registration of authentication handlers, etc. Each module would interact with
 * this interface that controls the registration and positioning of the handlers, etc.
 * The authentication manager contains this interface, and may dynamically for each transaction ask for a candidate list of handlers/resolvers, etc.
 **这是{@link AuthenticationEventExecutionPlan}。
 *
 * *一个更高级别的抽象，用于封装身份验证处理程序等的注册
 *
 * *此接口控制处理程序的注册和定位等。
 *
 * *身份验证管理器包含此接口，并且可以为每个事务动态地请求处理程序/解析程序等的候选列表。
 * @author Misagh Moayyed
 * @since 5.1.0
 */
public interface AuthenticationEventExecutionPlan {

    /**
     * Register authentication handler.
     *注册身份验证处理程序。
     * @param handler the handler
     */
    void registerAuthenticationHandler(AuthenticationHandler handler);

    /**
     * Register metadata populator（人口）.
     * 注册元数据填充器
     * @param populator the populator
     */
    void registerMetadataPopulator(AuthenticationMetaDataPopulator populator);

    /**
     * Register authentication post processor.
     *注册身份验证后处理程序。
     * @param processor the populator
     */
    void registerAuthenticationPostProcessor(AuthenticationPostProcessor processor);

    /**
     * Register authentication pre processor.
     *注册身份验证预处理器。
     * @param processor the populator
     */
    void registerAuthenticationPreProcessor(AuthenticationPreProcessor processor);


    /**
     * Register metadata populators.
     *注册元数据填充器。
     * @param populator the populator
     */
    void registerMetadataPopulators(Collection<AuthenticationMetaDataPopulator> populator);

    /**
     * Register authentication policy.
     * 注册身份验证策略。
     * @param authenticationPolicy the authentication policy
     */
    void registerAuthenticationPolicy(AuthenticationPolicy authenticationPolicy);

    /**
     * Register authentication handler resolver.
     *注册身份验证处理程序解析程序。
     * @param handlerResolver the handler resolver
     */
    void registerAuthenticationHandlerResolver(AuthenticationHandlerResolver handlerResolver);

    /**
     * Register authentication handler with principal resolver.
     *向主体解析程序注册身份验证处理程序。
     * @param plan the plan
     */
    void registerAuthenticationHandlerWithPrincipalResolver(Map<AuthenticationHandler, PrincipalResolver> plan);

    /**
     * Register authentication handler with principal resolver.
     *向主体解析程序注册身份验证处理程序。
     * @param handler           the handler
     * @param principalResolver the principal resolver
     */
    void registerAuthenticationHandlerWithPrincipalResolver(AuthenticationHandler handler, PrincipalResolver principalResolver);

    /**
     * Register authentication handlers with principal resolver.
     * 向主体解析程序注册身份验证处理程序。
     * @param handlers          the handlers
     * @param principalResolver the principal resolver
     */
    void registerAuthenticationHandlerWithPrincipalResolvers(Collection<AuthenticationHandler> handlers, PrincipalResolver principalResolver);

    /**
     * Register authentication handler with principal resolvers.
     * 向主体解析程序注册身份验证处理程序。
     * @param handlers          the handlers
     * @param principalResolver the principal resolver
     */
    void registerAuthenticationHandlerWithPrincipalResolvers(List<AuthenticationHandler> handlers, List<PrincipalResolver> principalResolver);

    /**
     * Gets authentication handlers for transaction.
     *获取事务的身份验证处理程序。
     * @param transaction the transaction
     * @return the authentication handlers for transaction
     */
    Set<AuthenticationHandler> getAuthenticationHandlersForTransaction(AuthenticationTransaction transaction);

    /**
     * Gets authentication metadata populators.
     * 获取身份验证元数据填充器。
     * @param transaction the transaction
     * @return the authentication metadata populators
     */
    Collection<AuthenticationMetaDataPopulator> getAuthenticationMetadataPopulators(AuthenticationTransaction transaction);

    /**
     * Gets authentication post processors.
     *获取身份验证后处理程序。
     * @param transaction the transaction
     * @return the authentication metadata populators
     */
    Collection<AuthenticationPostProcessor> getAuthenticationPostProcessors(AuthenticationTransaction transaction);

    /**
     * Gets authentication pre processors.
     * 获取身份验证预处理器。
     * @param transaction the transaction
     * @return the authentication metadata populators
     */
    Collection<AuthenticationPreProcessor> getAuthenticationPreProcessors(AuthenticationTransaction transaction);

    /**
     * Gets principal resolver for authentication transaction.
     *获取身份验证事务的主体解析程序
     * @param handler     the handler
     * @param transaction the transaction
     * @return the principal resolver for authentication transaction
     */
    PrincipalResolver getPrincipalResolverForAuthenticationTransaction(AuthenticationHandler handler, AuthenticationTransaction transaction);

    /**
     * Gets authentication policies.
     * 获取身份验证策略
     * @param transaction the transaction
     * @return the authentication policies
     */
    Collection<AuthenticationPolicy> getAuthenticationPolicies(AuthenticationTransaction transaction);

    /**
     * Gets authentication handler resolvers.
     *  获取身份验证处理程序解析程序。
     * @param transaction the transaction
     * @return the authentication handler resolvers
     */
    Collection<AuthenticationHandlerResolver> getAuthenticationHandlerResolvers(AuthenticationTransaction transaction);
}
