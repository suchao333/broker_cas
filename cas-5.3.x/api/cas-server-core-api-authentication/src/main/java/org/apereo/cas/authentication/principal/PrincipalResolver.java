package org.apereo.cas.authentication.principal;

import org.apereo.cas.authentication.AuthenticationHandler;
import org.apereo.cas.authentication.Credential;
import org.apereo.services.persondir.IPersonAttributeDao;

import java.util.Optional;

/**
 * Resolves a {@link Principal} from a {@link Credential} using an arbitrary(任意的) strategy（策略）.
 * Since a {@link Principal} requires an identifier at a minimum, the simplest strategy to produce a principal
 * is to simply copy {@link Credential#getId()} onto
 * {@link Principal#getId()}. Resolvers commonly query one or more data sources
 * to obtain attributes such as affiliations, group membership, display name, and email. The data source(s) may also
 * provide an alternate identifier mapped by the credential identifier.
 *
 * 使用任意策略从{@link Credential}(证书)解析{@link Principal}。
 *
 * 由于{@link Principal}至少需要一个标识符，这是生成主体的最简单策略
 *
 * 只是将{@link Credential#getId（）}复制到
 *
 * {@link Principal\getId（）}。解析器通常查询一个或多个数据源
 *
 * 获取属性，如从属关系、组成员身份、显示名称和电子邮件。数据源也可以
 *
 * 提供由凭据标识符映射的备用标识符
 * @author Scott Battaglia
 * @author Marvin S. Addison
 * @see Principal
 * @see Credential
 * @since 4.0.0
 */
public interface PrincipalResolver {

    /**
     * Resolves a principal from the given credential using an arbitrary strategy.
     * Assumes no principal is already resolved by the authentication subsystem, etc.
     **使用任意策略从给定凭证解析主体。
     *
     * *假设身份验证子系统没有解析主体，等等。
     * @param credential Source credential.
     * @return the principal
     */
    default Principal resolve(Credential credential) {
        return resolve(credential, Optional.empty(), Optional.empty());
    }

    /**
     * Resolves a principal from the given credential using an arbitrary（任意的） strategy.
     * Assumes no principal is already resolved by the authentication subsystem, etc.
     **使用任意策略从给定凭证解析主体。
     *
     * *假设身份验证子系统没有解析主体，等等。
     * @param credential Source credential.
     * @param handler    the authentication handler linked to the resolver. May be null.
     * @return the principal
     */
    default Principal resolve(Credential credential, Optional<AuthenticationHandler> handler) {
        return resolve(credential, Optional.empty(), handler);
    }
    
    /**
     * Resolves a principal from the given credential using an arbitrary strategy.
     *
     * @param credential Source credential.
     * @param principal  A principal that may have been produced during the authentication process. May be null.
     * @param handler    the authentication handler linked to the resolver. May be null.
     * @return Resolved principal, or null if the principal could not be resolved.
     */
    Principal resolve(Credential credential, Optional<Principal> principal, Optional<AuthenticationHandler> handler);

    /**
     * Determines whether this instance supports principal resolution from the given credential. This method SHOULD
     * be called prior to {@link #resolve(Credential, Optional, Optional)})}.
     **确定此实例是否支持来自给定凭据的主体解析。这种方法应该
     *
     * *在{@link#resolve（Credential，Optional，Optional）}}之前调用。
     * @param credential The credential to check for support.
     * @return True if credential is supported, false otherwise.
     */
    boolean supports(Credential credential);

    /**
     * Gets attribute repository, if any.
     *获取属性存储库(如果有的话)。
     * @return the attribute repository or null.
     * @since 5.1
     */
    IPersonAttributeDao getAttributeRepository();
}
