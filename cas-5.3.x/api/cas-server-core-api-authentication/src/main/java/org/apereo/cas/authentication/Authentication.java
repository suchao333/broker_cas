package org.apereo.cas.authentication;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apereo.cas.authentication.principal.Principal;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * The Authentication object represents a successful authentication request. It
 * contains the principal that the authentication request was made for as well
 * as the additional meta information such as the authenticated date and a map
 * of attributes.
 * 身份验证对象表示成功的身份验证请求。它
 * 也包含身份验证请求所针对的主体
 * 作为附加的元信息，如经过身份验证的日期和地图
 * 的属性。
 * </p>
 * <p>
 * An Authentication object must be serializable to permit persistence and
 * clustering.
 * 身份验证对象必须是可序列化的，以允许持久性和
 * 集群。
 * </p>
 * <p>
 * Implementing classes must take care to ensure that the Map returned by
 * getAttributes is serializable by using a Serializable map such as HashMap.
 * 实现类必须注意确保返回的映射
 * 通过使用可序列化的映射(如HashMap)， getAttributes可以序列化。
 * </p>
 *Authentication （认证）
 * @author Dmitriy Kopylenko
 * @author Scott Battaglia
 * @author Marvin S. Addison
 * @since 3.0.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface Authentication extends Serializable {

    /**
     * Method to obtain the Principal.
     *
     * @return a Principal implementation
     */
    Principal getPrincipal();

    /**
     * Method to retrieve the timestamp of when this Authentication object was
     * created.
     *
     * @return the date/time the authentication occurred.
     */
    ZonedDateTime getAuthenticationDate();

    /**
     * Attributes of the authentication (not the Principal).
     *
     * @return the map of attributes.
     */
    Map<String, Object> getAttributes();
    
    /**
     * Add attribute to the authentication object and update the instance.
     *
     * @param name  the name
     * @param value the value
     */
    void addAttribute(String name, Object value);

    /**
     * Gets a list of metadata about the credentials supplied at authentication time.
     *
     * @return Non -null list of supplied credentials represented as metadata that should be
     * considered safe for long-term storage (e.g. serializable and secure with respect to credential disclosure).
     * The order of items in the returned list SHOULD be the same as the order in which the source credentials
     * were presented and subsequently processed.
     *
     * 提供的凭据的非空列表，表示为
     *
     * 对于长期存储而言，被认为是安全的（例如，可串行化并在凭证披露方面是安全的）。
     *
     * 返回列表中项目的顺序应与源凭据的顺序相同
     *
     * 呈现并随后处理。
     */
    List<CredentialMetaData> getCredentials();

    /**
     * Gets a map describing successful authentications produced by {@link AuthenticationHandler} components.
     *
     * @return Map of handler names to successful authentication result produced by that handler.
     * *获取描述由{@link AuthenticationHandler}组件生成的成功身份验证的映射。
     * *@return处理程序名称到该处理程序生成的成功身份验证结果的映射。
     */
    Map<String, AuthenticationHandlerExecutionResult> getSuccesses();

    /**
     * Gets a map describing failed authentications. By definition the failures here were not sufficient to prevent
     * authentication.
     *
     * @return Map of authentication handler names to the authentication errors produced on attempted authentication.
     *获取描述失败身份验证的映射。根据定义，这里的故障不足以防止
     *身份验证。
     *@return身份验证处理程序名称到尝试身份验证时产生的身份验证错误的映射。
     */
    Map<String, Throwable> getFailures();

    /**
     * Updates the authentication object with what's passed.
     * Does not override current keys if there are clashes
     **使用传递的内容更新身份验证对象。
     *
     * *如果存在冲突，则不覆盖当前关键帧
     * @param authn the authn object
     */
    void update(Authentication authn);

    /**
     * Updates the authentication object with what's passed.
     * Does override current keys if there are clashes.
     * Clears the existing attributes and starts over.
     **使用传递的内容更新身份验证对象。
     *
     * *如果存在冲突，将覆盖当前关键帧。
     *
     * *清除现有属性并重新开始。
     * @param authn the authn object
     */
    void updateAll(Authentication authn);
}
