package org.apereo.cas.authentication.principal;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Generic concept of an authenticated thing. Examples include a person or a
 * service.
 * *认证事物的一般概念。例如一个人或一个服务。
 *
 * <p>
 * The implementation SimplePrincipal just contains the Id property. More
 * complex Principal objects may contain additional information that are
 * meaningful to the View layer but are generally transparent(透明的) to the rest(对其他人) of
 * CAS.
 * 但对CAS的其他成员通常是透明的
 * </p>
 * 实现SimplePrincipal只包含Id属性。更多的
 * 复杂主体对象可能包含以下附加信息
 * 对视图层有意义，但对CAs的其他成员通常是透明的
 *
 * @author Scott Battaglia
 * @since 3.0.0
 */
@FunctionalInterface
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface Principal extends Serializable {

    /**
     * @return the unique id for the Principal
     */
    String getId();

    /**
     * @return the map of configured attributes for this principal
     */
    default Map<String, Object> getAttributes() {
        return new LinkedHashMap<>(0);
    }
}
