package org.apereo.cas.authentication;

import org.springframework.core.Ordered;

/**
 * Strategy interface for pluggable authentication security policies.
 *可插拔身份验证安全策略的策略接口。
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@FunctionalInterface
public interface AuthenticationPolicy extends Ordered {
    /**
     * Determines whether an authentication event isSatisfiedBy arbitrary security policy.
     * 确定身份验证事件是否由任意安全策略满足。
     * @param authentication Authentication event to examine for compliance with security policy.
     * @return True if authentication isSatisfiedBy security policy, false otherwise.
     * @throws Exception the exception
     */
    boolean isSatisfiedBy(Authentication authentication) throws Exception;

    @Override
    default int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
