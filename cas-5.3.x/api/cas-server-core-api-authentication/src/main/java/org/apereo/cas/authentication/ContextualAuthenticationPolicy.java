package org.apereo.cas.authentication;

import java.util.Optional;

/**
 * A stateful authentication policy that is applied using arbitrary contextual information.
 * 使用任意上下文信息应用的有状态身份验证策略。
 * Contextual（上下文）
 * @author Marvin S. Addison
 * @since 4.0.0
 */
public interface ContextualAuthenticationPolicy<T> extends AuthenticationPolicy {
    /**
     * Gets the context used to evaluate the authentication policy.
     * 获取用于评估身份验证策略的上下文。
     * @return Context information.
     */
    T getContext();

    /**
     * Return an optional message code to use when this is unsatisfied.
     * 返回一个可选的消息代码，当不满足时使用。
     * @return Optional message code
     */
    default Optional<String> getCode() {
        return Optional.empty();
    }
}
