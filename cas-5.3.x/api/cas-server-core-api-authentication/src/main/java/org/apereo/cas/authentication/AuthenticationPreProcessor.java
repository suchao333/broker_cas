package org.apereo.cas.authentication;

import org.springframework.core.Ordered;

/**
 * This is {@link AuthenticationPreProcessor}. Authentication pre processors
 * run as the very first step in CAS authentication where credentials are about
 * to be authenticated.
 **这是{@link AuthenticationPreProcessor}。身份验证预处理器
 *
 * *作为CAS身份验证的第一步运行，其中涉及凭据
 *
 * *待认证。
 * @author Misagh Moayyed
 * @since 5.3.0
 */
@FunctionalInterface
public interface AuthenticationPreProcessor extends Ordered {

    @Override
    default int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    /**
     * Process the authentication event.
     *
     * @param transaction The authentication transaction. 身份验证事务。
     * @return the boolean
     * @throws AuthenticationException the authn security exception
     */
    boolean process(AuthenticationTransaction transaction) throws AuthenticationException;

    /**
     * Determines whether the processor has the capability（能力） to perform tasks on the given credential.
     *确定处理器是否有能力在给定凭据上执行任务。
     * @param credential The credential to check.
     * @return True if processor supports the Credential, false otherwise.
     */
    default boolean supports(final Credential credential) {
        return true;
    }
}
