package org.apereo.cas.authentication.principal;

import org.apereo.cas.validation.ValidationResponseType;

/**
 * Represents a service using CAS that comes from the web.
 * 表示使用来自web的CAS的服务。
 * @author Scott Battaglia
 * @since 3.1
 */
public interface WebApplicationService extends Service {

    /**
     * Retrieves the artifact supplied with the service. May be null.
     * 检索与服务一起提供的项目。可能为空。
     * @return the artifact if it exists, null otherwise.
     */
    String getArtifactId();

    /**
     * Indicates the source from which the service object was extracted and built.
     * Typically maps to a request parameter or attribute that provides the request.
     **指示从中提取和生成服务对象的源。
     *
     * *通常映射到提供请求的请求参数或属性。
     * @return the source if it exists, null otherwise.
     */
    String getSource();

    /**
     * Return the original url provided (as {@code service} or {@code targetService} request parameter).
     * Used to reconstruct the redirect url.
     **返回提供的原始url（作为{@code service}或{@code targetService}请求参数）。
     *
     * *用于重建重定向url。
     * @return the original url provided.
     */
    String getOriginalUrl();

    /**
     * Ticket validation response MUST be produced based on the parameter value.
     * Supported values are XML and JSON. If this parameter is not set,
     * the default XML format will be used. If the parameter value is not supported by the CAS server,
     * an error must be produced.
     **必须根据参数值生成票证验证响应。
     *
     * *支持的值是XML和JSON。如果未设置此参数，
     *
     * *将使用默认的XML格式。如果CAS服务器不支持该参数值，
     *
     * *必须产生错误。
     * @return the requested format
     * @since 4.2
     */
    ValidationResponseType getFormat();

    /**
     * Return if the service is already logged out.
     **如果服务已注销，则返回。
     * @return if the service is already logged out.
     */
    boolean isLoggedOutAlready();

    /**
     * Set if the service is already logged out.
     *设置服务是否已注销。
     * @param loggedOutAlready if the service is already logged out.
     */
    void setLoggedOutAlready(boolean loggedOutAlready);
}
