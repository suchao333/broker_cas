package org.apereo.cas.authentication;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Describes a credential provided for authentication. Implementations should expect instances of this type to be
 * stored for periods of time equal to the length of the SSO session or longer, which necessitates consideration of
 * serialization and security. All implementations MUST be serializable and secure with respect to long-term storage.
 *
 *描述为身份验证提供的凭据。实现应该期望此类型的实例
 *
 *存储的时间长度等于或超过SSO会话的长度，因此需要考虑
 *
 *序列化和安全性。对于长期存储，所有实现都必须是可序列化的和安全的。
 *
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include= JsonTypeInfo.As.PROPERTY)
public interface CredentialMetaData {
    /**
     * Gets a unique identifier for the kind of credential this represents.
     *获取此表示的凭据类型的唯一标识符。
     * @return Unique identifier for the given type of credential.
     */
    String getId();

    /**
     * Gets the type of the original credential.
     *获取原始凭据的类型。
     * @return Non-null credential class.
     */
    Class<? extends Credential> getCredentialClass();
}
