package org.apereo.cas.authentication;

/**
 * The {@link AuthenticationTransactionManager} deals exclusively with authentication concepts
 * e.g. Credentials, Principals, producing valid Authentication objects. It is invoked repeatedly with distinct credential type(s)
 * for interactive multi-staged authn flows that would authenticate at each step as opposed
 * to gather all credentials and send them for authentication in one batch.

 *{@link AuthenticationTransactionManager}专门处理身份验证概念

 *例如，凭据、主体、生成有效身份验证对象。它使用不同的凭据类型重复调用

 *对于交互的多阶段authn流，每个步骤都会进行身份验证

 *收集所有凭据并将其发送到一个批中进行身份验证。
 * @author Misagh Moayyed
 * @since 4.2
 */
public interface AuthenticationTransactionManager {
    /**
     * Handle authentication transaction manager.
     *处理身份验证事务管理器。
     * @param authenticationTransaction   the authn attempt 授权尝试
     * @param authenticationResultBuilder the authentication context 身份验证上下文
     * @return the transaction manager
     * @throws AuthenticationException the authentication exception
     */
    AuthenticationTransactionManager handle(AuthenticationTransaction authenticationTransaction,
                                            AuthenticationResultBuilder authenticationResultBuilder)
                                            throws AuthenticationException;

    /**
     * Gets authentication manager.
     *获取身份验证管理器。
     * @return the authentication manager
     */
    AuthenticationManager getAuthenticationManager();
}
