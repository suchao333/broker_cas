package org.apereo.cas.authentication;

import org.apereo.cas.authentication.principal.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

/**
 * This is {@link AuthenticationTransaction}.
 *
 * @author Misagh Moayyed
 * @since 4.2.0
 */
public interface AuthenticationTransaction extends Serializable {

    /**
     * Gets service.
     *
     * @return the service
     */
    Service getService();

    /**
     * Gets credentials.
     *
     * @return the credentials
     */
    Collection<Credential> getCredentials();

    /**
     * Gets the first (primary) credential in the chain.
     * 获取链中的第一个（主）凭据。
     * @return the credential
     */
    default Optional<Credential> getPrimaryCredential() {
        return getCredentials().stream().findFirst();
    }

    /**
     * Does this AuthenticationTransaction contain a credential of the given type?
     *此AuthenticationTransaction是否包含给定类型的凭据？
     * @param type the credential type to check for
     *             要检查的凭据类型
     * @return true if this AuthenticationTransaction contains a credential of the specified type
     *         true      如果此AuthenticationTransaction包含指定类型的凭据
     */
    default boolean hasCredentialOfType(final Class<? extends Credential> type) {
        return getCredentials().stream().anyMatch(type::isInstance);
    }
}

