package org.apereo.cas.ticket;

import org.springframework.core.Ordered;

/**
 * This is {@link TicketDefinition}. Ticket definition describes additional Properties and misc settings
 * that may be associated with a given ticket to be used by registries. Each CAS module on start up
 * has the ability to register/alter ticket metadata that may be requires for its own specific functionality.
 * Given each CAS module may decide to create many forms of tickets, this facility is specifically provided
 * to dynamically register ticket types and associated properties so modules that deal with registry functionality
 * wouldn't have to statically link to all modules and APIs.
 *这是{@link TicketDefinition}。票据定义描述其他属性和其他设置
 *它可能与注册中心使用的给定票证相关联。启动时的每个CAS模块
 *具有注册/更改票证元数据的能力，这些元数据可能是其自身特定功能所需的。
 *考虑到每个CAS模块可能决定创建多种形式的票据，因此专门提供了这种工具
 *动态注册票证类型和相关属性，以便处理注册表功能的模块
 *不需要静态链接到所有模块和api。
 * @author Misagh Moayyed
 * @see TicketCatalog
 * @since 5.1.0
 */
public interface TicketDefinition extends Ordered {
    /**
     * Gets prefix.
     *
     * @return the prefix
     */
    String getPrefix();

    /**
     * Gets implementation class.
     * 获取实现类。
     * @return the implementation class
     */
    Class<? extends Ticket> getImplementationClass();

    /**
     * Gets properties.
     *
     * @return the properties
     */
    TicketDefinitionProperties getProperties();

    /**
     * Returns order/priority associated with this definition.
     * Typically used in collection sorting and compare operations.
     * @return the order.
     */
    @Override
    default int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
