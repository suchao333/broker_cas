package org.apereo.cas.ticket;

/**
 * This is {@link TicketDefinitionProperties}. Ticket definition describes additional Properties and misc settings
 * that may be associated with a given ticket to be used by registries. Each CAS module on start up
 * has the ability to register/alter ticket metadata that may be requires for its own specific functionality.
 * Given each CAS module may decide to create many forms of tickets, this facility is specifically provided
 * to dynamically register ticket types and associated properties so modules that deal with registry functionality
 * wouldn't have to statically link to all modules and APIs.
 *
 * Ticket definition properties are intended to be treated as generally as possible, so common settings
 * can be shared across all modules that may have similar needs. When adding additional properties, be careful
 * to not tie the setting to a specific technology or terminology, and opt for generality as much as possible.
 *是{@link TicketDefinitionProperties}。票据定义描述其他属性和其他设置
 *它可能与注册中心使用的给定票证相关联。启动时的每个CAS模块
 *具有注册/更改票证元数据的能力，这些元数据可能是其自身特定功能所需的。
 *考虑到每个CAS模块可能决定创建多种形式的票据，因此专门提供了这种工具
 *动态注册票证类型和相关属性，以便处理注册表功能的模块
 *不需要静态链接到所有模块和api。
 *票证定义属性应尽可能地被视为一般属性，因此常用设置
 *可以在所有可能有类似需求的模块之间共享。添加其他属性时，请小心
 *不要将设置与特定的技术或术语联系起来，尽可能多地选择通用性。
 * @author Misagh Moayyed
 * @see TicketCatalog
 * @since 5.1.0
 */
public interface TicketDefinitionProperties {

    /**
     * Generically describes if this ticket is linked to all ticket entities
     * such that for normal CRUD operations, cascades may be required.
     **一般描述此票证是否链接到所有票证实体
     * *这样，对于正常CRUD操作，可能需要级联。
     * @return true /false
     */
    boolean isCascade();

    /**
     * Sets cascade ticket.
     *
     * @param cascadeTicket the cascade ticket
     */
    void setCascade(boolean cascadeTicket);

    /**
     * Generic cache/storage name this ticket may want to associate with itself
     * in cases where persistence is handled by an underlying cache, etc.
     *通用缓存/存储名称此票证可能要与其自身关联
     *如果持久性是由底层缓存处理的，等等。
     * @return the cache name
     */
    String getStorageName();

    /**
     * Sets store name.
     *
     * @param storageName the cache name
     */
    void setStorageName(String storageName);

    /**
     * Describes how long may this ticket definition
     * exist in the underlying storage unit. For cache-based storage
     * services, this may translate to idle/max time-to-live, etc.
     * 描述此票证定义的时间
     * 存在于底层存储单元中。对于基于缓存的存储
     * 服务，这可能转化为空闲/最长生存时间等。
     * @return the long
     */
    long getStorageTimeout();

    /**
     * Sets store password if any.
     *
     * @param psw the password for the storage.
     */
    void setStoragePassword(String psw);

    /**
     * Describes the credential required to access the storage, if any.
     * @return the psw
     */
    String getStoragePassword();

    /**
     * Sets cache timeout.
     *
     * @param timeout the cache timeout
     */
    void setStorageTimeout(long timeout);

}
