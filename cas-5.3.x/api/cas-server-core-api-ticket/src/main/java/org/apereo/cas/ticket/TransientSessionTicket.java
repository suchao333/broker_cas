package org.apereo.cas.ticket;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apereo.cas.authentication.principal.Service;

import java.io.Serializable;
import java.util.Map;

/**
 * This is {@link TransientSessionTicket} that allows CAS to use the ticket registry
 * as a distributed session store for short-lived non-specific objects. The intention
 * of this ticket is to encapsulate objects that would otherwise be tracked by the application
 * container's session. By using the ticket registry as a generic session store, all operations
 * that may require session awareness specially in a clustered environment can be freed from
 * that requirement.
 * <p>
 * Note that objects/values put into the session ticket are required to be serializable,
 * just as normal ticket properties would be, depending on the design of the underlying ticket registry.
 *
 * Transient tickets generally have prominent use when CAS is acting as a proxy to another identity provider
 * where the results of current application session/request need to be stored across the cluster and remembered later.
 **这是{@link TransientSessionTicket}，它允许CAS使用票证注册表
作为短期非特定对象的分布式会话存储。目的
这个票证是封装应用程序跟踪的对象
容器的会话。通过使用票证注册表作为通用会话存储，所有操作
这可能需要会话感知，特别是在集群环境中，可以从
这个要求。
<p>
请注意，放入会话票证的对象/值必须是可序列化的，
就像正常的票证属性一样，这取决于底层票证注册表的设计。
当ca充当另一个身份提供者的代理时，临时票证通常有显著的用途
当前应用程序会话/请求的结果需要跨集群存储并在以后记住。
 * @author Misagh Moayyed
 * @since 5.3.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface TransientSessionTicket extends Ticket {
    /**
     * Ticket prefix for the delegated authentication request.
     */
    String PREFIX = "TST";

    /**
     * Gets properties.
     *
     * @return the properties
     */
    Map<String, Serializable> getProperties();

    /**
     * Gets service.
     *
     * @return the service
     */
    Service getService();

    /**
     * Put property.
     *
     * @param name  the name
     * @param value the value
     */
    void put(String name, Serializable value);

    /**
     * Put all properties.
     *
     * @param props the props
     */
    void putAll(Map<String, Serializable> props);

    /**
     * Contains property boolean.
     *
     * @param name the name
     * @return the boolean
     */
    boolean contains(String name);

    /**
     * Gets property.
     *
     * @param <T>   the type parameter
     * @param name  the name
     * @param clazz the clazz
     * @return the property
     */
    <T extends Serializable> T get(String name, Class<T> clazz);

    /**
     * Gets property.
     *
     * @param <T>          the type parameter
     * @param name         the name
     * @param clazz        the clazz
     * @param defaultValue the default value
     * @return the property
     */
    <T extends Serializable> T get(String name, Class<T> clazz, T defaultValue);
}
