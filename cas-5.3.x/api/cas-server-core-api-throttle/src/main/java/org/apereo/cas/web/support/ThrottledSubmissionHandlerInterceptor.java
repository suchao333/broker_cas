package org.apereo.cas.web.support;

import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This is {@link ThrottledSubmissionHandlerInterceptor}.
 * 提交处理程序限流拦截器
 * @author Misagh Moayyed
 * @since 5.0.0
 */
public interface ThrottledSubmissionHandlerInterceptor extends AsyncHandlerInterceptor {

    /**
     * Record submission（n 提交） failure.
     *
     * @param request the request
     */
    default void recordSubmissionFailure(final HttpServletRequest request) {
    }

    /**
     * Determine whether threshold（阀值） has been exceeded（超过）.
     * 确定是否已超过阈值。
     * @param request the request
     * @return true, if successful
     */
    default boolean exceedsThreshold(final HttpServletRequest request) {
        return false;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    default String getName() {
        return getClass().getSimpleName();
    }

    /**
     * Decrement the the throttle so authentication can resume（从新开始）.
     * 减小油门，以便继续身份验证。
     */
    default void decrement() {
    }

    @Override
    default boolean preHandle(final HttpServletRequest request,
                              final HttpServletResponse response,
                              final Object handler) throws Exception {
        return true;
    }

    @Override
    default void postHandle(final HttpServletRequest request,
                            final HttpServletResponse response,
                            final Object handler,
                            final ModelAndView modelAndView) {
    }

    @Override
    default void afterConcurrentHandlingStarted(final HttpServletRequest httpServletRequest,
                                                final HttpServletResponse httpServletResponse, final Object o) throws Exception {
    }

    @Override
    default void afterCompletion(final HttpServletRequest httpServletRequest,
                                 final HttpServletResponse httpServletResponse,
                                 final Object o,
                                 final Exception e) throws Exception {
    }
}
