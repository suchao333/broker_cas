package org.apereo.cas.configuration.model.support.mfa;

import org.apereo.cas.configuration.support.RequiresModule;
import org.apereo.cas.configuration.support.RestEndpointProperties;
import org.apereo.cas.configuration.support.SpringResourceProperties;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link MultifactorAuthenticationProviderBypassProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-core-authentication", automated = true)

@Getter
@Setter
public class MultifactorAuthenticationProviderBypassProperties implements Serializable {

    private static final long serialVersionUID = -9181362378365850397L;

    public enum MultifactorProviderBypassTypes {

        /**
         * Handle multifactor authentication bypass per default CAS rules.
         * 根据默认CAS规则处理多因素身份验证绕过。
         */
        DEFAULT, /**
         * Handle multifactor authentication bypass via a Groovy script.
         * 通过Groovy脚本处理多因素身份验证绕过。
         */
        GROOVY, /**
         * Handle multifactor authentication bypass via a REST endpoint.
         * 通过REST端点处理多因素身份验证绕过。
         */
        REST
    }

    /**
     * Acceptable values are:
     * <ul>
     *     <li>{@code DEFAULT}: Default bypass rules to skip provider via attributes, etc.</li>
     *     <li>{@code GROOVY}: Handle bypass decisions via a groovy script.</li>
     *     <li>{@code REST}: Handle bypass rules via a REST endpoint</li>
     * </ul>
     */
    private MultifactorProviderBypassTypes type = MultifactorProviderBypassTypes.DEFAULT;

    /**
     * Skip multifactor authentication based on designated principal attribute names.
     * 跳过基于指定的主属性名的多因素身份验证。
     */
    private String principalAttributeName;

    /**
     * Optionally, skip multifactor authentication based on designated principal attribute values.
     * 或者，根据指定的主属性值跳过多因素身份验证。
     */
    private String principalAttributeValue;

    /**
     * Skip multifactor authentication based on designated authentication attribute names.
     * 基于指定的身份验证属性名称跳过多因素身份验证。
     */
    private String authenticationAttributeName;

    /**
     * Optionally, skip multifactor authentication based on designated authentication attribute values.
     * （可选）根据指定的身份验证属性值跳过多因素身份验证。
     */
    private String authenticationAttributeValue;

    /**
     * Skip multifactor authentication depending on form of primary authentication execution.
     * Specifically, skip multifactor if the a particular authentication handler noted by its name
     * successfully is able to authenticate credentials in the primary factor.
     * 根据主身份验证执行的形式跳过多因素身份验证。
     * 具体地说，如果某个特定的身份验证处理程序以其名称标记，则跳过multifactor
     * 成功地能够对主要因素中的凭据进行身份验证。
     */
    private String authenticationHandlerName;

    /**
     * Skip multifactor authentication depending on method/form of primary authentication execution.
     * Specifically, skip multifactor if the authentication method attribute collected as part of
     * authentication metadata matches a certain value.
     * 根据主身份验证执行的方法/形式跳过多因素身份验证。
     * 特别是，如果身份验证方法属性作为
     * 身份验证元数据与某个值匹配。
     */
    private String authenticationMethodName;

    /**
     * Skip multifactor authentication depending on form of primary credentials.
     * Value must equal the fully qualified class name of the credential type.
     * *根据主凭据的形式跳过多因素身份验证。值必须等于凭据类型的完全限定类名。
     */
    private String credentialClassType;

    /**
     * Skip multifactor authentication if the http request's remote address or host
     * matches the value defined here. The value may be specified as a regular expression.
     * 如果http请求的远程地址或主机
     * 与此处定义的值匹配。该值可以指定为正则表达式。
     */
    private String httpRequestRemoteAddress;

    /**
     * Skip multifactor authentication if the http request contains the defined header names.
     * Header names may be comma-separated and can be regular expressions; values are ignored.
     * 如果http请求包含已定义的头名称，则跳过多因素身份验证。
     * 标头名称可以用逗号分隔，也可以是正则表达式；值将被忽略。
     */
    private String httpRequestHeaders;

    /**
     * Handle bypass using a Groovy resource.
     * 使用Groovy资源处理旁路。
     */
    private Groovy groovy = new Groovy();

    /**
     * Handle bypass using a REST endpoint.
     * 使用REST端点处理旁路。
     */
    private Rest rest = new Rest();

    @RequiresModule(name = "cas-server-core-authentication", automated = true)
    @Getter
    @Setter
    public static class Groovy extends SpringResourceProperties {

        private static final long serialVersionUID = 8079027843747126083L;
    }

    @RequiresModule(name = "cas-server-core-authentication", automated = true)
    @Getter
    @Setter
    public static class Rest extends RestEndpointProperties {

        private static final long serialVersionUID = 1833594332973137011L;
    }
}
