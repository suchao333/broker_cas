package org.apereo.cas.configuration.model.support.oauth;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link OAuthAccessTokenProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-oauth")

@Getter
@Setter
public class OAuthAccessTokenProperties implements Serializable {

    private static final long serialVersionUID = -6832081675586528350L;

    /**
     * Hard timeout to kill the access token and expire it.
     * 硬超时杀死访问令牌并使其过期。
     */
    private String maxTimeToLiveInSeconds = "PT28800S";

    /**
     * Sliding window for the access token expiration policy.
     * Essentially, this is an idle time out.
     * *滑动窗口用于访问令牌过期策略。
     * 本质上，这是一个空闲时间。
     *
     */
    private String timeToKillInSeconds = "PT7200S";

    /**
     * Whether CAS authentication/protocol attributes
     * should be released as part of this access token's validation.
     * *是否有CAS认证/协议属性
     * *应该作为访问令牌验证的一部分被释放。
     */
    private boolean releaseProtocolAttributes = true;
}
