package org.apereo.cas.configuration.model.support.oauth;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link OAuthGrantsProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-oauth")

@Getter
@Setter
public class OAuthGrantsProperties implements Serializable {

    private static final long serialVersionUID = -2246860215082703251L;

    /**
     * Resource owner grant settings.
     * 资源所有者授予设置。
     */
    private ResourceOwner resourceOwner = new ResourceOwner();

    @RequiresModule(name = "cas-server-support-oauth")
    @Getter
    @Setter
    public static class ResourceOwner implements Serializable {

        private static final long serialVersionUID = 3171206304518294330L;

        /**
         * Whether using the resource-owner grant should
         * enforce authorization rules and per-service policies
         * based on a service parameter is provided as a header
         * outside the normal semantics of the grant and protocol.
         * *是否应该使用资源所有者授权
         * *实施授权规则和每个服务策略
         * *基于服务参数作为头提供
         * *超出授权和协议的正常语义。
         */
        private boolean requireServiceHeader;
    }
}
