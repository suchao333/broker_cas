package org.apereo.cas.configuration.model.support.consent;

import org.apereo.cas.configuration.model.core.util.EncryptionJwtSigningJwtCryptographyProperties;
import org.apereo.cas.configuration.model.support.jpa.AbstractJpaProperties;
import org.apereo.cas.configuration.model.support.ldap.AbstractLdapSearchProperties;
import org.apereo.cas.configuration.model.support.mongo.SingleCollectionMongoDbProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.apereo.cas.configuration.support.SpringResourceProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link ConsentProperties}.
 *  consent 批准，同意
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-consent-webflow")
@Getter
@Setter
public class ConsentProperties implements Serializable {

    private static final long serialVersionUID = 5201308051524438384L;

    /**
     * Global reminder time unit, to reconfirm consent
     * in cases no changes are detected.
     * *全局提醒时间单位，用于再次确认同意
     * 如果没有检测到任何变化。
     */
    private int reminder = 30;

    /**
     * Global reminder time unit of measure, to reconfirm consent
     * in cases no changes are detected.
     * 全局提醒时间度量单位，用于再次确认同意
     * 如果没有检测到任何变化。
     */
    private ChronoUnit reminderTimeUnit = ChronoUnit.DAYS;

    /**
     * Keep consent decisions stored via REST.
     * 通过REST保存同意决定。
     */
    private Rest rest = new Rest();

    /**
     * Keep consent decisions stored via LDAP user records.
     * 通过LDAP用户记录保存同意决定。
     */
    private Ldap ldap = new Ldap();

    /**
     * Keep consent decisions stored via JDBC resources.
     * 通过JDBC资源保存同意决定。
     */
    private Jpa jpa = new Jpa();

    /**
     * Keep consent decisions stored via a static JSON resource.
     * 通过静态JSON资源保存同意决定。
     */
    private Json json = new Json();

    /**
     * Keep consent decisions stored via a Groovy resource.
     * 通过Groovy资源保存同意决定。
     */
    private Groovy groovy = new Groovy();

    /**
     * Keep consent decisions stored via a MongoDb database resource.
     * 通过MongoDb数据库资源保存同意决定。
     */
    private MongoDb mongo = new MongoDb();

    /**
     * Signing/encryption settings.
     * 签名/加密设置。
     */
    @NestedConfigurationProperty
    private EncryptionJwtSigningJwtCryptographyProperties crypto = new EncryptionJwtSigningJwtCryptographyProperties();

    @RequiresModule(name = "cas-server-consent-webflow")
    @Getter
    @Setter
    public static class Json extends SpringResourceProperties {

        private static final long serialVersionUID = 7079027843747126083L;
    }

    @RequiresModule(name = "cas-server-consent-webflow")
    @Getter
    @Setter
    public static class Groovy extends SpringResourceProperties {

        private static final long serialVersionUID = 7079027843747126083L;
    }

    @RequiresModule(name = "cas-server-consent-jdbc")
    @Getter
    @Setter
    public static class Jpa extends AbstractJpaProperties {

        private static final long serialVersionUID = 1646689616653363554L;
    }

    @RequiresModule(name = "cas-server-consent-ldap")
    @Getter
    @Setter
    public static class Ldap extends AbstractLdapSearchProperties {

        private static final long serialVersionUID = 1L;

        /**
         * Type of LDAP directory.
         */
        private LdapType type;

        /**
         * Name of LDAP attribute that holds consent decisions as JSON.
         */
        private String consentAttributeName = "casConsentDecision";
    }

    @RequiresModule(name = "cas-server-consent-mongo")
    @Getter
    @Setter
    public static class MongoDb extends SingleCollectionMongoDbProperties {

        private static final long serialVersionUID = -1918436901491275547L;

        public MongoDb() {
            setCollection("MongoDbCasConsentRepository");
        }
    }

    @RequiresModule(name = "cas-server-consent-rest")
    @Getter
    @Setter
    public static class Rest implements Serializable {

        private static final long serialVersionUID = -6909617495470495341L;

        /**
         * REST endpoint to use to which consent decision records will be submitted.
         */
        private String endpoint;
    }
}
