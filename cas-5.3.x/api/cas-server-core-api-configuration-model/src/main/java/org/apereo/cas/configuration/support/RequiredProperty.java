package org.apereo.cas.configuration.support;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This is {@link RequiredProperty} that is put on top of a CAS property/field
 * to indicate the presence of the field is required for CAS to function correctly
 * and/or to recognize the existence of an enabled feature, etc.
 **这是放在CAS属性/字段顶部的{@link RequiredProperty}
 * *指出字段的存在是CAS正常运行所必需的
 * *和/或识别已启用特性的存在等。
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RequiredProperty {
    /**
     * The message associated with this required property.
     * Might want to explain caveats and fallbacks to defaults.
     *
     * @return the msg
     */
    String message() default "The property is required";
}
