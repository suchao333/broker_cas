package org.apereo.cas.configuration.model.support.oauth;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link OAuthCodeProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-oauth")

@Getter
@Setter
public class OAuthCodeProperties implements Serializable {

    private static final long serialVersionUID = -7687928082301669359L;

    /**
     * Number of times this code is valid and can be used.
     * 此代码有效并可使用的次数。
     */
    private int numberOfUses = 1;

    /**
     * Duration in seconds where the code is valid.
     * 持续时间(以秒为单位)，其中代码有效。
     */
    private long timeToKillInSeconds = 30;
}
