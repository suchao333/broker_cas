package org.apereo.cas.configuration.model.support.jdbc;

import org.apereo.cas.configuration.model.core.authentication.PasswordEncoderProperties;
import org.apereo.cas.configuration.model.core.authentication.PrincipalTransformationProperties;
import org.apereo.cas.configuration.model.support.jpa.AbstractJpaProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.apereo.cas.configuration.support.RequiredProperty;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link QueryEncodeJdbcAuthenticationProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-jdbc-authentication")

@Getter
@Setter
public class QueryEncodeJdbcAuthenticationProperties extends AbstractJpaProperties {

    private static final long serialVersionUID = -6647373426301411768L;

    /**
     * A number of authentication handlers are allowed to determine whether they can operate on the provided credential
     * and as such lend themselves to be tried and tested during the authentication handler selection phase.
     * The credential criteria may be one of the following options:<ul>
     * <li>1) A regular expression pattern that is tested against the credential identifier.</li>
     * <li>2) A fully qualified class name of your own design that implements {@code Predicate<Credential>}.</li>
     * <li>3) Path to an external Groovy script that implements the same interface.</li>
     * </ul>
     * *允许多个身份验证处理程序来确定它们是否可以对所提供的凭据进行操作
     *
     * *因此，可以在身份验证处理程序选择阶段进行尝试和测试。
     *
     * *凭证条件可以是以下选项之一：<ul>
     *
     * *<li>1）根据凭证标识符进行测试的正则表达式模式</li>
     *
     * *<li>2）您自己设计的实现{@code Predicate<Credential>}的完全限定类名</li>
     *
     * *<li>3）实现相同接口的外部Groovy脚本的路径</li>
     *
     * *</ul>
     */
    private String credentialCriteria;

    /**
     * Algorithm used for hashing.
     */
    private String algorithmName;

    /**
     * SQL query to execute and look up accounts.
     * Example: {@code SELECT * FROM table WHERE username=?}.
     */
    @RequiredProperty
    private String sql;

    /**
     * Password column name.
     */
    private String passwordFieldName = "password";

    /**
     * Field/column name that indicates the salt used for password hashing.
     * 字段/列名，指示用于密码哈希的salt。
     */
    @RequiredProperty
    private String saltFieldName = "salt";

    /**
     * Column name that indicates whether account is expired.
     * 指示帐户是否过期的列名。
     */
    private String expiredFieldName;

    /**
     * Column name that indicates whether account is disabled.
     * 指示帐户是否已禁用的列名。
     */
    private String disabledFieldName;

    /**
     * Field/column name that indicates the number of iterations used for password hashing.
     * 字段/列名，指示用于密码哈希的迭代次数。
     */
    private String numberOfIterationsFieldName = "numIterations";

    /**
     * Default number of iterations for hashing.
     * 哈希的默认迭代次数。
     */
    private long numberOfIterations;

    /**
     * Static salt to be used for hashing.
     * 用于哈希的静态盐。
     */
    private String staticSalt;

    /**
     * Name of the authentication handler.
     */
    private String name;

    /**
     * Order of the authentication handler in the chain.
     * 链中身份验证处理程序的顺序。
     */
    private int order = Integer.MAX_VALUE;

    /**
     * Principal transformation settings for this authentication.
     * 此身份验证的主体转换设置。
     */
    @NestedConfigurationProperty
    private PrincipalTransformationProperties principalTransformation = new PrincipalTransformationProperties();

    /**
     * Password encoding strategies for this authentication.
     * 此身份验证的密码编码策略。
     */
    @NestedConfigurationProperty
    private PasswordEncoderProperties passwordEncoder = new PasswordEncoderProperties();
}
