package org.apereo.cas.configuration.model.core.sso;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * Configuration properties class for {@code create.sso}.
 *
 * @author Dmitriy Kopylenko
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-authentication", automated = true)
@Getter
@Setter
public class SsoProperties implements Serializable {

    private static final long serialVersionUID = -8777647966370741733L;

    /**
     * Flag that indicates whether to create SSO session on re-newed authentication event.
     */
    private boolean createSsoCookieOnRenewAuthn = true;

    /**
     * Flag that indicates whether to allow SSO session with a missing target service.
     * 指示是否允许与缺少目标服务的SSO会话的标志。
     */
    private boolean allowMissingServiceParameter = true;

    /**
     * Indicates whether CAS proxy authentication/tickets
     * are supported by this server implementation.
     * 指示CAS代理身份验证/票证
     *
     * *此服务器实现支持。
     */
    private boolean proxyAuthnEnabled = true;

    /**
     *  Indicates whether this server implementation should globally
     *  support CAS protocol authentication requests that are tagged with "renew=true".
     *  指示此服务器实现是否应全局
     *
     * 支持标记为“renew=true”的CAS协议身份验证请求。
     */
    private boolean renewAuthnEnabled = true;
}
