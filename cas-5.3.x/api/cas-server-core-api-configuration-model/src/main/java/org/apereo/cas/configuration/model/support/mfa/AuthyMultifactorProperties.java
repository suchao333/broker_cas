package org.apereo.cas.configuration.model.support.mfa;

import org.apereo.cas.configuration.support.RequiresModule;
import org.apereo.cas.configuration.support.RequiredProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link AuthyMultifactorProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-authy")

@Getter
@Setter
public class AuthyMultifactorProperties extends BaseMultifactorProviderProperties {

    /**
     * Provider id by default.
     * 默认情况下提供程序id。
     */
    public static final String DEFAULT_IDENTIFIER = "mfa-authy";

    private static final long serialVersionUID = -3746749663459157641L;

    /**
     * Authy API key.
     * 验证API密钥。
     */
    @RequiredProperty
    private String apiKey;

    /**
     * Authy API url.
     * 身份验证API url。
     */
    @RequiredProperty
    private String apiUrl;

    /**
     * Principal attribute used to look up a phone number
     * for credential verification. The attribute value
     * is then used to look up the user record in Authy, or
     * create the user.
     * 用于查找电话号码的主属性
     * 用于凭证验证。属性值
     * 然后用于在Authy中查找用户记录，或者
     * 创建用户。
     */
    @RequiredProperty
    private String phoneAttribute = "phone";

    /**
     * Principal attribute used to look up an email address
     * for credential verification. The attribute value
     * is then used to look up the user record in Authy, or
     * create the user.
     * 用于查找电子邮件地址的主体属性
     * 用于凭证验证。属性值
     * 然后用于在Authy中查找用户记录，或者
     * 创建用户。
     */
    @RequiredProperty
    private String mailAttribute = "mail";

    /**
     * Phone number country code used to look up and/or create the Authy user account.
     * 用于查找和/或创建身份验证用户帐户的电话号码国家/地区代码。
     */
    private String countryCode = "1";

    /**
     * Flag authentication requests to authy to force verification of credentials.
     * 标记身份验证请求以强制验证凭据。
     */
    private boolean forceVerification = true;

    /**
     * Indicates whether this provider should support trusted devices.
     * 指示此提供程序是否应支持受信任的设备。
     */
    private boolean trustedDeviceEnabled;

    public AuthyMultifactorProperties() {
        setId(DEFAULT_IDENTIFIER);
    }
}
