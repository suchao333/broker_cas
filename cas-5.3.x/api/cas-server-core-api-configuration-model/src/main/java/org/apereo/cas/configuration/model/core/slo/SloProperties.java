package org.apereo.cas.configuration.model.core.slo;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link SloProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-authentication", automated = true)
@Getter
@Setter
public class SloProperties implements Serializable {

    private static final long serialVersionUID = 3676710533477055700L;

    /**
     * Whether SLO callbacks should be done in an asynchronous manner via the HTTP client.
     * When true, CAS will not wait for the operation to fully complete and will resume control to carry on.
     * SLO回调是否应该通过HTTP客户端以异步方式完成。
     * 当为true时，CAS将不等待操作完全完成，并将恢复控制继续进行。
     */
    private boolean asynchronous = true;

    /**
     * Whether SLO should be entirely disabled globally for the CAS deployment.
     * 对于CAS部署，是否应全局禁用SLO。
     */
    private boolean disabled;
}
