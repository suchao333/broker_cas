package org.apereo.cas.configuration.model.support.cookie;

import org.apache.commons.lang3.StringUtils;
import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * Common properties for all cookie configs.
 *
 * @author Dmitriy Kopylenko
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-cookie", automated = true)
@Getter
@Setter
public class CookieProperties implements Serializable {

    private static final long serialVersionUID = 6804770601645126835L;

    /**
     * Cookie name. Constructs a cookie with a specified name and value.
     * The name must conform to RFC 2965. That means it can contain only ASCII alphanumeric characters and
     * cannot contain commas, semicolons, or white space or begin with a $ character.
     * The cookie's name cannot be changed after creation.
     * By default, cookies are created according to the RFC 2965 cookie specification.
     * *Cookie名称。构造具有指定名称和值的cookie。
     *
     * *名称必须符合RFC 2965。这意味着它只能包含ASCII字母数字字符和
     *
     * *不能包含逗号、分号或空格，也不能以$字符开头。
     *
     * *创建后无法更改cookie的名称。
     *
     * *默认情况下，cookies是根据rfc2965 cookie规范创建的。
     */
    private String name;

    /**
     * Cookie path.
     * Specifies a path for the cookie to which the client should return the cookie.
     * The cookie is visible to all the pages in the directory you specify, and all the pages in that directory's
     * subdirectories. A cookie's path must include the servlet that set the cookie, for example, /catalog,
     * which makes the cookie visible to all directories on the server under /catalog.
     * Consult RFC 2965 (available on the Internet) for more information on setting path names for cookies.
     *Cookie路径。

     *指定客户端应将cookie返回到的cookie的路径。

     *cookie对指定目录中的所有页以及该目录中的所有页都可见

     *子目录。cookie的路径必须包含设置cookie的servlet，例如/catalog，

     *这将使cookie对服务器上/catalog下的所有目录可见。

     *有关为cookies设置路径名的详细信息，请参阅rfc2965（可在互联网上找到）。
     */
    private String path = StringUtils.EMPTY;

    /**
     * Cookie domain. Specifies the domain within which this cookie should be presented.
     * The form of the domain name is specified by RFC 2965. A domain name begins with a dot (.foo.com)
     * and means that the cookie is visible to servers in a
     * specified Domain Name System (DNS) zone (for example, www.foo.com, but not a.b.foo.com).
     * By default, cookies are only returned to the server that sent them.
     * *Cookie域。指定应在其中显示此cookie的域。
     *
     * *域名的形式由RFC2965指定。域名以点（）开头。foo.com网站)
     *
     * *意味着cookie对服务器可见
     *
     * *指定的域名系统（DNS）区域（例如，www.foo.com网站但不是a.b。foo.com网站).
     *
     * *默认情况下，cookies只返回给发送它们的服务器。
     */
    private String domain = StringUtils.EMPTY;

    /**
     * True if sending this cookie should be restricted to a secure protocol, or false if the it can be sent using any protocol.
     * 如果发送此cookie应限制为安全协议，则为True；如果可以使用任何协议发送，则为false。
     */
    private boolean secure = true;

    /**
     * true if this cookie contains the HttpOnly attribute. This means that the cookie should not be accessible to scripting engines, like javascript.
     * 如果此cookie包含HttpOnly属性，则为true。这意味着cookie不应该被脚本引擎（比如javascript）访问。
     */
    private boolean httpOnly = true;

    /**
     * The maximum age of the cookie, specified in seconds. By default, -1 indicating the cookie will persist until browser shutdown.
     * A positive value indicates that the cookie will expire after that many seconds have passed. Note that the value is
     * the maximum age when the cookie will expire, not the cookie's current age.
     * A negative value means that the cookie is not stored persistently and will be deleted when the Web browser exits.
     * A zero value causes the cookie to be deleted.
     * *cookie的最长期限，以秒为单位指定。默认情况下，-1表示cookie将持续到浏览器关闭。
     *
     * *正值表示cookie将在该秒数之后过期。请注意，该值为
     *
     * *cookie将过期的最长期限，而不是cookie的当前期限。
     *
     * *负值表示cookie不会永久存储，当Web浏览器退出时将被删除。
     *
     * *零值会导致cookie被删除。
     */
    private int maxAge = -1;

    /**
     * When generating cookie values, determine whether the value
     * should be compounded and signed with the properties of
     * the current session, such as IP address, user-agent, etc.
     生成cookie值时，确定是否应将该值与的属性复合并签名当前会话，如IP地址、用户代理等。
     */
    private boolean pinToSession = true;
}
