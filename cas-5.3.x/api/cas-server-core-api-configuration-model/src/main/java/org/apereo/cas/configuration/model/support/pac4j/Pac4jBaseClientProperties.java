package org.apereo.cas.configuration.model.support.pac4j;

import lombok.Getter;
import lombok.Setter;
import org.apereo.cas.configuration.support.RequiresModule;

import java.io.Serializable;

/**
 * This is {@link Pac4jBaseClientProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.3.0
 */
@RequiresModule(name = "cas-server-support-pac4j-webflow")
//https://blog.csdn.net/change_on/article/details/76302161
//pac4j是一个简单而强大的安全引擎，用于Java对用户进行身份验证、
// 获取其配置文件和管理授权，以确保web应用程序安全。
// 它提供了一套完整的概念和组件。它基于Java 8，并在Apache 2许可下使用。
// 它可用于大多数框架/工具和支持大多数认证/授权机制。
@Getter
@Setter
public class Pac4jBaseClientProperties implements Serializable {

    private static final long serialVersionUID = -7885975876831784206L;
    
    /**
     * Name of the client mostly for UI purposes and uniqueness.
     * This name, with 'non-word' characters converted to '-' (e.g. "This Org (New)" becomes "This-Org--New-")
     * is added to the "class" attribute of the redirect link on the login page, to allow for
     * custom styling of individual IdPs (e.g. for an organization logo).
     */
    private String clientName;
    
    /**
     * Auto-redirect to this client.
     */
    private boolean autoRedirect;

    /**
     * Create a callback url with the clientId in the path instead of in the querystring.
     */
    private boolean usePathBasedCallbackUrl;

    /**
     * The attribute to use as the principal identifier built during and upon a successful authentication attempt.
     */
    private String principalAttributeId;
}
