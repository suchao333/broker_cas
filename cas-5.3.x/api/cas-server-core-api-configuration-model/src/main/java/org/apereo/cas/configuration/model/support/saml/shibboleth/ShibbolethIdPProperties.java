package org.apereo.cas.configuration.model.support.saml.shibboleth;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link ShibbolethIdPProperties}.
 *Shibboleth是一个针对SSO的开源项目
 * @author Misagh Moayyed
 * @since 5.1.0
 */
@RequiresModule(name = "cas-server-support-shibboleth")

@Getter
@Setter
public class ShibbolethIdPProperties implements Serializable {

    private static final long serialVersionUID = 1741075420882227768L;

    /**
     * The server url of the shibboleth idp deployment.
     */
    private String serverUrl;
}
