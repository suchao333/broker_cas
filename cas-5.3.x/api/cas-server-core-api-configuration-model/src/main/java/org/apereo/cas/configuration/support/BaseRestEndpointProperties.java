package org.apereo.cas.configuration.support;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * This is {@link BaseRestEndpointProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */

@Getter
@Setter
public class BaseRestEndpointProperties implements Serializable {
    private static final long serialVersionUID = 2687020856160473089L;

    /**
     * The endpoint URL to contact and retrieve（获取索取，检索） attributes.
     * 用于联系和检索属性的终结点URL
     */
    @RequiredProperty
    private String url;

    /**
     * If REST endpoint is protected via basic authentication,
     * specify the username for authentication.
     * *如果REST端点通过基本身份验证得到保护，
     *
     * 指定身份验证的用户名。
     */
    private String basicAuthUsername;
    /**
     * If REST endpoint is protected via basic authentication,
     * specify the password for authentication.
     *如果REST端点通过基本身份验证得到保护，
     *指定身份验证的密码。
     */
    private String basicAuthPassword;
}
