package org.apereo.cas.configuration.model.support.trusted;

import org.apereo.cas.configuration.model.core.authentication.PersonDirectoryPrincipalResolverProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link TrustedAuthenticationProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-support-trusted-webflow")
//授信
@Getter
@Setter
public class TrustedAuthenticationProperties extends PersonDirectoryPrincipalResolverProperties {

    private static final long serialVersionUID = 279410895614233349L;

    /**
     * Indicates the name of the request header that may be extracted from the request
     * as the indicated authenticated userid from the remote authn system.
     * 指示可从请求中提取的请求标头的名称
     * 作为远程身份验证系统中指定的已验证用户标识。
     */
    private String remotePrincipalHeader;

    /**
     * Indicates（表明） the name of the authentication handler.
     * 指示身份验证处理程序的名称。
     */
    private String name;
}
