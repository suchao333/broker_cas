package org.apereo.cas.configuration.model.support.jpa.ticketregistry;

import org.apereo.cas.configuration.model.core.util.EncryptionRandomizedSigningJwtCryptographyProperties;
import org.apereo.cas.configuration.model.support.jpa.AbstractJpaProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import javax.persistence.LockModeType;
import lombok.Getter;
import lombok.Setter;

/**
 * Common properties for jpa ticket reg.
 *
 * @author Dmitriy Kopylenko
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-support-jpa-ticket-registry")

@Getter
@Setter
public class JpaTicketRegistryProperties extends AbstractJpaProperties {

    /**
     * Default lock timeout is 1 hour.
     */
    public static final String DEFAULT_LOCK_TIMEOUT = "PT1H";

    private static final long serialVersionUID = -8053839523783801072L;

    /**
     * Ticket locking type. Acceptable values are
     * 票锁类型。可接受值为 读，写，乐观，乐观性增强，乐观读，乐观写，悲观写，悲观读，悲观性增强，无锁
     * {@code READ,WRITE,OPTIMISTIC,OPTIMISTIC_FORCE_INCREMENT,PESSIMISTIC_READ,
     * PESSIMISTIC_WRITE,PESSIMISTIC_FORCE_INCREMENT,NONE}.
     */
    private LockModeType ticketLockType = LockModeType.NONE;

    /**
     * Indicates the lock duration when one is about to be acquired by the cleaner.
     * 指示清洁器即将获取的锁定持续时间。
     */
    private String jpaLockingTimeout = DEFAULT_LOCK_TIMEOUT;

    /**
     * Crypto settings for the registry.
     * 注册表的加密设置。
     */
    @NestedConfigurationProperty
    private EncryptionRandomizedSigningJwtCryptographyProperties crypto = new EncryptionRandomizedSigningJwtCryptographyProperties();

    public JpaTicketRegistryProperties() {
        super.setUrl("jdbc:hsqldb:mem:cas-ticket-registry");
        this.crypto.setEnabled(false);
    }
}
