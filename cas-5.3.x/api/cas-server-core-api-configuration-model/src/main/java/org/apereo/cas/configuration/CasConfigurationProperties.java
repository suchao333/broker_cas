package org.apereo.cas.configuration;

import lombok.Getter;
import lombok.Setter;
import org.apereo.cas.configuration.model.core.CasJavaClientProperties;
import org.apereo.cas.configuration.model.core.CasServerProperties;
import org.apereo.cas.configuration.model.core.HostProperties;
import org.apereo.cas.configuration.model.core.audit.AuditProperties;
import org.apereo.cas.configuration.model.core.authentication.AuthenticationProperties;
import org.apereo.cas.configuration.model.core.authentication.HttpClientProperties;
import org.apereo.cas.configuration.model.core.authentication.PersonDirectoryPrincipalResolverProperties;
import org.apereo.cas.configuration.model.core.events.EventsProperties;
import org.apereo.cas.configuration.model.core.logout.LogoutProperties;
import org.apereo.cas.configuration.model.core.metrics.MetricsProperties;
import org.apereo.cas.configuration.model.core.monitor.MonitorProperties;
import org.apereo.cas.configuration.model.core.rest.RestProperties;
import org.apereo.cas.configuration.model.core.services.ServiceRegistryProperties;
import org.apereo.cas.configuration.model.core.slo.SloProperties;
import org.apereo.cas.configuration.model.core.sso.SsoProperties;
import org.apereo.cas.configuration.model.core.standalone.StandaloneConfigurationProperties;
import org.apereo.cas.configuration.model.core.util.TicketProperties;
import org.apereo.cas.configuration.model.core.web.MessageBundleProperties;
import org.apereo.cas.configuration.model.core.web.security.AdminPagesSecurityProperties;
import org.apereo.cas.configuration.model.core.web.security.HttpRequestProperties;
import org.apereo.cas.configuration.model.core.web.view.ViewProperties;
import org.apereo.cas.configuration.model.support.analytics.GoogleAnalyticsProperties;
import org.apereo.cas.configuration.model.support.aup.AcceptableUsagePolicyProperties;
import org.apereo.cas.configuration.model.support.captcha.GoogleRecaptchaProperties;
import org.apereo.cas.configuration.model.support.clearpass.ClearpassProperties;
import org.apereo.cas.configuration.model.support.consent.ConsentProperties;
import org.apereo.cas.configuration.model.support.cookie.TicketGrantingCookieProperties;
import org.apereo.cas.configuration.model.support.cookie.WarningCookieProperties;
import org.apereo.cas.configuration.model.support.custom.CasCustomProperties;
import org.apereo.cas.configuration.model.support.geo.googlemaps.GoogleMapsProperties;
import org.apereo.cas.configuration.model.support.geo.maxmind.MaxmindProperties;
import org.apereo.cas.configuration.model.support.interrupt.InterruptProperties;
import org.apereo.cas.configuration.model.support.jpa.DatabaseProperties;
import org.apereo.cas.configuration.model.support.saml.SamlCoreProperties;
import org.apereo.cas.configuration.model.support.saml.googleapps.GoogleAppsProperties;
import org.apereo.cas.configuration.model.support.saml.mdui.SamlMetadataUIProperties;
import org.apereo.cas.configuration.model.support.saml.sps.SamlServiceProviderProperties;
import org.apereo.cas.configuration.model.support.scim.ScimProperties;
import org.apereo.cas.configuration.model.support.sms.SmsProvidersProperties;
import org.apereo.cas.configuration.model.support.themes.ThemeProperties;
import org.apereo.cas.configuration.model.webapp.LocaleProperties;
import org.apereo.cas.configuration.model.webapp.WebflowProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.io.Serializable;

/**
 * This is {@link CasConfigurationProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@ConditionalOnMissingBean
//已cas开头，如果不能匹配就报错
@ConfigurationProperties(value = "cas", ignoreUnknownFields = false)
@Getter
@Setter
public class CasConfigurationProperties implements Serializable {
    /**
     * Prefix used for all CAS-specific settings.
     */
    public static final String PREFIX = "cas";

    private static final long serialVersionUID = -8620267783496071683L;

    /**
     * Interrupt/notification functionality.
     * 中断/通知功能。
     */
    @NestedConfigurationProperty
    private InterruptProperties interrupt = new InterruptProperties();

    /**
     * Attribute consent functionality.
     * 同意的功能属性。
     */
    @NestedConfigurationProperty
    private ConsentProperties consent = new ConsentProperties();

    /**
     * SCIM functionality.
     * SCIM功能。
     */
    @NestedConfigurationProperty
    private ScimProperties scim = new ScimProperties();

    /**
     * General settings for authentication.
     * 身份验证的一般设置。
     */
    @NestedConfigurationProperty
    private AuthenticationProperties authn = new AuthenticationProperties();

    /**
     * Authentication audit functionality.
     * 认证审计功能。
     */
    @NestedConfigurationProperty
    private AuditProperties audit = new AuditProperties();

    /**
     * Http client and outgoing connections settings.
     * Http客户端和传出连接设置。
     */
    @NestedConfigurationProperty
    private HttpClientProperties httpClient = new HttpClientProperties();

    /**
     * Person directory and principal resolution functionality.
     * 人员目录和主解析功能。
     */
    @NestedConfigurationProperty
    private PersonDirectoryPrincipalResolverProperties personDirectory = new PersonDirectoryPrincipalResolverProperties();

    /**
     * Authentication events functionality.
     * 验证事件的功能。
     */
    @NestedConfigurationProperty
    private EventsProperties events = new EventsProperties();

    /**
     * Settings that define this CAS host.
     */
    @NestedConfigurationProperty
    private HostProperties host = new HostProperties();

    /**
     * Logout functionality.
     * 定义此CAS主机的设置。
     */
    @NestedConfigurationProperty
    private LogoutProperties logout = new LogoutProperties();

    /**
     * Metrics functionality.
     * 度量功能
     */
    @NestedConfigurationProperty
    private MetricsProperties metrics = new MetricsProperties();

    /**
     * Monitoring functionality.
     * 监控
     */
    @NestedConfigurationProperty
    private MonitorProperties monitor = new MonitorProperties();

    /**
     * REST API functionality.
     *
     */
    @NestedConfigurationProperty
    private RestProperties rest = new RestProperties();

    /**
     * Settings that define this CAS server instance.
     * 定义此CAS服务器实例的设置。
     */
    @NestedConfigurationProperty
    private CasServerProperties server = new CasServerProperties();

    /**
     * Settings that configure the Java CAS client instance used internally for validation ops, etc.
     * 配置内部用于验证操作的Java CAS客户端实例的设置，等等。
     */
    @NestedConfigurationProperty
    private CasJavaClientProperties client = new CasJavaClientProperties();

    /**
     * Service registry functionality.
     * 服务注册中心的功能。
     */
    @NestedConfigurationProperty
    private ServiceRegistryProperties serviceRegistry = new ServiceRegistryProperties();

    /**
     * SLO functionality.
     * 单点退出
     */
    @NestedConfigurationProperty
    private SloProperties slo = new SloProperties();

    /**
     * SSO functionality.
     * 单点登录
     */
    @NestedConfigurationProperty
    private SsoProperties sso = new SsoProperties();

    /**
     * Ticketing functionality.
     * 票据  Credential是 某种身份的凭证
     * 2个概念不一样
     */
    @NestedConfigurationProperty
    private TicketProperties ticket = new TicketProperties();

    /**
     * Message bundles and internationalization functionality.
     * 消息包和国际化功能。
     */
    @NestedConfigurationProperty
    private MessageBundleProperties messageBundle = new MessageBundleProperties();

    /**
     * Admin pages and their security, controling endpoints, etc.
     * 管理页面及其安全性，控制端点等。
     */
    @NestedConfigurationProperty
    private AdminPagesSecurityProperties adminPagesSecurity = new AdminPagesSecurityProperties();

    /**
     * Settings that control filtering of the incoming http requests.
     * 控制过滤传入http请求的设置。
     */
    @NestedConfigurationProperty
    private HttpRequestProperties httpWebRequest = new HttpRequestProperties();

    /**
     * Views and UI functionality.
     * 视图和UI功能。
     */
    @NestedConfigurationProperty
    private ViewProperties view = new ViewProperties();

    /**
     * Google Analytics functionality.
     * 谷歌分析的功能。
     */
    @NestedConfigurationProperty
    private GoogleAnalyticsProperties googleAnalytics = new GoogleAnalyticsProperties();

    /**
     * Google reCAPTCHA settings.
     * 谷歌reCAPTCHA设置。
     */
    @NestedConfigurationProperty
    private GoogleRecaptchaProperties googleRecaptcha = new GoogleRecaptchaProperties();

    /**
     * SMS and Text messaging settings.
     * 短信和短信设置。
     */
    @NestedConfigurationProperty
    private SmsProvidersProperties smsProvider = new SmsProvidersProperties();

    /**
     * AUP settings.
     * 可接收使用策略
     */
    @NestedConfigurationProperty
    private AcceptableUsagePolicyProperties acceptableUsagePolicy = new AcceptableUsagePolicyProperties();

    /**
     * Clear pass settings.
     * 无障碍通行证
     * 系统与系统集成的时候使用，但是需要在服务器端保留用户密码存在安全隐患
     */
    @NestedConfigurationProperty
    private ClearpassProperties clearpass = new ClearpassProperties();

    /**
     * Ticket-granting cookie settings.
     * 票据授予 cookie 设置
     */
    @NestedConfigurationProperty
    private TicketGrantingCookieProperties tgc = new TicketGrantingCookieProperties();

    /**
     * Warning cookie settings.
     * 警告cookie设置。
     */
    @NestedConfigurationProperty
    private WarningCookieProperties warningCookie = new WarningCookieProperties();

    /**
     * SAML SP integration settings.
     * SAML SP集成设置。
     */
    @NestedConfigurationProperty
    private SamlServiceProviderProperties samlSp = new SamlServiceProviderProperties();

    /**
     * MaxMind settings.
     * 根据经度和纬度获取用户的地区信息
     */
    @NestedConfigurationProperty
    private MaxmindProperties maxmind = new MaxmindProperties();

    /**
     * Google Maps settings.
     * 谷歌地图配置
     */
    @NestedConfigurationProperty
    private GoogleMapsProperties googleMaps = new GoogleMapsProperties();

    /**
     * General database and hibernate settings.
     * 常规数据库和hibernate 设置
     */
    @NestedConfigurationProperty
    private DatabaseProperties jdbc = new DatabaseProperties();

    /**
     * Google Apps integration settings.
     * 谷歌应用程序集成设置。
     */
    @NestedConfigurationProperty
    private GoogleAppsProperties googleApps = new GoogleAppsProperties();

    /**
     * SAML Metadata UI settings and parsing.
     * SAML元数据UI设置和解析。
     */
    @NestedConfigurationProperty
    private SamlMetadataUIProperties samlMetadataUi = new SamlMetadataUIProperties();

    /**
     * SAML Core functionality and settings.
     * SAML核心功能和设置。
     */
    @NestedConfigurationProperty
    private SamlCoreProperties samlCore = new SamlCoreProperties();

    /**
     * UI and theme settings.
     * UI和主题设置。
     */
    @NestedConfigurationProperty
    private ThemeProperties theme = new ThemeProperties();

    /**
     * Locale and internationalization settings.
     * 区域设置和国际化设置。
     */
    @NestedConfigurationProperty
    private LocaleProperties locale = new LocaleProperties();

    /**
     * Spring Webflow functionality.
     * Spring Webflow 功能
     */
    @NestedConfigurationProperty
    private WebflowProperties webflow = new WebflowProperties();

    /**
     * Custom properties.
     * 客户属性
     */
    @NestedConfigurationProperty
    private CasCustomProperties custom = new CasCustomProperties();

    /**
     * Standalone configuration settings.
     * 独立配置设置。
     */
    @NestedConfigurationProperty
    private StandaloneConfigurationProperties standalone = new StandaloneConfigurationProperties();
}
