package org.apereo.cas.configuration.model.webapp;

import org.apereo.cas.configuration.model.core.util.EncryptionRandomizedSigningJwtCryptographyProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.apereo.cas.configuration.support.SpringResourceProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * Configuration properties class for webflow.
 *
 * @author Dmitriy Kopylenko
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-webflow")

@Getter
@Setter
public class WebflowProperties implements Serializable {

    private static final long serialVersionUID = 4949978905279568311L;

    /**
     * Encryption/signing setting for webflow.
     * webflow的加密/签名设置。
     */
    @NestedConfigurationProperty
    private EncryptionRandomizedSigningJwtCryptographyProperties crypto = new EncryptionRandomizedSigningJwtCryptographyProperties();

    /**
     * Whether CAS should take control of all spring webflow modifications
     * and dynamically alter views, states and actions.
     * CAS是否应该控制所有SpringWebflow修改
     *
     * 动态地改变视图、状态和动作。
     */
    private boolean autoconfigure = true;

    /**
     * Whether webflow should remain in "live reload" mode, able to auto detect
     * changes and react. This is useful if the location of the webflow is externalized
     * and changes are done ad-hoc to the webflow to accommodate changes.
     * *webflow是否应该保持“livereload”模式，能够自动检测
     *
     * *改变和反应。如果webflow的位置是外部化的，这很有用
     *
     * *对webflow进行临时更改以适应更改。
     */
    private boolean refresh;

    /**
     * Whether flow executions should redirect after they pause before rendering.
     * 流执行是否应在渲染前暂停后重定向。
     */
    private boolean alwaysPauseRedirect;

    /**
     * Whether flow executions redirect after they pause for transitions that remain in the same view state.
     对于保持相同视图状态的转换，流执行是否在暂停后重定向。
     */
    private boolean redirectSameState;

    /**
     * Webflow session management settings.
     * Webflow会话管理设置。
     */
    @NestedConfigurationProperty
    private WebflowSessionManagementProperties session = new WebflowSessionManagementProperties();

    /**
     * Path to groovy resource that may auto-configure the webflow context
     * dynamically creating/removing states and actions.
     * 可以自动配置webflow上下文的groovy资源的路径
     * 动态创建/删除状态和操作。
     */
    private Groovy groovy = new Groovy();

    @RequiresModule(name = "cas-server-core-webflow", automated = true)
    @Getter
    @Setter
    public static class Groovy extends SpringResourceProperties {

        private static final long serialVersionUID = 8079027843747126083L;
    }
}
