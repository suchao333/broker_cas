package org.apereo.cas.configuration.model.core.logout;

import lombok.Getter;
import lombok.Setter;
import org.apereo.cas.configuration.support.RequiresModule;

import java.io.Serializable;

/**
 * This is {@link LogoutProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-logout", automated = true)
@Getter
@Setter
public class LogoutProperties implements Serializable {

    private static final long serialVersionUID = 7466171260665661949L;

    /**
     * The target destination to which CAS should redirect after logout
     * is indicated and extracted by a parameter name of your choosing here. If none specified,
     * the default will be used as {@code service}.
     * *注销后CAS应重定向到的目标目标
     *
     * *由您在此处选择的参数名称指示和提取。如果没有指定，
     *
     * *默认值将用作{@code service}。
     */
    private String redirectParameter;

    /**
     * Whether CAS should be allowed to redirect to an alternative location after logout.
     * 是否应允许CAS在注销后重定向到其他位置。
     */
    private boolean followServiceRedirects;

    /**
     * Indicates whether tickets issued and linked to a ticket-granting ticket
     * should also be removed as part of logout. There are a number of tickets
     * issued by CAS whose expiration policy is usually by default bound
     * to the SSO expiration policy and the active TGT, yet such tickets may be
     * allowed to live beyond the normal lifetime of a CAS SSO session
     * with options to be renewed. Examples include OAuth's access tokens, etc.
     * Set this option to true if you want all linked tickets to be removed.
     * *指示是否已颁发票证并将其链接到票证授予票证
     *
     * *也应作为注销的一部分删除。有很多票
     *
     * *由CA发出，其过期策略通常在默认情况下是绑定的
     *
     * *对于SSO过期策略和活动TGT，但是这些票证可能是
     *
     * *允许超出CAS SSO会话的正常生存期
     *
     * *可选择续签。示例包括OAuth的访问令牌等。
     *
     * *如果要删除所有链接的票证，请将此选项设置为true。
     */
    private boolean removeDescendantTickets;

    /**
     * Before logout, allow the option to confirm on the web interface.
     * 在注销之前，允许在web界面上确认选项。
     */
    private boolean confirmLogout;

    /**
     * A url to which CAS must immediately redirect
     * after all logout operations have completed.
     * Typically useful in scenarios where CAS is acting
     * as a proxy and needs to redirect to an external
     * identity provider's logout endpoint in order to
     * remove a session, etc.
     * CAS必须立即重定向到的url
     * 在所有注销操作完成后。
     * 通常在CAS起作用的情况下非常有用
     * 作为代理，需要重定向到外部
     * 身份提供程序的注销终结点，以便删除会话等。
     */
    private String redirectUrl;
}
