package org.apereo.cas.configuration.model.core.authentication;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link PasswordEncoderProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-authentication", automated = true)
@Getter
@Setter
public class PasswordEncoderProperties implements Serializable {

    private static final long serialVersionUID = -2396781005262069816L;

    public enum PasswordEncoderTypes {

        /**
         * No password encoding will take place.
         * 不会进行密码编码。
         */
        NONE, /**
         * Uses an encoding algorithm and a char encoding algorithm.
         * 使用编码算法和字符编码算法。
         */
        DEFAULT, /**
         * Uses {@link org.springframework.security.crypto.password.StandardPasswordEncoder}.
         * A standard {@code PasswordEncoder} implementation that uses SHA-256 hashing with 1024
         * iterations and a random 8-byte random salt value.
         * 使用{@linkorg.springframework.security.crypto.password.StandardPasswordEncoder}.
         *
         * 一个标准的{@code PasswordEncoder}实现，使用1024的SHA-256哈希
         *
         * 迭代和随机8字节的随机salt值。
         */
        STANDARD, /**
         * Uses {@link org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder}.
         */
        BCRYPT, /**
         * Uses {@link org.springframework.security.crypto.scrypt.SCryptPasswordEncoder}.
         */
        SCRYPT, /**
         * Uses {@link org.springframework.security.crypto.password.Pbkdf2PasswordEncoder}.
         */
        PBKDF2, /**
         * Uses <code>org.apereo.cas.util.crypto.GlibcCryptPasswordEncoder</code> from module <code>server-core-util-api</code>.
         */
        GLIBC_CRYPT
    }

    /**
     * Define the password encoder type to use.
     * Type may be specified as blank or 'NONE' to disable password encoding.
     * It may also refer to a fully-qualified class name that implements
     * the {@link org.springframework.security.crypto.password.PasswordEncoder} interface
     * if you wish you define your own encoder.
     * @see PasswordEncoderTypes
     * 定义要使用的密码编码器类型。
     *
     * 类型可以指定为空白或“无”以禁用密码编码。
     *
     * 它还可以引用实现
     *
     * {@链接org.springframework.security.crypto.password.PasswordEncoder}接口
     *
     * 如果你想定义你自己的编码器。
     *
     * @查看密码编码类型
     */
    private String type = "NONE";

    /**
     * The encoding algorithm to use such as 'MD5'.
     * Relevant when the type used is 'DEFAULT' or 'GLIBC_CRYPT'.
     * 要使用的编码算法，如“MD5”。
     * 当使用的类型为“DEFAULT”或“GLIBC_CRYPT”时相关。
     */
    private String encodingAlgorithm;

    /**
     * The encoding algorithm to use such as 'UTF-8'.
     * Relevant when the type used is 'DEFAULT'.
     * *要使用的编码算法，如“UTF-8”。
     *
     * *当使用的类型为“DEFAULT”时相关。
     */
    private String characterEncoding;

    /**
     * Secret to use with STANDARD, PBKDF2, BCRYPT, GLIBC_CRYPT password encoders.
     * Secret usually is an optional setting.
     * 与标准、PBKDF2、BCRYPT、GLIBC_CRYPT密码编码器一起使用的秘密。
     * 秘密通常是一个可选的设置。
     */
    private String secret;

    /**
     * Strength or number of iterations to use for password hashing.
     * Usually relevant when dealing with PBKDF2 or BCRYPT encoders.
     * Used by GLIBC_CRYPT encoders as well.
     * 用于密码哈希的迭代强度或迭代次数。
     * 通常在处理PBKDF2或BCRYPT编码器时相关。
     * 也可用于GLIBC_CRYPT编码器。
     */
    private int strength = 16;
}
