package org.apereo.cas.configuration.model.support.fortress;

import org.apereo.cas.configuration.support.RequiresModule;
import org.apereo.cas.configuration.support.RequiredProperty;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link FortressAuthenticationProperties}.
 * apache fortress（LDAP目录管理）
 * @author Yudhi Karunia Surtan
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-fortress")

@Getter
@Setter
public class FortressAuthenticationProperties implements Serializable {

    private static final long serialVersionUID = 9068259944327425315L;

    /**
     * Use this setting to set the tenant id onto function call into Fortress which allows segregation of data by customer.
     * The context is used for multi-tenancy to isolate data sets within a particular sub-tree within DIT.
     * Setting contextId into this object will render this class' implementer thread unsafe.
     *使用此设置将租户id设置到Fortress函数调用上，该函数允许按客户分离数据。

     *上下文用于多租户，以隔离DIT中特定子树中的数据集。

     *将contextId设置为此对象将使该类的实现程序线程不安全。
     */
    @RequiredProperty
    private String rbaccontext = "HOME";
}
