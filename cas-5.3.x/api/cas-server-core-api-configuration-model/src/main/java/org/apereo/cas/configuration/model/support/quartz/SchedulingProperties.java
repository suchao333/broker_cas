package org.apereo.cas.configuration.model.support.quartz;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link SchedulingProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-core-util", automated = true)

@Getter
@Setter
public class SchedulingProperties implements Serializable {

    private static final long serialVersionUID = -1522227059439367394L;

    /**
     * Whether scheduler is should schedule the job to run.
     * 调度程序是否应运行作业调度程序。
     */
    private boolean enabled = true;

    /**
     * String representation of a start delay of loading data for a data store implementation.
     * This is the delay between scheduler startup and first job’s execution
     * 数据存储实现加载数据的开始延迟的字符串表示。
     * 这是调度程序启动和第一个作业执行之间的延迟
     */
    private String startDelay = "PT15S";

    /**
     * String representation of a repeat interval of re-loading data for an data store implementation.
     * This is the timeout between consecutive job’s executions.
     * 数据存储实现重新加载数据的重复间隔的字符串表示。
     * 这是连续作业执行之间的超时。
     */
    private String repeatInterval = "PT2M";
}
