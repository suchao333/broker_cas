package org.apereo.cas.configuration.model.webapp;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link WebflowSessionManagementProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */

@Getter
@Setter
public class WebflowSessionManagementProperties implements Serializable {

    private static final long serialVersionUID = 7479028707118198914L;

    /**
     * Sets the time period that can elapse before a
     * timeout occurs on an attempt to acquire a conversation lock. The default is 30 seconds.
     * Only relevant if session storage is done on the server.
     * *设置在
     *
     * *尝试获取会话锁时发生超时。默认值为30秒。
     *
     * *仅当会话存储在服务器上完成时才相关。
     */
    private String lockTimeout = "PT30S";

    /**
     * Using the maxConversations property, you can limit the number of concurrently
     * active conversations allowed in a single session. If the maximum is exceeded,
     * the conversation manager will automatically end the oldest conversation.
     * The default is 5, which should be fine for most situations.
     * Set it to -1 for no limit. Setting maxConversations
     * to 1 allows easy resource cleanup in situations where there
     * should only be one active conversation per session.
     * Only relevant if session storage is done on the server.
     * *使用maxConversations属性，可以限制并发的数量
     *
     * *单个会话中允许活动对话。如果超过最大值，
     *
     * *对话管理器将自动结束最旧的对话。
     *
     * *默认值为5，这在大多数情况下都可以。
     *
     * *设置为-1表示无限制。设置maxConversations
     *
     * *到1允许在以下情况下轻松清理资源
     *
     * *每个会话只能有一个活动对话。
     *
     * *仅当会话存储在服务器上完成时才相关。
     */
    private int maxConversations = 5;

    /**
     * Whether or not the snapshots should be compressed.
     * 是否应压缩快照。
     */
    private boolean compress;

    /**
     * Controls whether spring webflow sessions are to be stored server-side or client side.
     * By default state is managed on the client side, that is also signed and encrypted.
     *控制springwebflow会话是存储在服务器端还是客户端。
     *默认情况下，状态是在客户端管理的，客户端也是经过签名和加密的。
     */
    private boolean storage;

    /**
     * If sessions are to be replicated via Hazelcast, defines the location of a {@code hazelcast.xml}
     * file that defines how state should be replicated.
     * Only relevant if session storage is done on the server.
     * *如果要通过Hazelcast复制会话，则定义{@代码的位置hazelcast.xml}
     *
     * *定义应如何复制状态的文件。
     *
     * *仅当会话存储在服务器上完成时才相关。
     */
    private transient Resource hzLocation = new ClassPathResource("hazelcast.xml");
}
