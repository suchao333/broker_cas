package org.apereo.cas.configuration.model.support.saml.mdui;

import org.apereo.cas.configuration.model.support.quartz.SchedulingProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.apereo.cas.configuration.support.RequiredProperty;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link SamlMetadataUIProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-support-saml-mdui")

@Getter
@Setter
public class SamlMetadataUIProperties implements Serializable {

    private static final long serialVersionUID = 2113479681245996975L;

    /**
     * The parameter name that indicates the entity id of the service provider
     * submitted to CAS.
     * *指示服务提供程序的实体id的参数名
     *
     * *提交给CAS。
     */
    private String parameter = "entityId";

    /**
     * If specified, creates a validity filter on the metadata to check for
     * metadata freshness based on the max validity. Value is specified in seconds.
     * *如果指定，则在元数据上创建一个要检查的有效性过滤器
     * *基于最大有效性的元数据新鲜度。值以秒为单位指定。
     */
    private long maxValidity;

    /**
     * When parsing metadata, whether the root element is required to be signed.
     * 解析元数据时，根元素是否需要签名。
     */
    private boolean requireSignedRoot;

    /**
     * Whether valid metadata is required（需要） when parsing metadata.
     * 解析元数据时是否需要有效的元数据。
     */
    private boolean requireValidMetadata = true;

    /**
     * Metadata resources to load and parse through based on the incoming entity id
     * in order to locate MDUI. Resources can be classpath/file/http resources.
     * If each metadata resource has a signing certificate（证书）, they can be added onto the resource with a {@code ::}
     * separator. Example: {@code classpath:/sp-metadata.xml::classpath:/pub.key}.
     */
    @RequiredProperty
    private List<String> resources = new ArrayList<>();

    /**
     * Scheduler settings to indicate how often is metadata reloaded.
     */
    @NestedConfigurationProperty
    private SchedulingProperties schedule = new SchedulingProperties();

    public SamlMetadataUIProperties() {
        schedule.setEnabled(true);
        schedule.setStartDelay("PT30S");
        schedule.setRepeatInterval("PT2M");
    }
}
