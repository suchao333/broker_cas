package org.apereo.cas.configuration.model.support.jdbc;

import org.apereo.cas.configuration.support.RequiresModule;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link JdbcAuthenticationProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-support-jdbc-authentication")

@Getter
@Setter
public class JdbcAuthenticationProperties implements Serializable {

    private static final long serialVersionUID = 7199786191466526110L;

    /**
     * Settings related to search-mode jdbc authentication.
     * Searches for a user record by querying against a username and password; the user is authenticated if at least one result is found.
     *与搜索模式jdbc身份验证相关的设置。
     *通过查询用户名和密码来搜索用户记录；如果至少找到一个结果，则对用户进行身份验证。
     */
    private List<SearchJdbcAuthenticationProperties> search = new ArrayList();

    /**
     * Settings related to query-encode-mode jdbc authentication.
     * A JDBC querying handler that will pull back the password and the private salt value for a user and validate
     * the encoded password using the public salt value. Assumes everything is inside the same database table.
     * Supports settings for number of iterations as well as private salt.
     * This password encoding method combines the private Salt and the public salt which it prepends to the password
     * before hashing. If multiple iterations
     * are used, the byte code hash of the first iteration is rehashed without the salt values. The final hash
     * is converted to hex before comparing it to the database value.
     * *与查询编码模式jdbc身份验证相关的设置。
     *
     * *一个JDBC查询处理程序，它将收回用户的密码和私有salt值并进行验证
     *
     * *使用公共salt值的编码密码。假设所有内容都在同一个数据库表中。
     *
     * *支持迭代次数和私有salt的设置。
     *
     * *此密码编码方法将私有Salt和公共Salt合并在密码前面
     *
     * *在散列之前。如果多次迭代
     *
     * *如果使用，则第一次迭代的字节码哈希将不带salt值重新哈希。最后的散列
     *
     * *在与数据库值进行比较之前将其转换为十六进制。
     */
    private List<QueryEncodeJdbcAuthenticationProperties> encode = new ArrayList();

    /**
     * Settings related to query-mode jdbc authentication.
     * Authenticates a user by comparing the user password
     * (which can be encoded with a password encoder) against the password on record determined by a configurable database query.
     *与查询模式jdbc身份验证相关的设置。
     *通过比较用户密码来验证用户
     *（可以用密码编码器编码）根据可配置数据库查询确定的记录密码。
     */
    private List<QueryJdbcAuthenticationProperties> query = new ArrayList();

    /**
     * Settings related to bind-mode jdbc authentication.
     * Authenticates a user by attempting to create a database connection using the username and (hashed) password.
     *与绑定模式jdbc身份验证相关的设置。
     *通过尝试使用用户名和（哈希）密码创建数据库连接来验证用户。
     */
    private List<BindJdbcAuthenticationProperties> bind = new ArrayList();
}
