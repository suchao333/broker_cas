package org.apereo.cas.configuration.model.support.clearpass;

import org.apereo.cas.configuration.model.core.util.EncryptionJwtSigningJwtCryptographyProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link ClearpassProperties}.
 * 免登陆
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-authentication", automated = true)

@Getter
@Setter
public class ClearpassProperties implements Serializable {

    private static final long serialVersionUID = 6047778458053531460L;

    /**
     * Enable clearpass and allow CAS to cache credentials.
     * 启用clearpass并允许CAS缓存凭证。
     */
    private boolean cacheCredential;

    /**
     * Crypto settings that sign/encrypt the password captured.
     * 对捕获的密码进行签名/加密的密码设置。
     */
    @NestedConfigurationProperty
    private EncryptionJwtSigningJwtCryptographyProperties crypto = new EncryptionJwtSigningJwtCryptographyProperties();
}
