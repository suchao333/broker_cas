package org.apereo.cas.configuration.model.support.jdbc;

import org.apereo.cas.configuration.model.core.authentication.PasswordEncoderProperties;
import org.apereo.cas.configuration.model.core.authentication.PrincipalTransformationProperties;
import org.apereo.cas.configuration.model.support.jpa.AbstractJpaProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link BindJdbcAuthenticationProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-jdbc-authentication")

@Getter
@Setter
public class BindJdbcAuthenticationProperties extends AbstractJpaProperties {

    private static final long serialVersionUID = 4268982716707687796L;

    /**
     * A number of authentication handlers are allowed to determine whether they can operate on the provided credential
     * and as such lend themselves to be tried and tested during the authentication handler selection phase.
     * The credential criteria may be one of the following options:<ul>
     * <li>1) A regular expression pattern that is tested against the credential identifier.</li>
     * <li>2) A fully qualified class name of your own design that implements {@code Predicate<Credential>}.</li>
     * <li>3) Path to an external Groovy script that implements the same interface.</li>
     * </ul>
     * *允许多个身份验证处理程序来确定它们是否可以对所提供的凭据进行操作
     *
     * *因此，可以在身份验证处理程序选择阶段进行尝试和测试。
     *
     * *凭证条件可以是以下选项之一：<ul>
     *
     * *<li>1）根据凭证标识符进行测试的正则表达式模式</li>
     *
     * *<li>2）您自己设计的实现{@code Predicate<Credential>}的完全限定类名</li>
     *
     * *<li>3）实现相同接口的外部Groovy脚本的路径</li>
     *
     * *</ul>
     */
    private String credentialCriteria;

    /**
     * Principal transformation settings for this authentication.
     */
    @NestedConfigurationProperty
    private PrincipalTransformationProperties principalTransformation = new PrincipalTransformationProperties();

    /**
     * Password encoding strategies for this authentication.
     */
    @NestedConfigurationProperty
    private PasswordEncoderProperties passwordEncoder = new PasswordEncoderProperties();

    /**
     * Name of the authentication handler.
     */
    private String name;

    /**
     * Order of the authentication handler in the chain.
     */
    private int order = Integer.MAX_VALUE;
}
