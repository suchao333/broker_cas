package org.apereo.cas.configuration.model.support.generic;

import org.apache.commons.lang3.StringUtils;
import org.apereo.cas.configuration.model.core.authentication.PasswordEncoderProperties;
import org.apereo.cas.configuration.model.core.authentication.PasswordPolicyProperties;
import org.apereo.cas.configuration.model.core.authentication.PrincipalTransformationProperties;
import org.apereo.cas.configuration.support.RequiresModule;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link AcceptAuthenticationProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-authentication")
@Getter
@Setter
public class AcceptAuthenticationProperties implements Serializable {

    private static final long serialVersionUID = 2448007503183227617L;

    /**
     * Accepted users for authentication, in the syntax of {@code uid::password}.
     * 接受用户身份验证，语法为{@code uid::password}。
     */
    private String users = StringUtils.EMPTY;

    /**
     * Name of the authentication handler.
     * 身份验证处理程序的名称。
     */
    private String name;

    /**
     * Password encoder settings for the authentication handler.
     * 身份验证程序的加密设置
     */
    @NestedConfigurationProperty
    private PasswordEncoderProperties passwordEncoder = new PasswordEncoderProperties();

    /**
     * A number of authentication handlers are allowed to determine whether they can operate on the provided credential
     * and as such lend themselves to be tried and tested during the authentication handler selection phase.
     * The credential criteria may be one of the following options:<ul>
     * <li>1) A regular expression pattern that is tested against the credential identifier.</li>
     * <li>2) A fully qualified class name of your own design that implements {@code Predicate<Credential>}.</li>
     * <li>3) Path to an external Groovy script that implements the same interface.</li>
     * </ul>
     * *允许许多身份验证处理程序来确定它们是否可以对所提供的凭据进行操作
     * *这样就可以在身份验证处理程序选择阶段进行尝试和测试。
     * *凭据标准可以是以下选项之一:<ul>
     * 1)根据凭据标识符测试的正则表达式模式
     * 2)实现{@code谓词<Credential>}.</li>的您自己设计的完全限定类名
     * 3)一个实现相同接口的外部Groovy脚本的路径
     * * < / ul >
     */
    private String credentialCriteria;

    /**
     * This is principal transformation properties.
     * 这是主要的变换性质。
     */
    @NestedConfigurationProperty
    private PrincipalTransformationProperties principalTransformation = new PrincipalTransformationProperties();

    /**
     * Password policy settings.
     * 密码策略设置。
     */
    @NestedConfigurationProperty
    private PasswordPolicyProperties passwordPolicy = new PasswordPolicyProperties();
}
