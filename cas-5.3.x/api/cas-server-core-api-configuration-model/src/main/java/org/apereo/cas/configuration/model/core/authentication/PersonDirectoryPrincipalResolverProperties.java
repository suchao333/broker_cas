package org.apereo.cas.configuration.model.core.authentication;

import org.apereo.cas.configuration.support.RequiresModule;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Configuration properties class for Person Directory.
 *个人目录的配置属性类
 * @author Dmitriy Kopylenko
 * @since 5.0.0
 */
@RequiresModule(name = "cas-server-core-authentication", automated = true)
@Getter
@Setter
public class PersonDirectoryPrincipalResolverProperties implements Serializable {

    private static final long serialVersionUID = 8929912041234879300L;

    /**
     * Attribute name to use to indicate the identifier of the principal constructed.
     * If the attribute is blank or has no values, the default principal id will be used
     * determined by the underlying authentication engine. The principal id attribute
     * usually is removed from the collection of attributes collected, though this behavior
     * depends on the schematics of the underlying authentication strategy.
     * *用于指示构造的主体的标识符的属性名。
     *
     * *如果属性为空或没有值，则将使用默认主体id
     *
     * *由底层身份验证引擎确定。主体id属性
     *
     * *通常是从收集的属性集合中移除的，但此行为
     *
     * *取决于底层身份验证策略的示意图。
     */
    private String principalAttribute;

    /**
     * Return a null principal object if no attributes can be found for the principal.
     * 如果找不到主体的属性，则返回空的主体对象。
     */
    private boolean returnNull;

    /**
     * When true, throws an error back indicating that principal resolution
     * has failed and no principal can be found based on the authentication requirements.
     * Otherwise, simply logs the condition as an error without raising a catastrophic error.
     * 如果为true，则返回一个错误，指示主解析
     *
     * 失败，根据身份验证要求找不到主体。
     *
     * 否则，只需将条件记录为错误而不引发灾难性错误。
     */
    private boolean principalResolutionFailureFatal;

    /**
     * Uses an existing principal id that may have already
     * been established in order to run person directory queries.
     * This is generally useful in situations where
     * authentication is delegated to an external identity provider
     * and a principal is first established to then query an attribute source.
     * 使用可能已经
     * 为运行人员目录查询而建立。
     * 这通常在以下情况下有用：
     * 身份验证委托给外部身份提供程序
     * 首先建立一个主体来查询属性源。
     */
    private boolean useExistingPrincipalId;
}
