package org.apereo.cas.configuration.model.core.authentication;

import lombok.Getter;
import lombok.Setter;
import org.apereo.cas.configuration.support.RequiresModule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Authentication attribute release properties.
 *
 * @author Daniel Frett
 * @since 5.2.0
 */
@RequiresModule(name = "cas-server-support-validation", automated = true)
@Getter
@Setter
public class AuthenticationAttributeReleaseProperties implements Serializable {
    private static final long serialVersionUID = 6123748197108749858L;
    
    /**
     * List of authentication attributes that should never be released.
     * 不应释放的身份验证属性列表。
     */
    private List<String> neverRelease = new ArrayList<>();

    /**
     * List of authentication attributes that should be the only ones released. An empty list indicates all attributes
     * should be released.
     * 应该是唯一发布的身份验证属性的列表。空列表表示所有属性
     * 应该被释放。
     */
    private List<String> onlyRelease = new ArrayList<>();
}
