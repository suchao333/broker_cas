package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.core.Ordered;

import java.io.Serializable;
import java.util.Map;

/**
 * Defines the general contract of the attribute release policy for a registered service.
 * An instance of this attribute filter may determine how principal/global attributes are translated to a
 * map of attributes that may be released for a registered service.
 *定义已注册服务的属性释放策略的常规约定。
 *此属性筛选器的实例可以确定如何将主/全局属性转换为
 *可为已注册服务释放的属性的映射。
 * @author Misagh Moayyed
 * @since 4.1.0
 */
@FunctionalInterface
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredServiceAttributeFilter extends Serializable, Ordered {
    /**
     * Filters the received principal attributes for the given registered service.
     *过滤给定注册服务的接收到的主体属性。
     * @param givenAttributes the map for the original given attributes
     * @return a map that contains the filtered attributes.
     */
    Map<String, Object> filter(Map<String, Object> givenAttributes);

    @Override
    default int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
