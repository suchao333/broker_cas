package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * Expiration policy that dictates how long should this service be kept alive.
 * 指定此服务应保持活动多长时间的过期策略。
 * @author Misagh Moayyed
 * @since 5.2
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredServiceExpirationPolicy extends Serializable {
    
    /**
     * Gets expiration date that indicates when this may be expired.
     *
     * @return the expiration date
     */
    String getExpirationDate();

    /**
     * Notify service owners and contacts
     * when this service is marked as expired.
     *通知服务所有者和联系人
     *当此服务被标记为过期时。
     * @return true/false
     */
    boolean isNotifyWhenDeleted();

    /**
     * Whether service should be deleted from the registry
     * if and when expired. 
     **是否应该从注册表中删除服务
     * *如果和当到期。
     * @return true/false
     */
    boolean isDeleteWhenExpired();
}
