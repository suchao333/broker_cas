package org.apereo.cas.services;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * This is {@link ServiceRegistryExecutionPlan}.
 *
 * @author Misagh Moayyed
 * @since 5.3.0
 */
public interface ServiceRegistryExecutionPlan {
    /**
     * Register service registry.
     *注册服务注册表。
     * @param registry the registry
     * @return the service registry execution plan
     */
    ServiceRegistryExecutionPlan registerServiceRegistry(ServiceRegistry registry);

    /**
     * Get service registries collection.
     *获取服务注册表集合。
     * @return the collection
     */
    Collection<ServiceRegistry> getServiceRegistries();

    /**
     * Get service registries collection.
     *获取服务注册表集合。
     * @param typeFilter the type filter
     * @return the collection
     */
    Collection<ServiceRegistry> getServiceRegistries(Predicate<ServiceRegistry> typeFilter);

}
