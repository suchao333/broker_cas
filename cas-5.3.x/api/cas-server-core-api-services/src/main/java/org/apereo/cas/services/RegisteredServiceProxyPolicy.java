package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.net.URL;

/**
 * Defines the proxying policy for a registered service.
 * While a service may be allowed proxying on a general level,
 * it may still want to restrict who is authorizes to receive
 * the proxy granting ticket. This interface defines the behavior
 * for both options.
 *定义已注册服务的代理策略。
 *
 *虽然服务可以在一般级别上进行代理，
 *
 *它可能仍然想限制谁被授权接收
 *
 *代理授权票。此接口定义行为
 *对于这两种选择。
 * @author Misagh Moayyed
 * @since 4.1.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredServiceProxyPolicy extends Serializable {

    /**
     * Determines whether the service is allowed proxy
     * capabilities.
     **确定是否允许代理服务
     *
     * *能力。
     * @return true, if is allowed to proxy
     */
    boolean isAllowedToProxy();

    /**
     * Determines if the given proxy callback
     * url is authorized and allowed to
     * request proxy access.
     *确定给定的代理回调
     *url被授权并允许
     *请求代理访问。
     * @param pgtUrl the pgt url
     * @return true, if url allowed.
     */
    boolean isAllowedProxyCallbackUrl(URL pgtUrl);
}
