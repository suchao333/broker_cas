package org.apereo.cas.services;

import org.apereo.cas.authentication.principal.Principal;

import java.util.Collection;

/**
 * This is {@link MultifactorAuthenticationProviderSelector}
 * that decides how to resolve a single provider from a collection available
 * to a registered service.
 *这是{@link MultifactorAuthenticationProviderSelector}
 *它决定如何从可用集合解析单个提供程序
 *注册服务。
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@FunctionalInterface
public interface MultifactorAuthenticationProviderSelector {

    /**
     * Resolve multifactor authentication provider.
     *解析多因素身份验证提供程序。
     * @param providers the providers
     * @param service   the service
     * @param principal the principal
     * @return the multifactor authentication provider
     */
    MultifactorAuthenticationProvider resolve(Collection<MultifactorAuthenticationProvider> providers,
                                              RegisteredService service,
                                              Principal principal);
}
