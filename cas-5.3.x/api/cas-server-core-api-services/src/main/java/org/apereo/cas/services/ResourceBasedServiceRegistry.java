package org.apereo.cas.services;

import java.io.File;
import java.util.Collection;

/**
 * This is {@link ResourceBasedServiceRegistry},
 * which describes operations relevant to a service registry
 * that is backed by file-system resources.
 **这是{@link ResourceBasedServiceRegistry}，
 *
 * *它描述与服务注册表相关的操作
 *
 * *由文件系统资源支持。
 * @author Misagh Moayyed
 * @since 5.0.0
 */
public interface ResourceBasedServiceRegistry extends ServiceRegistry {

    /**
     * Update a single service instance.
     *更新单个服务实例。
     * @param service the service
     */
    void update(RegisteredService service);

    /**
     * Load registered service from the given file.
     *从给定文件加载已注册的服务。
     * @param file the file
     * @return the registered services
     */
    Collection<RegisteredService> load(File file);
}
