package org.apereo.cas.authentication;

import org.apereo.cas.services.RegisteredService;

import java.util.Map;

/**
 * An encoder that defines how a CAS attribute
 * is to be encoded and signed in the CAS
 * validation response. The collection of
 * attributes should not be mangled with and
 * filtered. All attributes will be released.
 * It is up to the implementations
 * to decide which attribute merits encrypting.
 **一个定义CAS属性的编码器
 * *将在核证机关内进行编码和签署
 * *验证响应。的集合
 * 属性不应该被and混淆
 * *过滤。所有属性都将被释放。
 * *这取决于实现
 * *决定哪些属性值得加密。
 * @author Misagh Moayyed
 * @since 4.1
 */
@FunctionalInterface
public interface ProtocolAttributeEncoder {

    /**
     * Encodes attributes that are ready to be released.
     * Typically, this method tries to ensure that the
     * PGT and the credential password are correctly encrypted
     * before they are released. Attributes should not be filtered
     * and removed and it is assumed that all will be returned
     * back to the service.
     * *对准备释放的属性进行编码。
     * *通常，此方法尝试确保
     * *PGT和凭证密码已正确加密
     * *在他们被释放之前。不应筛选属性
     * *并将其删除，则假定将全部返回
     * *回到服务台。
     * @param attributes The attribute collection that is ready to be released
     * @param service the requesting service for which attributes are to be encoded
     * @return collection of attributes after encryption ready for release.
     * @since 4.1
     */
    Map<String, Object> encodeAttributes(Map<String, Object> attributes, RegisteredService service);

}
