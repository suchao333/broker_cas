package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This is {@link RegisteredServiceConsentPolicy}.
 * 注册服务consent规则
 * consent（批准 这里动词）
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredServiceConsentPolicy extends Serializable {

    /**
     * Indicate whether consent is enabled.
     * 表示 consent 是否可用
     * @return the boolean
     */
    default boolean isEnabled() {
        return true;
    }

    /**
     * Gets excluded attributes.
     * Excludes the set of specified（指定的） attributes from consent（同意，答应）.
     * 获取被排除的属性。
     * 从consent中排除指定的属性集。
     * @return the excluded attributes
     */
    default Set<String> getExcludedAttributes() {
        return new LinkedHashSet<>(0);
    }

    /**
     * Gets include-only attributes.
     * If specified, consent should only be applied to the listed attributes
     * and not everything the attribute release policy may indicate.
     *获取只包含属性。
     *如果指定，则只应将consent应用于列出的属性
     *而不是属性release策略可能指示的所有内容。
     * @return the include-only attributes. If the return collection is null or empty, 
     * attribute release policy is consulted to determine all of included attributes.
     */
    default Set<String> getIncludeOnlyAttributes() {
        return new LinkedHashSet<>(0);
    }
}
