package org.apereo.cas.services;

import java.util.Optional;

/**
 * Defines how to encrypt data based on registered service's public key, etc.
 * 定义如何基于已注册服务的公钥加密数据等。
 * Cipher（密码，暗号）
 * @author Misagh Moayyed
 * @since 4.1
 */
public interface RegisteredServiceCipherExecutor {
    /**
     * Encode string.
     * 加密字符串
     * @param data    the data
     * @param service the service
     * @return the encoded string or null
     */
    String encode(String data, Optional<RegisteredService> service);

    /**
     * Encode string.
     * 加密字符串
     * @param data the data
     * @return the string
     */
    default String encode(String data) {
        return encode(data, Optional.empty());
    }

    /**
     * Decode string.
     * 解密字符串
     * @param data    the data
     * @param service the service
     * @return the string
     */
    String decode(String data, Optional<RegisteredService> service);

    /**
     * Is enabled?.
     * 是否可用
     * @return the boolean
     */
    default boolean isEnabled() {
        return true;
    }

    /**
     * Supports boolean.
     *
     * @param registeredService the registered service
     * @return the boolean
     */
    default boolean supports(final RegisteredService registeredService) {
        return true;
    }
}
