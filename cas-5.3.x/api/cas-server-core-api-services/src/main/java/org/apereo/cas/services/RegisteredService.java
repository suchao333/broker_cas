package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apereo.cas.authentication.principal.Service;

import java.io.Serializable;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Interface for a service that can be registered by the Services Management
 * interface.
 * 服务管理可以注册的服务的接口。
 * 接口。
 * @author Scott Battaglia
 * @since 3.1
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredService extends Serializable, Comparable<RegisteredService> {

    /**
     * The logout type.
     * 退出类型
     */
    enum LogoutType {
        /**
         * For no SLO.
         */
        NONE,
        /**
         * For back channel SLO.
         */
        BACK_CHANNEL,
        /**
         * For front channel SLO.
         */
        FRONT_CHANNEL
    }

    /**
     * Initial ID value of newly created (but not persisted) registered service.
     * 新创建(但不持久)注册服务的初始ID值。
     */
    long INITIAL_IDENTIFIER_VALUE = -1;

    /**
     * Get the expiration policy rules for this service.
     *获取此服务的过期策略规则。
     * @return the proxy policy
     */
    RegisteredServiceExpirationPolicy getExpirationPolicy();

    /**
     * Get the proxy policy rules for this service.
     *获取此服务的代理策略规则。
     * @return the proxy policy
     */
    RegisteredServiceProxyPolicy getProxyPolicy();

    /**
     * The unique identifier for this service.
     *此服务的唯一标识符。
     * @return the unique identifier for this service.
     */
    String getServiceId();

    /**
     * The numeric（数值的） identifier（标识符） for this service. Implementations
     * are expected to initialize the id with the value of {@link #INITIAL_IDENTIFIER_VALUE}.
     *
     * @return the numeric identifier for this service.
     */
    long getId();

    /**
     * Returns the name of the service.
     *
     * @return the name of the service.
     */
    String getName();

    /**
     * Returns a short theme（主题） name. Services do not need to have unique theme
     * names.
     *
     * @return the theme name associated with this service.
     */
    String getTheme();

    /**
     * Returns the description of the service.
     *
     * @return the description of the service.
     */
    String getDescription();

    /**
     * Response determines how CAS should contact the matching service
     * typically with a ticket id. By default, the strategy is a 302 redirect.
     **响应决定CAS应该如何联系匹配服务
     * *通常使用票证id。默认情况下，策略是302重定向。
     * @return the response type
     * @see org.apereo.cas.authentication.principal.Response.ResponseType
     */
    String getResponseType();

    /**
     * Gets the relative（相对的） evaluation（评审） order of this service when determining（决定）
     * matches.
     **确定时获取此服务的相对评估顺序
     *
     * *匹配。
     * @return Evaluation order relative to other registered services. Services with lower values will be evaluated for a match before others.
     */
    int getEvaluationOrder();

    /**
     * Sets the relative evaluation order of this service when determining
     * matches.
     **在确定时设置此服务的相对评估顺序
     *
     * *匹配。
     * @param evaluationOrder the service evaluation order
     */
    void setEvaluationOrder(int evaluationOrder);

    /**
     * Sets the identifier for this service. Use {@link #INITIAL_IDENTIFIER_VALUE} to indicate a branch new service definition.
     设置此服务的标识符。使用{@link#INITIAL_IDENTIFIER_VALUE}指示分支新服务定义。
     * @param id the numeric identifier for the service.
     */
    void setId(long id);

    /**
     * Get the name of the attribute this service prefers to consume as username.
     *获取此服务希望使用的属性名称作为用户名。
     * @return an instance of {@link RegisteredServiceUsernameAttributeProvider}
     */
    RegisteredServiceUsernameAttributeProvider getUsernameAttributeProvider();

    /**
     * Gets authentication policy.
     *
     * @return the authentication policy
     */
    RegisteredServiceMultifactorPolicy getMultifactorPolicy();

    /**
     * Gets the set of handler names that must successfully authenticate credentials（资格证书） in order to access the service.
     * An empty set indicates that there are no requirements on particular authentication handlers; any will suffice.
     *获取必须成功验证凭据才能访问服务的处理程序名称集。
     *
     * *空集表示对特定的身份验证处理程序没有要求；任何要求都可以。
     * @return Non -null set of required handler names.
     */
    Set<String> getRequiredHandlers();

    /**
     * Gets the access strategy that decides whether this registered
     * service is able to proceed with authentication requests.
     * 获取决定此是否已注册的访问策略
     * 服务能够处理身份验证请求。
     * @return the access strategy
     */
    RegisteredServiceAccessStrategy getAccessStrategy();

    /**
     * Returns whether the service matches the registered service.
     * <p>Note, as of 3.1.2, matches are case insensitive.
      返回服务是否与注册的服务匹配。
     *
     <p>注意，从3.1.2开始，匹配不区分大小写。
     * @param service the service to match.
     * @return true if they match, false otherwise.
     */
    boolean matches(Service service);

    /**
     * Returns whether the service id matches the registered service.
     *返回服务id是否与注册的服务匹配。
     * @param serviceId the service id to match.
     * @return true if they match, false otherwise.
     */
    boolean matches(String serviceId);

    /**
     * Returns the logout type of the service.
     * 返回服务的注销类型。
     * @return the logout type of the service.
     */
    LogoutType getLogoutType();

    /**
     * Gets the attribute filtering policy to determine
     * how attributes are to be filtered and released for
     * this service.
     **获取要确定的属性筛选策略
     *
     * *如何过滤和释放属性
     *
     * *这项服务。
     * @return the attribute release policy
     */
    RegisteredServiceAttributeReleasePolicy getAttributeReleasePolicy();

    /**
     * Gets the logo image associated with this service.
     * The image mostly is served on the user interface
     * to identify this requesting service during authentication.
     *获取与此服务关联的徽标图像。
     *图像主要在用户界面上提供
     *在身份验证期间标识此请求服务。
     * @return URL of the image
     * @since 4.1
     */
    String getLogo();

    /**
     * Describes the canonical information url
     * where this service is advertised and may provide
     * help/guidance.
     *描述规范信息url
     *此项服务的广告和可能提供
     *帮助/指导。
     * @return the info url.
     */
    String getInformationUrl();

    /**
     * Links to the privacy（隐私） policy of this service, if any.
     *指向此服务的隐私策略的链接（如果有）。
     * @return the link to privacy policy
     */
    String getPrivacyUrl();

    /**
     * Identifies the logout url that that will be invoked
     * upon sending single-logout callback notifications.
     * This is an optional setting. When undefined, the service
     * url as is defined by {@link #getServiceId()} will be used
     * to handle logout invocations.
     *标识将调用的注销url
     *发送单次注销回调通知时。
     *这是一个可选设置。未定义时，服务
     *将使用{@link#getServiceId（）}定义的url
     *处理注销调用。
     * @return the logout url for this service
     * @since 4.1
     */
    URL getLogoutUrl();

    /**
     * Gets the public key associated with this service
     * that is used to authorize the request by
     * encrypting certain elements and attributes in
     * the CAS validation protocol response, such as
     * the PGT.
     *获取与此服务关联的公钥用于授权请求的在中加密某些元素和属性CAS验证协议响应，如PGT
     * @return the public key instance used to authorize the request
     * @since 4.1
     */
    RegisteredServicePublicKey getPublicKey();

    /**
     * Describes extra metadata about the service; custom fields
     * that could be used by submodules implementing additional
     * behavior on a per-service basis.
     * 描述有关服务的额外元数据；自定义字段它可以被实现额外基于每项服务的行为。
     * @return map of custom metadata.
     * @since 4.2
     */
    default Map<String, RegisteredServiceProperty> getProperties() {
        return new LinkedHashMap<>(0);
    }

    /**
     * A list of contacts that are responsible for the clients that use
     * this service.
     *负责使用的客户端的联系人列表
     *这项服务。
     * @return list of Contacts
     * @since 5.2
     */
    List<RegisteredServiceContact> getContacts();

    /**
     * Gets friendly name of this service.
     * Typically describes the purpose of this service
     * and the return value is usually used for display purposes.
     *获得该服务的友好名称。
     *典型地描述此服务的目的
     *和返回值通常用于显示目的。
     * @return the friendly name
     */
    @JsonIgnore
    default String getFriendlyName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Initialize the registered service instance by defaulting fields to specific
     * values or object instances, etc.
     *通过将字段默认为“特定”来初始化已注册的服务实例
     *值或对象实例等。
     */
    default void initialize() {
    }
}
