package org.apereo.cas.services;

/**
 * This is {@link ImmutableServiceRegistry}.
 *Immutable（不变的）
 * @author Misagh Moayyed
 * @since 5.3.0
 */
public interface ImmutableServiceRegistry extends ServiceRegistry {
}
