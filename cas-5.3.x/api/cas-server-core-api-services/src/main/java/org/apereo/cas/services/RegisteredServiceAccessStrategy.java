package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.core.Ordered;

import java.io.Serializable;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This is {@link RegisteredServiceAccessStrategy} stategy战略
 * that can decide if a service is recognized and authorized to participate
 * in the CAS protocol flow during authentication/validation events.
 *它可以决定一个服务是否被认可并被授权参与在验证/验证事件期间的CAS协议流中。
 * @author Misagh Moayyed
 * @since 4.1
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredServiceAccessStrategy extends Serializable, Ordered {

    /**
     * Verify is the service is enabled and recognized by CAS.
     *
     * @return true /false if service is enabled
     */
    @JsonIgnore
    default boolean isServiceAccessAllowed() {
        return true;
    }

    /**
     * Assert that the service can participate（参与） in sso.
     *
     * @return true /false if service can participate in sso
     */
    @JsonIgnore
    default boolean isServiceAccessAllowedForSso() {
        return true;
    }

    /**
     * Verify authorization policy by checking the pre-configured rules
     * that may depend on what the principal might be carrying.
     * <p>
     * Verify presence of service required attributes.
     * <ul>
     * <li>If no rejected attributes are specified, authz is granted.</li>
     * <li>If no required attributes are specified, authz is granted.</li>
     * <li>If ALL attributes must be present, and the principal contains all and there is
     * at least one attribute value that matches the rejected, authz is denied.</li>
     * <li>If ALL attributes must be present, and the principal contains all and there is
     * at least one attribute value that matches the required, authz is granted.</li>
     * <li>If ALL attributes don't have to be present, and there is at least
     * one principal attribute present whose value matches the rejected, authz is denied.</li>
     * <li>If ALL attributes don't have to be present, and there is at least
     * one principal attribute present whose value matches the required, authz is granted.</li>
     * <li>Otherwise, access is denied</li>
     * </ul>
     * 通过检查预先配置的规则来验证授权策略这可能取决于principal可能携带什么。<p>
   验证是否存在所需的服务属性。
<ul>
     *
     * *<li>如果未指定拒绝的属性，则授予authz</li>
     *
     * *<li>如果没有指定必需的属性，则授予authz</li>
     *
     * *<li>如果所有属性都必须存在，并且主体包含所有且存在
     *
     * *至少有一个与拒绝的authz匹配的属性值被拒绝</li>
     *
     * *<li>如果所有属性都必须存在，并且主体包含所有且存在
     *
     * *至少授予一个与所需的authz匹配的属性值</li>
     *
     * *<li>如果所有属性都不必存在，并且至少
     *
     * *存在一个主属性，其值与拒绝的authz相匹配，拒绝该属性</li>
     *
     * *<li>如果所有属性都不必存在，并且至少
     *
     * *存在一个值与所需值匹配的主属性authz被授予</li>
     *
     * *<li>否则，访问被拒绝</li>
     *
     * @param principal  The authenticated principal
     * @param attributes the attributes. Rather than passing the principal directly, we are only allowing principal attributes
     *                   given they may be coming from a source external to the principal itself. (Cached principal attributes, etc)
     * @return true /false if service access can be granted to principal
     */
    @JsonIgnore
    default boolean doPrincipalAttributesAllowServiceAccess(final String principal, final Map<String, Object> attributes) {
        return true;
    }

    /**
     * Redirect the request to a separate and possibly external URL
     * in case authorization fails for this service. If no URL is
     * specified, CAS shall redirect the request by default to a generic
     * page that describes the authorization failed attempt.
     *将请求重定向到一个单独的外部URL
     *以防此服务的授权失败。如果没有URL
     *指定，CAS将默认将请求重定向到
     *描述授权失败尝试的页面。
     * @return the redirect url
     * @since 4.2
     */
    default URI getUnauthorizedRedirectUrl() {
        return null;
    }

    @Override
    default int getOrder() {
        return Integer.MAX_VALUE;
    }

    /**
     * Sets service access allowed.
     *
     * @param enabled the value
     */
    @JsonIgnore
    default void setServiceAccessAllowed(final boolean enabled) {
    }

    /**
     * Return the delegated（委派） authentication policy for this service.
     * 返回此服务的委托身份验证策略。
     * @return authn policy
     */
    default RegisteredServiceDelegatedAuthenticationPolicy getDelegatedAuthenticationPolicy() {
        return null;
    }

    /**
     * Expose underlying attributes for auditing purposes.
     *出于审核目的公开底层属性。
     * @return required attributes
     */
    default Map<String, Set<String>> getRequiredAttributes() {
        return new HashMap<>();
    }
}
