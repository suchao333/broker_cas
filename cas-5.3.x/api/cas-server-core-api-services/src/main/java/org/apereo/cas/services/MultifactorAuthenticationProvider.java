package org.apereo.cas.services;

import org.apereo.cas.authentication.AuthenticationException;

import org.apereo.cas.authentication.MultifactorAuthenticationProviderBypass;
import org.springframework.core.Ordered;

import java.io.Serializable;

/**
 * This is {@link MultifactorAuthenticationProvider}
 * that describes an external authentication entity/provider
 * matched against a registered service. Providers may be given
 * the ability to check authentication provider for availability
 * before actually producing a relevant identifier.
 *这是{@link MultifactorAuthenticationProvider}
 *
 *描述外部身份验证实体/提供程序的
 *
 *与注册服务相匹配。可能会提供
 *
 *能够检查身份验证提供程序的可用性
 *
 *在实际生成相关标识符之前。
 * @author Misagh Moayyed
 * @since 5.0.0
 */
public interface MultifactorAuthenticationProvider extends Serializable, Ordered {

    /**
     * Ensure the provider is available.
     *
     * @param service - the service
     * @return true /false flag once verification is successful.
     * @throws AuthenticationException the authentication exception
     */
    boolean isAvailable(RegisteredService service) throws AuthenticationException;

    /**
     * Returns the configured bypass provider for this MFA provider.
     *
     * @return - the bypass evaluator
     */
    MultifactorAuthenticationProviderBypass getBypassEvaluator();

    /**
     * Gets id for this provider.
     *
     * @return the id
     */
    String getId();

    /**
     * Gets the friendly-name for this provider.
     *
     * @return the name
     */
    String getFriendlyName();

    /**
     * Does provider match/support this identifier（标识符）?
     * The identifier passed may be formed as a regular expression.
     *传递的标识符可以是正则表达式。
     * @param identifier the identifier
     * @return the boolean
     */
    boolean matches(String identifier);

    /**
     * This method will return the failure mode for the provider.
     *此方法将返回提供程序的失败模式。
     * @return the FailureMode
     */
    RegisteredServiceMultifactorPolicy.FailureModes failureMode();

    /**
     * Creates a unique mark that identifies this provider instance.
     *创建标识此提供程序实例的唯一标记。
     * @return - the mark
     */
    default String createUniqueId() {
        return getId().concat(String.valueOf(hashCode()));
    }

    /**
     * Validates that the passed mark was created by this provider.
     *验证传递的标记是否由此提供程序创建。
     * @param id - the id to validate
     * @return - true if the mark was created by this provider
     */
    default boolean validateId(final String id) {
        return id != null && createUniqueId().equals(id);
    }
}
