package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.Set;

/**
 * This is {@link RegisteredServiceMultifactorPolicy} that describes how a service
 * should handle authentication requests.
 *
 * @author Misagh Moayyed
 * @since 5.0.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredServiceMultifactorPolicy extends Serializable {
    /**
     * The enum Failure modes.
     */
    enum FailureModes {
        /**
         * Disallow MFA, proceed with authentication but don't communicate MFA to the RP.
         * 如果不允许MFA，继续进行身份验证，但不要将MFA与RP通信。
         */
        OPEN,
        /**
         * Disallow MFA, block with authentication.
         * 不允许MFA，阻止与身份验证。
         */
        CLOSED,
        /**
         * Disallow MFA, proceed with authentication and communicate MFA to the RP.
         * 如果不允许MFA，继续进行身份验证，并将MFA与RP通信。
         */
        PHANTOM,

        /**
         * Do not check for failure at all.
         * 根本不要检查是否有故障。
         */
        NONE,

        /**
         * The default one indicating that no failure mode is set at all.
         * 表示根本没有设置任何失败模式的默认模式。
         */
        NOT_SET
    }

    /**
     * Gets MFA authentication provider id.
     *
     * @return the authentication provider id
     */
    Set<String> getMultifactorAuthenticationProviders();

    /**
     * Gets failure mode.
     *
     * @return the failure mode
     */
    FailureModes getFailureMode();

    /**
     * Gets principal attribute name trigger.
     *
     * @return the principal attribute name trigger
     */
    String getPrincipalAttributeNameTrigger();

    /**
     * Gets principal attribute value to match.
     * Values may be regex patterns.
     *获取要匹配的主属性值。
     * 值可以是正则表达式模式。
     * @return the principal attribute value to match
     */
    String getPrincipalAttributeValueToMatch();

    /**
     * Indicates whether authentication should be skipped.
     *指示是否应该跳过身份验证。
     * @return true/false
     */
    boolean isBypassEnabled();

}
