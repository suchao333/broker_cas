package org.apereo.cas.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apereo.cas.authentication.principal.Principal;
import org.apereo.cas.authentication.principal.Service;

import java.io.Serializable;
import java.util.Map;

/**
 * The release policy that decides how attributes are to be released for a given service.
 * Each policy has the ability to apply an optional filter.
 *决定如何为给定服务释放属性的发布策略。
 *每个策略都可以应用可选筛选器。
 * @author Misagh Moayyed
 * @since 4.1.0
 */
@FunctionalInterface
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface RegisteredServiceAttributeReleasePolicy extends Serializable {

    /**
     * Is authorized to release authentication attributes boolean.
     *被授权释放身份验证属性布尔值。
     * @return the boolean
     */
    default boolean isAuthorizedToReleaseAuthenticationAttributes() {
        return true;
    }

    /**
     * Is authorized to release credential password?
     *是否授权释放凭据密码？
     * @return true /false
     */
    default boolean isAuthorizedToReleaseCredentialPassword() {
        return false;
    }

    /**
     * Is authorized to release proxy granting ticket?
     *是否有权发放代理授权票？
     * @return true /false
     */
    default boolean isAuthorizedToReleaseProxyGrantingTicket() {
        return false;
    }

    /**
     * Sets the attribute filter.
     *设置属性过滤器。
     * @param filter the new attribute filter
     */
    default void setAttributeFilter(RegisteredServiceAttributeFilter filter) {
    }

    /**
     * Gets the attributes, having applied the filter.
     *获取已应用筛选器的属性。
     * @param p               the principal that contains the resolved attributes
     * @param selectedService the selected service
     * @param service         the service
     * @return the attributes
     */
    Map<String, Object> getAttributes(Principal p, Service selectedService, RegisteredService service);

    /**
     * Gets the attributes that qualify for consent.
     * 获取符合同意条件的属性。
     * @param p               the principal that contains the resolved attributes
     * @param selectedService the selected service
     * @param service         the service
     * @return the attributes
     */
    default Map<String, Object> getConsentableAttributes(final Principal p, final Service selectedService, 
                                                         final RegisteredService service) {
        return getAttributes(p, selectedService, service);    
    }
}
