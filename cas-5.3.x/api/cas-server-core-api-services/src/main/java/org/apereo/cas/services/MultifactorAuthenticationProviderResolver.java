package org.apereo.cas.services;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * This is {@link MultifactorAuthenticationProviderResolver}.
 *
 * @author Misagh Moayyed
 * @since 5.1.0
 */
public interface MultifactorAuthenticationProviderResolver {

    /**
     * Consolidate providers collection.
     * If the provider is multi-instance in the collection, consolidate and flatten.
     **整合提供程序集合。
     * *如果提供程序是集合中的多实例，则合并并扁平化。
     * @param providers the providers
     * @return the collection
     */
    Collection<MultifactorAuthenticationProvider> flattenProviders(Collection<? extends MultifactorAuthenticationProvider> providers);


    /**
     * Locate the provider in the collection, and have it match the requested mfa.
     * If the provider is multi-instance, resolve based on inner-registered providers.
     **在集合中找到提供程序，并使其与请求的mfa匹配。
     * *如果提供程序是多实例，则基于内部注册的提供程序进行解析。
     * @param providers        the providers
     * @param requestMfaMethod the request mfa method
     * @return the optional
     */
    Optional<MultifactorAuthenticationProvider> resolveProvider(Map<String, MultifactorAuthenticationProvider> providers,
                                                                Collection<String> requestMfaMethod);
}
