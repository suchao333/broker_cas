package org.apereo.cas.validation;

import org.apereo.cas.authentication.Authentication;
import org.apereo.cas.authentication.principal.Service;

import java.util.List;

/**
 * Represents a security assertion obtained from a successfully validated ticket.
 *表示从成功验证的票证获得的安全断言。
 * @author Scott Battaglia
 * @author Marvin S. Addison
 * @since 3.0.0
 */
public interface Assertion {

    /**
     * Gets the authentication event that is basis of this assertion.
     *获取作为此断言基础的身份验证事件。
     * @return Non-null primary authentication event.
     */
    Authentication getPrimaryAuthentication();

    /**
     * Gets a list of all authentications that have occurred during a CAS SSO session.
     * 获取在CAS SSO会话期间发生的所有身份验证的列表。
     * @return Non-null, non-empty list of authentications in leaf-first order (i.e. authentications on the root ticket
     * occur at the end).
     */
    List<Authentication> getChainedAuthentications();

    /**
     * True if the validated ticket was granted in the same transaction as that
     * in which its grantor GrantingTicket was originally issued.
     *如果已验证的票证是在与该票证相同的事务中授予的，则为True
其授予人授予罚单的最初签发地。
     * @return true if validated ticket was granted simultaneous with its
     * grantor's issuance
     */
    boolean isFromNewLogin();

    /**
     * Method to obtain the service for which we are asserting this ticket is
     * valid for.
     **方法获取要为其声明此票证的服务
     * *有效期为。
     * @return the service for which we are asserting this ticket is valid for.
     */
    Service getService();

}
