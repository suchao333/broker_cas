package org.apereo.cas;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * No-Op cipher executor that does nothing for encryption/decryption.
 * This singleton class is hidden from "the world" by being package-private and is exposed to consumers by
 * {@link CipherExecutor}'s static factory method.
 **这是{@link DistributedCacheManager}，它充当缓存实现的facade。
 *
 * *它是通过泛型设计来接受一个键、一个与该键相关联的值和一个输出对象。
 *
 * *虽然大部分价值和产出是同一个，但它们是有意分开的
 *
 * *以避免序列化问题，并提供对最终结果进行转换的灵活性。
 * @author Misagh Moayyed
 * @author Dmitriy Kopylenko
 *
 * @since 4.1
 */
@Slf4j
@Getter
class NoOpCipherExecutor<I, O> implements CipherExecutor<I, O> {

    private static volatile CipherExecutor INSTANCE;

    /**
     * Gets instance.
     *
     * @return the instance
     */
    static <I, O> CipherExecutor<I, O> getInstance() {
        //Double-check pattern here to ensure correctness of only single instance creation in multi-threaded environments
        if (INSTANCE == null) {
            synchronized (NoOpCipherExecutor.class) {
                if (INSTANCE == null) {
                    INSTANCE = new NoOpCipherExecutor<>();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public O encode(final I value, final Object[] parameters) {
        return (O) value;
    }

    @Override
    public O decode(final I value, final Object[] parameters) {
        return (O) value;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
