package org.apereo.cas;

import java.util.Collection;

/**
 * This is {@link ComponentSerializationPlan} that allows modules to register objects and classes
 * they wish to let the underlying engine serialize explicitly. Most specifically, this is used by
 * ticket registry backends such as Kryo that deal with serialized data before passing the object
 * onto memcached where explicit registration is required. The plan is passed to modules that do
 * have a need to register classes explicitly, and engines and libraries such as kryo can explicitly
 * ask the executing plan for all classes that they can handle and register in their own engine.
 **这是{@link ComponentSerializationPlan}，它允许模块注册对象和类
它们希望让底层引擎显式序列化。最具体地说，这是由
票证注册表后端，如Kryo，在传递对象之前处理序列化数据
到需要显式注册的memcached上。计划被传递给
需要显式地注册类，并且像kryo这样的引擎和库可以显式地注册
询问所有可以处理并在自己的引擎中注册的类的执行计划。
 * @author Misagh Moayyed
 * @see ComponentSerializationPlanConfigurator
 * @since 5.2.0
 */
public interface ComponentSerializationPlan {

    /**
     * Register serializable class.
     *
     * @param clazz the clazz to register
     */
    void registerSerializableClass(Class clazz);

    /**
     * Register serializable class.
     *
     * @param clazz the clazz to register
     * @param order the order in which the class will be positioned in the registry of classes
     */
    void registerSerializableClass(Class clazz, Integer order);

    /**
     * Gets registered classes.
     *
     * @return the registered classes
     */
    Collection<Class> getRegisteredClasses();
}
