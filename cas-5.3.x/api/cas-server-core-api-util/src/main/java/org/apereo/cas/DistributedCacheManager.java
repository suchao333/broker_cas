package org.apereo.cas;

import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * This is {@link DistributedCacheManager} that acts as a facade for a cache implementation.
 * It's designed via generics to accept a key, a value associated with that key and an output object.
 * While mostly value and output are one of the same, these are made separate intentionally
 * to avoid serialization issues and provide flexibility to provide transformations on the final result.
 *这是{@link DistributedCacheManager}，它充当缓存实现的facade。
它是通过泛型设计来接受一个键、一个与该键相关联的值和一个输出对象。
虽然大部分价值和产出是同一个，但它们是有意分开的
以避免序列化问题，并提供对最终结果进行转换的灵活性。
 * @param <K> the type parameter
 * @param <V> the type parameter
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@FunctionalInterface
public interface DistributedCacheManager<K extends Serializable, V extends DistributedCacheObject> extends Closeable {

    /**
     * Get item.
     *
     * @param key the key
     * @return the item or null if not found.
     */
    default V get(final K key) {
        return null;
    }

    /**
     * Gets all items in the cache.
     *
     * @return the all
     */
    default Collection<V> getAll() {
        return new ArrayList<>(0);
    }

    /**
     * Set item in the cache.
     *
     * @param key  the key
     * @param item the item to store in the cache
     */
    default void set(final K key, final V item) {
    }

    /**
     * Contains key in the cache?
     *
     * @param key the key
     * @return true /false
     */
    default boolean contains(final K key) {
        return false;
    }

    /**
     * update key/item from the cache and overwrite.
     *
     * @param key the key
     * @param item the item
     */
    default void update(final K key, final V item) {
    }

    /**
     * Remove key/item from the cache.
     *
     * @param key the key
     * @param item the item
     */
    default void remove(final K key, final V item) {
    }

    /**
     * Gets the cache impl name.
     *
     * @return the name
     */
    default String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Find values matching this predicate.
     *
     * @param filter the filter
     * @return the collection
     */
    default Collection<V> findAll(final Predicate<V> filter) {
        return new ArrayList<>(0);
    }

    /**
     * Find values matching this predicate.
     *
     * @param filter the filter
     * @return the collection
     */
    default Optional<V> find(final Predicate<V> filter) {
        final Collection<V> results = findAll(filter);
        if (results.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(results.iterator().next());
    }
}
