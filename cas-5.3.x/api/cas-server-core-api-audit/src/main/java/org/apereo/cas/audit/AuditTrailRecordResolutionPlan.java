package org.apereo.cas.audit;

import org.apereo.inspektr.audit.spi.AuditActionResolver;
import org.apereo.inspektr.audit.spi.AuditResourceResolver;

import java.util.Map;

/**
 * This is {@link AuditTrailRecordResolutionPlan}.
 *审核跟踪记录解决计划
 * @author Misagh Moayyed
 * @since 5.3.0
 */
public interface AuditTrailRecordResolutionPlan {

    /**
     * Register audit resource resolver.
     *注册审计资源解析器
     * @param key      the key
     * @param resolver the resolver
     */
    void registerAuditResourceResolver(String key, AuditResourceResolver resolver);

    /**
     * Register audit action resolver.
     *注册审计操作解析器
     * @param key      the key
     * @param resolver the resolver
     */
    void registerAuditActionResolver(String key, AuditActionResolver resolver);

    /**
     * Register audit action resolvers.
     *注册审计操作解析器。
     * @param resolvers the resolvers
     */
    void registerAuditActionResolvers(Map<String, AuditActionResolver> resolvers);

    /**
     * Register audit resource resolvers.
     *注册审计资源解析器
     * @param resolvers the resolvers
     */
    void registerAuditResourceResolvers(Map<String, AuditResourceResolver> resolvers);

    /**
     * Gets audit resource resolvers.
     * 获取森吉资源解析器
     * @return the audit resource resolvers
     */
    Map<String, AuditResourceResolver> getAuditResourceResolvers();

    /**
     * Gets audit action resolvers.
     * 获取神器操作解析器
     * @return the audit action resolvers
     */
    Map<String, AuditActionResolver> getAuditActionResolvers();
}
