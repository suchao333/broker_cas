package org.apereo.cas.audit;

import org.apereo.cas.authentication.Authentication;
import org.springframework.core.Ordered;

/**
 * Strategy interface to provide principal id tokens from any given authentication event.
 * 策略接口,从任何给定的身份验证事件中提供主id令牌。
 * <p>
 * Useful for authentication scenarios where there is not only one primary principal id available, but additional authentication metadata
 * in addition to custom requirement to compute and show more complex principal identifier for auditing purposes.
 * An example would be compound ids resulted from multi-legged mfa authentications, 'surrogate' authentications, etc.
 *对于不仅只有一个主主体id可用，而且还有其他身份验证元数据的身份验证场景非常有用
 * 除了为审计目的计算和显示更复杂的主体标识符的定制需求之外。
 * 例如，由多腿mfa身份验证、“代理”身份验证等生成的复合id。
 * @author Dmitriy Kopylenko
 * @since 4.2.0
 */
public interface AuditPrincipalIdProvider extends Ordered {

    /**
     * Return principal（主要的，首要的） id from a given authentication event（给定的身份验证事件）.
     *
     * @param authentication authentication event containing the data to computed the final principal id from
     * @param resultValue    the result value that is currently processed by the executing op. May be null.
     * @param exception      the exception that may have occurred as part of the current executing op. May be null.
     * @return computed principal id
     */
    String getPrincipalIdFrom(Authentication authentication, Object resultValue, Exception exception);

    /**
     * Whether this provider can support the authentication transaction to provide a principal id.
     *这个提供者是否可以支持身份验证事务以提供一个主id。
     * @param authentication the authentication transaction.
     * @param resultValue    the result value that is currently processed by the executing op. May be null.
     * @param exception      the exception that may have occurred as part of the current executing op. May be null.
     * @return true /false
     */
    boolean supports(Authentication authentication, Object resultValue, Exception exception);
}
