package org.apereo.cas.audit;

/**
 * This is {@link BaseAuditableExecution}. Provides a simple implementation that just copies data from context to execution result.
 * Useful for subclasses with simple requirements that just need to capture context data at audit points and pass it on as a result.
 *这是{@link BaseAuditableExecution}。提供一个简单的实现，仅将数据从上下文复制到执行结果。
 * 对于有简单需求的子类非常有用，这些子类只需要在审计点捕获上下文数据并将其传递
 * @author Misagh Moayyed
 * @author Dmitriy Kopylenko
 * @since 5.3.0
 */
public abstract class BaseAuditableExecution implements AuditableExecution {

    @Override
    public AuditableExecutionResult execute(final AuditableContext context) {
        return AuditableExecutionResult.of(context);
    }
}
