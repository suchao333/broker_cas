package org.apereo.cas.audit;

/**
 * This is {@link AuditTrailExecutionPlanConfigurer}.
 *
 * @author Misagh Moayyed
 * @since 5.3.0
 */
@FunctionalInterface
public interface AuditTrailExecutionPlanConfigurer {

    /**
     * Configure audit trail execution plan.
     *配置审计跟踪执行计划
     * @param plan the plan
     */
    void configureAuditTrailExecutionPlan(AuditTrailExecutionPlan plan);
}
