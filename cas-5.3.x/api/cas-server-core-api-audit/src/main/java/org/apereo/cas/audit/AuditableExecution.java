package org.apereo.cas.audit;

/**
 * This is {@link AuditableExecution}. This is a strategy interface which acts as an abstraction for audit trail execution
 * at various audit points throughout CAS server and its various modules.
 * 这是{@link AuditableExecution}。这是一个策略接口，充当审计跟踪执行的抽象
 * 在整个CAS服务器及其各个模块的各个审计点上。
 * <p>
 * Implementors of this API are typically annotated with <i>Inspektr's</i> auditing library {@code Audit} annotation
 * to capture data at any particular audit point and make it available to main Inspektr's engine for processing that audit trail data.
 *该API的实现者通常使用<i>Inspektr的</i>审计库{@code Audit}注释
 * 捕获数据在任何特定的审计点，并使它可用的主要Inspektr的引擎处理审计跟踪数据。
 * @author Misagh Moayyed
 * @author Dmitriy Kopylenko
 * @since 5.3.0
 */
@FunctionalInterface
public interface AuditableExecution {

    /**
     * Execute auditable action.
     *
     * @param context the context
     * @return the result
     */
    AuditableExecutionResult execute(AuditableContext context);
}
