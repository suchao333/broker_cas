package org.apereo.cas.web;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.webflow.execution.repository.BadlyFormattedFlowExecutionKeyException;
import org.springframework.webflow.execution.repository.FlowExecutionRepositoryException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@link FlowExecutionExceptionResolver} catches the {@link FlowExecutionRepositoryException}
 * thrown by Spring Webflow when the given flow id no longer exists. This can
 * occur if a particular flow has reached an end state (the id is no longer valid)
 * <p>
 * It will redirect back to the requested URI which should start a new workflow.
 * </p>
 **{@link FlowExecutionExceptionResolver}捕获{@link FlowExecutionRepositoryException}
 *
 * *当给定的流id不再存在时由springwebflow抛出。这个罐子
 *
 * *在特定流已达到结束状态（id不再有效）时发生
 *
 * *<p>
 *
 * *它将重定向回请求的URI，该URI应该启动一个新的工作流。
 *
 * *在</p>
 * @author Scott Battaglia
 * @author Misagh Moayyed
 * @since 3.0.0
 */
@Slf4j
@Getter
public class FlowExecutionExceptionResolver implements HandlerExceptionResolver {
    private String modelKey = "exception.message";

    /**
     * Since FlowExecutionRepositoryException is a common ancestor to these exceptions and other
     * error cases we would likely want to hide from the user, it seems reasonable to check for
     * FlowExecutionRepositoryException.
     *
     * BadlyFormattedFlowExecutionKeyException is specifically ignored by this handler
     * because redirecting to the requested URI with this exception may cause an infinite
     * redirect loop (i.e. when invalid "execution" parameter exists as part of the query string
     *因为FlowExecutionRepositoryException是这些异常和其他异常的共同祖先

     *我们可能希望对用户隐藏的错误案例，进行检查似乎是合理的

     *FlowExecutionRepositoryException。

     *

     *此处理程序特别忽略BadlyFormattedFlowExecutionKeyException

     *因为在此异常情况下重定向到请求的URI可能会导致无限大的

     *重定向循环（即当查询字符串中存在无效的“执行”参数时
     */
    @Override
    public ModelAndView resolveException(final HttpServletRequest request,
        final HttpServletResponse response, final Object handler,
        final Exception exception) {


        if (!(exception instanceof FlowExecutionRepositoryException) || exception instanceof BadlyFormattedFlowExecutionKeyException) {
            LOGGER.debug("Ignoring the received exception due to a type mismatch", exception);
            return null;
        }

        final String urlToRedirectTo = request.getRequestURI()
                + (request.getQueryString() != null ? '?'
                + request.getQueryString() : StringUtils.EMPTY);

        LOGGER.debug("Error getting flow information for URL [{}]", urlToRedirectTo, exception);
        final Map<String, Object> model = new HashMap<>();
        model.put(this.modelKey, StringEscapeUtils.escapeHtml4(exception.getMessage()));

        return new ModelAndView(new RedirectView(urlToRedirectTo), model);
    }
}
