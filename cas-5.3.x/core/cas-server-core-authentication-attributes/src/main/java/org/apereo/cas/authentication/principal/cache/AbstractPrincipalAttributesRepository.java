package org.apereo.cas.authentication.principal.cache;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apereo.cas.authentication.principal.Principal;
import org.apereo.cas.authentication.principal.PrincipalAttributesRepository;
import org.apereo.cas.util.CollectionUtils;
import org.apereo.cas.util.spring.ApplicationContextProvider;
import org.apereo.services.persondir.IPersonAttributeDao;
import org.apereo.services.persondir.IPersonAttributes;
import org.apereo.services.persondir.support.merger.IAttributeMerger;
import org.apereo.services.persondir.support.merger.MultivaluedAttributeMerger;
import org.apereo.services.persondir.support.merger.NoncollidingAttributeAdder;
import org.apereo.services.persondir.support.merger.ReplacingAttributeAdder;
import org.springframework.context.ApplicationContext;
import java.io.Closeable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lombok.Setter;

/**
 * Parent class for retrieval principals attributes, provides operations
 * around caching, merging of attributes.
 * 用于检索Principal属性的父类，提供操作
 * 关于缓存，属性的合并。
 * @author Misagh Moayyed
 * @since 4.2
 */
@Slf4j
@ToString
@Getter
@Setter
@EqualsAndHashCode(of = {"timeUnit", "expiration"})
public abstract class AbstractPrincipalAttributesRepository implements PrincipalAttributesRepository, Closeable {

    /**
     * Default cache expiration time unit.
     * 默认失效时间单位
     */
    private static final String DEFAULT_CACHE_EXPIRATION_UNIT = TimeUnit.HOURS.name();

    /**
     * Default expiration lifetime based on the default time unit.
     * 基于默认过期单位的生存期。
     */
    private static final long DEFAULT_CACHE_EXPIRATION_DURATION = 2;

    private static final long serialVersionUID = 6350245643948535906L;

    /**
     * The expiration time.
     * 过期时间。
     */
    protected long expiration;

    /**
     * Expiration time unit.
     * 过期时间单位。
     */
    protected String timeUnit;

    /**
     * The merging strategy that deals with existing principal attributes
     * and those that are retrieved from the source. By default, existing attributes
     * are ignored and the source is always consulted.
     * *处理现有主要属性的合并策略
     *
     * *以及那些从源头检索到的。默认情况下，现有属性
     *
     * *都被忽略了，总是要咨询消息来源。
     */
    protected MergingStrategy mergingStrategy;

    /**
     * Defines the merging strategy options.
     */
    public enum MergingStrategy {

        /**
         * Replace attributes.
         * 属性替换
         */
        REPLACE, /**
         * Add attributes.
         * 属性添加
         */
        ADD, /**
         * No merging.
         * 不合并
         */
        NONE, /**
         * Multivalued attributes.
         * 多值属性。
         */
        MULTIVALUED;

        /**
         * Get attribute merger.
         *
         * @return the attribute merger
         */
        public IAttributeMerger getAttributeMerger() {
            final String name = this.name().toUpperCase();
            switch(name.toUpperCase()) {
                case "REPLACE":
                    return new ReplacingAttributeAdder();
                case "ADD":
                    return new NoncollidingAttributeAdder();
                case "MULTIVALUED":
                    return new MultivaluedAttributeMerger();
                default:
                    return null;
            }
        }
    }

    private transient IPersonAttributeDao attributeRepository;

    /**
     * Instantiates a new principal attributes repository.
     * Simply used buy
     * 实例化新的主体属性存储库。
     * 简单使用购买
     *
     */
    protected AbstractPrincipalAttributesRepository() {
        this(DEFAULT_CACHE_EXPIRATION_DURATION, DEFAULT_CACHE_EXPIRATION_UNIT);
    }

    /**
     * Instantiates a new principal attributes repository.
     * 实例化新的主体属性存储库。
     * @param expiration the expiration
     * @param timeUnit   the time unit
     */
    public AbstractPrincipalAttributesRepository(final long expiration, final String timeUnit) {
        this.expiration = expiration;
        this.timeUnit = timeUnit;
    }

    /**
     * Convert person attributes to principal attributes.
     * 转换个人属性到委托人属性
     * @param attributes person attributes
     * @return principal attributes
     */
    protected Map<String, Object> convertPersonAttributesToPrincipalAttributes(final Map<String, List<Object>> attributes) {
        return attributes.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().size() == 1
            ? entry.getValue().get(0) : entry.getValue(), (e, f) -> f == null ? e : f));
    }

    /***
     * Convert principal attributes to person attributes.
     * 将委托人属性转换为个人属性
     * @param p  the principal carrying attributes
     * @return person attributes
     */
    private static Map<String, List<Object>> convertPrincipalAttributesToPersonAttributes(final Principal p) {
        final Map<String, List<Object>> convertedAttributes = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        final Map<String, Object> principalAttributes = p.getAttributes();
        principalAttributes.entrySet().forEach(entry -> {
            final Object values = entry.getValue();
            final String key = entry.getKey();
            if (values instanceof List) {
                convertedAttributes.put(key, (List) values);
            } else {
                convertedAttributes.put(key, CollectionUtils.wrap(values));
            }
        });
        return convertedAttributes;
    }

    /**
     * Obtains（获得） attributes first from the repository by calling
     * {@link org.apereo.services.persondir.IPersonAttributeDao#getPerson(String)}.
     *
     * @param id the person id to locate in the attribute repository
     * @return the map of attributes
     */
    protected Map<String, List<Object>> retrievePersonAttributesToPrincipalAttributes(final String id) {
        final IPersonAttributes attrs = getAttributeRepository().getPerson(id);
        if (attrs == null) {
            LOGGER.debug("Could not find principal [{}] in the repository so no attributes are returned.", id);
            return new HashMap<>(0);
        }
        final Map<String, List<Object>> attributes = attrs.getAttributes();
        if (attributes == null) {
            LOGGER.debug("Principal [{}] has no attributes and so none are returned.", id);
            return new HashMap<>(0);
        }
        return attributes;
    }

    /**
     * 获取 校验主体的所有属性，验证主体
     * @param p the principal whose attributes need to be retrieved.
     * @return
     */
    @Override
    public Map<String, Object> getAttributes(final Principal p) {
        //从缓存中获取属性，如果有直接返回，抽象方法自己实现
        final Map<String, Object> cachedAttributes = getPrincipalAttributes(p);
        if (cachedAttributes != null && !cachedAttributes.isEmpty()) {
            LOGGER.debug("Found [{}] cached attributes for principal [{}] that are [{}]", cachedAttributes.size(), p.getId(), cachedAttributes);
            return cachedAttributes;
        }
        //获取属性库有东西就返回Principal属性
        if (getAttributeRepository() == null) {
            LOGGER.debug("No attribute repository is defined for [{}]. Returning default principal attributes for [{}]", getClass().getName(), p.getId());
            return cachedAttributes;
        }
        //先获取 所有的用户属性
        final Map<String, List<Object>> sourceAttributes = retrievePersonAttributesToPrincipalAttributes(p.getId());
        LOGGER.debug("Found [{}] attributes for principal [{}] from the attribute repository.", sourceAttributes.size(), p.getId());
        //如果没有合并策略直接转换为Principal属性 ，缓存并返回
        if (this.mergingStrategy == null || this.mergingStrategy.getAttributeMerger() == null) {
            LOGGER.debug("No merging strategy found, so attributes retrieved from the repository will be used instead.");
            return convertAttributesToPrincipalAttributesAndCache(p, sourceAttributes);
        }
        //将Principal属性转为Person属性
        final Map<String, List<Object>> principalAttributes = convertPrincipalAttributesToPersonAttributes(p);
        LOGGER.debug("Merging current principal attributes with that of the repository via strategy [{}]", this.mergingStrategy);
        try {
            //属性合并
            final Map<String, List<Object>> mergedAttributes = this.mergingStrategy.getAttributeMerger().mergeAttributes(principalAttributes, sourceAttributes);
            //转换为Principal属性
            return convertAttributesToPrincipalAttributesAndCache(p, mergedAttributes);
        } catch (final Exception e) {
            final StringBuilder builder = new StringBuilder();
            builder.append(e.getClass().getName().concat("-"));
            if (StringUtils.isNotBlank(e.getMessage())) {
                builder.append(e.getMessage());
            }
            LOGGER.error("The merging strategy [{}] for [{}] has failed to produce principal attributes because: [{}]. "
                + "This usually is indicative of a bug and/or configuration mismatch. CAS will skip the merging process "
                + "and will return the original collection of principal attributes [{}]", this.mergingStrategy, p.getId(),
                builder.toString(), principalAttributes);
            return convertAttributesToPrincipalAttributesAndCache(p, principalAttributes);
        }
    }

    /**
     * Convert attributes to principal attributes and cache.
     * 缓存属性
     * @param p                the p
     * @param sourceAttributes the source attributes
     * @return the map
     */
    private Map<String, Object> convertAttributesToPrincipalAttributesAndCache(final Principal p, final Map<String, List<Object>> sourceAttributes) {
        final Map<String, Object> finalAttributes = convertPersonAttributesToPrincipalAttributes(sourceAttributes);
        addPrincipalAttributes(p.getId(), finalAttributes);
        return finalAttributes;
    }

    /**
     * Add principal attributes into the underlying cache instance.
     *
     * @param id         identifier used by the cache as key.
     * @param attributes attributes to cache
     * @since 4.2
     */
    protected abstract void addPrincipalAttributes(String id, Map<String, Object> attributes);

    /**
     * Gets principal attributes from cache.
     *
     * @param p the principal
     * @return the principal attributes from cache
     */
    protected abstract Map<String, Object> getPrincipalAttributes(Principal p);

    private IPersonAttributeDao getAttributeRepository() {
        try {
            if (this.attributeRepository == null) {
                final ApplicationContext context = ApplicationContextProvider.getApplicationContext();
                if (context != null) {
                    return context.getBean("attributeRepository", IPersonAttributeDao.class);
                }
                LOGGER.warn("No application context could be retrieved, so no attribute repository instance can be determined.");
            }
        } catch (final Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
        return this.attributeRepository;
    }

}
