package org.apereo.cas.authentication;

import lombok.extern.slf4j.Slf4j;
import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.Setter;

/**
 * Default AuthenticationAttributeReleasePolicy implementation.
 * 默认授权属性发布规则
 * @author Daniel Frett
 * @since 5.2.0
 */
@Slf4j
@Setter
public class DefaultAuthenticationAttributeReleasePolicy implements AuthenticationAttributeReleasePolicy {

    private Collection<String> attributesToRelease;

    @Nonnull
    private Set<String> attributesToNeverRelease = new HashSet<>();

    /**
     * Add additional attributes that should never be released in a validation response.
     *添加不应在验证响应中释放的附加属性。
     * @param attrs Additional attributes to never release
     */
    public void addAttributesToNeverRelease(final Collection<String> attrs) {
        if (attrs != null) {
            attributesToNeverRelease.addAll(attrs);
        }
    }

    /**
     * Return authentications attributes that we are allowed to release to client systems.
     *返回允许我们发布到客户端系统的身份验证属性。
     * @param authentication The authentication object we are processing.
     * @return The attributes to be released
     */
    @Override
    public Map<String, Object> getAuthenticationAttributesForRelease(@Nonnull final Authentication authentication) {
        final HashMap<String, Object> attrs = new HashMap<>(authentication.getAttributes());
        // remove any attributes explicitly prohibited
        //删除明确禁止发布的任何属性
        attrs.keySet().removeAll(attributesToNeverRelease);
        // only apply whitelist if it contains attributes
        // 仅当白名单包含属性时应用它，获取交集
        if (attributesToRelease != null && !attributesToRelease.isEmpty()) {
            //获取交集
            attrs.keySet().retainAll(attributesToRelease);
        }
        return attrs;
    }
}
