package org.apereo.cas.config;

import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.CipherExecutor;
import org.apereo.cas.authentication.AuthenticationEventExecutionPlanConfigurer;
import org.apereo.cas.authentication.AuthenticationMetaDataPopulator;
import org.apereo.cas.authentication.metadata.AuthenticationCredentialTypeMetaDataPopulator;
import org.apereo.cas.authentication.metadata.CacheCredentialsCipherExecutor;
import org.apereo.cas.authentication.metadata.CacheCredentialsMetaDataPopulator;
import org.apereo.cas.authentication.metadata.RememberMeAuthenticationMetaDataPopulator;
import org.apereo.cas.authentication.metadata.SuccessfulHandlerMetaDataPopulator;
import org.apereo.cas.configuration.CasConfigurationProperties;
import org.apereo.cas.configuration.model.support.clearpass.ClearpassProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This is {@link CasCoreAuthenticationMetadataConfiguration}.
 * 核心授权元数据配置
 * @author Misagh Moayyed
 * @author Dmitriy Kopylenko
 * @since 5.1.0
 */
@Configuration("casCoreAuthenticationMetadataConfiguration")
@EnableConfigurationProperties(CasConfigurationProperties.class)
@Slf4j
public class CasCoreAuthenticationMetadataConfiguration {
    @Autowired
    private CasConfigurationProperties casProperties;
    //成功的处理程序元数据填充器
    @ConditionalOnMissingBean(name = "successfulHandlerMetaDataPopulator")
    @Bean
    public AuthenticationMetaDataPopulator successfulHandlerMetaDataPopulator() {

        return new SuccessfulHandlerMetaDataPopulator();
    }
    //记住我授权元数据填充器
    @ConditionalOnMissingBean(name = "rememberMeAuthenticationMetaDataPopulator")
    @Bean
    public AuthenticationMetaDataPopulator rememberMeAuthenticationMetaDataPopulator() {
        return new RememberMeAuthenticationMetaDataPopulator();
    }
    //缓存凭据密码执行器(系统对接的时候使用)
    @ConditionalOnMissingBean(name = "cacheCredentialsCipherExecutor")
    @Bean
    public CipherExecutor cacheCredentialsCipherExecutor() {
        final ClearpassProperties cp = casProperties.getClearpass();
        //是否启用CAS缓存凭证
        if (cp.isCacheCredential()) {
            //是否启用加密操作
            if (cp.getCrypto().isEnabled()) {
                return new CacheCredentialsCipherExecutor(cp.getCrypto().getEncryption().getKey(),
                    cp.getCrypto().getSigning().getKey(),
                    cp.getCrypto().getAlg());
            }
            LOGGER.warn("Cas is configured to capture and cache credentials via Clearpass yet crypto operations for the cached password are "
                + "turned off. Consider enabling the crypto configuration in CAS settings that allow the system to sign & encrypt the captured credential.");
        }
        return CipherExecutor.noOp();
    }
    //授权凭证类型元数据 填充器
    @ConditionalOnMissingBean(name = "authenticationCredentialTypeMetaDataPopulator")
    @Bean
    public AuthenticationMetaDataPopulator authenticationCredentialTypeMetaDataPopulator() {
        return new AuthenticationCredentialTypeMetaDataPopulator();
    }
    //cas核心身份验证元数据身份验证事件执行计划配置器
    @ConditionalOnMissingBean(name = "casCoreAuthenticationMetadataAuthenticationEventExecutionPlanConfigurer")
    @Bean
    public AuthenticationEventExecutionPlanConfigurer casCoreAuthenticationMetadataAuthenticationEventExecutionPlanConfigurer() {
        return plan -> {
            plan.registerMetadataPopulator(successfulHandlerMetaDataPopulator());
            plan.registerMetadataPopulator(rememberMeAuthenticationMetaDataPopulator());
            plan.registerMetadataPopulator(authenticationCredentialTypeMetaDataPopulator());

            final ClearpassProperties cp = casProperties.getClearpass();
            //是否启用缓存凭证
            if (cp.isCacheCredential()) {
                LOGGER.warn("Cas is configured to capture and cache credentials via Clearpass. Sharing the user credential with other applications "
                    + "is generally NOT recommended, may lead to security vulnerabilities and MUST only be used as a last resort .");
                //注册元数据填充器
                plan.registerMetadataPopulator(new CacheCredentialsMetaDataPopulator(cacheCredentialsCipherExecutor()));
            }
        };
    }
}
