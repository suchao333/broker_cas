package org.apereo.cas.services.resource;

import org.apache.commons.lang3.StringUtils;
import org.apereo.cas.services.RegisteredService;

/**
 * This is {@link DefaultRegisteredServiceResourceNamingStrategy}. This class provides the default
 * naming for resource based services.
 **这是{@link DefaultRegisteredServiceResourceNamingStrategy}。此类提供默认
 *
 * *为基于资源的服务命名。
 * @author Travis Schmidt
 * @since 5.3.0
 */
public class DefaultRegisteredServiceResourceNamingStrategy implements RegisteredServiceResourceNamingStrategy {

    /**
     * Method creates a filename to store the service.
     *方法创建存储服务的文件名。
     * @param service - Service to be stored. 要存储的服务。
     * @param extension - extension to use for the file. 用于文件的扩展名。
     * @return - String representing file name. 表示文件名的字符串。
     */
    @Override
    public String build(final RegisteredService service, final String extension) {
        return StringUtils.remove(service.getName(), ' ') + '-' + service.getId() + '.' + extension;
    }
}
