package org.apereo.cas.authentication;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.configuration.model.support.mfa.MultifactorAuthenticationProviderBypassProperties;
import org.apereo.cas.services.MultifactorAuthenticationProvider;
import org.apereo.cas.services.VariegatedMultifactorAuthenticationProvider;
import org.apereo.cas.util.CollectionUtils;
import org.springframework.context.ApplicationContext;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This is {@link MultifactorAuthenticationUtils}.
 *
 * @author Misagh Moayyed
 * @since 5.2.0
 */
@Slf4j
@UtilityClass
public class MultifactorAuthenticationUtils {
    /**
     * New multifactor authentication provider bypass multifactor.
     *新的多因素身份验证提供程序绕过了多因素。
     * @param props the props
     * @return the multifactor authentication provider bypass
     */
    public static MultifactorAuthenticationProviderBypass newMultifactorAuthenticationProviderBypass(
        final MultifactorAuthenticationProviderBypassProperties props) {

        final ChainingMultifactorAuthenticationProviderBypass bypass
                = new ChainingMultifactorAuthenticationProviderBypass();
        bypass.addBypass(new DefaultMultifactorAuthenticationProviderBypass(props));

        switch (props.getType()) {
            case GROOVY:
                bypass.addBypass(new GroovyMultifactorAuthenticationProviderBypass(props));
                break;
            case REST:
                bypass.addBypass(new RestMultifactorAuthenticationProviderBypass(props));
                break;
            case DEFAULT:
            default:
                break;
        }
        return bypass;
    }

    /**
     * Gets all multifactor authentication providers from application context.
     * 从应用程序上下文获取所有多因素身份验证提供程序。
     * @param applicationContext the application context
     * @return the all multifactor authentication providers from application context
     */
    public static Map<String, MultifactorAuthenticationProvider> getAvailableMultifactorAuthenticationProviders(
        final ApplicationContext applicationContext) {
        try {
            return applicationContext.getBeansOfType(MultifactorAuthenticationProvider.class, false, true);
        } catch (final Exception e) {
            LOGGER.debug("No beans of type [{}] are available in the application context. "
                    + "CAS may not be configured to handle multifactor authentication requests in absence of a provider",
                MultifactorAuthenticationProvider.class);
        }
        return new HashMap<>(0);
    }

    /**
     * Gets multifactor authentication providers by ids.
     * 按ID获取多因素身份验证提供程序。
     * @param ids                the ids
     * @param applicationContext the application context
     * @return the multifactor authentication providers by ids
     */
    public static Collection<MultifactorAuthenticationProvider> getMultifactorAuthenticationProvidersByIds(final Collection<String> ids,
                                                                                                           final ApplicationContext applicationContext) {
        final Map<String, MultifactorAuthenticationProvider> available = getAvailableMultifactorAuthenticationProviders(applicationContext);
        final Collection<MultifactorAuthenticationProvider> values = available.values();
        return values.stream()
            .filter(p -> ids.contains(p.getId()))
            .collect(Collectors.toSet());
    }

    /**
     * Method returns an Optional that will contain a MultifactorAuthenticationProvider that has the
     * same id as the passed providerId parameter.
     *方法返回一个可选的，它将包含具有
     *与传递的providerId参数相同的id。
     * @param providerId - the id to match
     * @param context - ApplicationContext
     * @return - Optional
     */
    public static Optional<MultifactorAuthenticationProvider> getMultifactorAuthenticationProviderById(final String providerId,
                                                                                                       final ApplicationContext context) {
        return getAvailableMultifactorAuthenticationProviders(context).values().stream()
            .filter(p -> p.getId().equals(providerId)).findFirst();
    }

    /**
     * Consolidate providers collection.
     * If the provider is multi-instance in the collection, consolidate and flatten.
     **合并提供程序集合。如果提供程序是集合中的多实例，请合并并展平。
     * @param providers the providers
     * @return the collection
     */
    public static Collection<MultifactorAuthenticationProvider> flattenProviders(final Collection<? extends MultifactorAuthenticationProvider> providers) {
        final Collection<MultifactorAuthenticationProvider> flattenedProviders = new HashSet<>();
        providers.forEach(p -> flattenedProviders.addAll(flattenProvider(p)));
        return flattenedProviders;
    }

    /**
     * Returns the collection of providers in a VariegatedMultifactorAuthenticationProvider or wraps the passed provider
     * into a Collection.
     *返回VariegatedMultifactorAuthenticationProvider中的提供程序集合或包装传递的提供程序成为一个集合。
     * @param provider the provider
     * @return - the collection
     */
    public static Collection<MultifactorAuthenticationProvider> flattenProvider(final MultifactorAuthenticationProvider provider) {
        if (provider instanceof VariegatedMultifactorAuthenticationProvider) {
            return ((VariegatedMultifactorAuthenticationProvider) provider).getProviders();
        }
        return CollectionUtils.wrap(provider);
    }
}
