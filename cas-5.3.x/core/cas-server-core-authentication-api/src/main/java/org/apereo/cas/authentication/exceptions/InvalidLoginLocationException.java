package org.apereo.cas.authentication.exceptions;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.security.auth.login.AccountException;

/**
 * Describes an error condition where authentication occurs from a location that is disallowed by security policy
 * applied to the underlying user account.
 *描述从安全策略不允许的位置进行身份验证的错误情况
 *应用于基础用户帐户。
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@Slf4j
@NoArgsConstructor
public class InvalidLoginLocationException extends AccountException {

    private static final long serialVersionUID = 5745711263227480194L;

    /**
     * Instantiates a new invalid login location exception.
     *实例化新的无效登录位置异常。
     * @param message the message
     */
    public InvalidLoginLocationException(final String message) {
        super(message);
    }
}
