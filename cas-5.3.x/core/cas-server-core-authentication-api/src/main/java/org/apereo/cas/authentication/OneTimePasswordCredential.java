package org.apereo.cas.authentication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * Describes a one-time-password credential that contains an optional unique identifier and required password.
 * The primary difference between this component and {@link UsernamePasswordCredential} is that the username/ID is optional
 * in the former and requisite in the latter.
 * <p>
 * This class implements {@link CredentialMetaData} since the one-time-password is safe for long-term storage after
 * authentication. Note that metadata is stored only _after_ authentication, at which time the OTP has already
 * been consumed and by definition is no longer useful for authentication.
 *描述包含可选唯一标识符和必需密码的一次性密码凭据。
 *这个组件和{@link UsernamePasswordCredential}之间的主要区别是username/ID是可选的
 *前者是必要的，后者是必要的。
 *<p>
 *这个类实现{@link CredentialMetaData}，因为一次性密码在以后的长期存储中是安全的
 *身份验证。请注意，元数据仅在认证后存储，此时OTP已经
 *已使用，并且根据定义，不再对身份验证有用。
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@Slf4j
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {"password"})
public class OneTimePasswordCredential extends AbstractCredential {

    /**
     * Serialization version marker.
     */
    private static final long serialVersionUID = 1892587671827699709L;

    /**
     * One-time password.
     * 一次性密码。
     */
    private String password;

    /**
     * Optional unique identifier.
     * 可选的唯一标识符。
     */
    private String id;

    @JsonCreator
    public OneTimePasswordCredential(@JsonProperty("id") final String id, @JsonProperty("password") final String password) {
        this.password = password;
        this.id = id;
    }
}
