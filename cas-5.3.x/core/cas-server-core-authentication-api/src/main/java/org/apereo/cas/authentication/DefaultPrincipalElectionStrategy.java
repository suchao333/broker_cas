package org.apereo.cas.authentication;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.authentication.principal.Principal;
import org.apereo.cas.authentication.principal.PrincipalFactory;
import org.apereo.cas.authentication.principal.PrincipalFactoryUtils;

import java.util.Collection;
import java.util.Map;

/** 默认委托者选举策略
 * This is {@link DefaultPrincipalElectionStrategy} that selects the primary principal
 * to be the first principal in the chain of authentication history.
 **这是选择主主体的{@link DefaultPrincipalElectionStrategy}
 * *成为身份验证历史链中的第一个主体。
 *
 * @author Misagh Moayyed
 * @since 4.2.0
 */
@Slf4j
@RequiredArgsConstructor
public class DefaultPrincipalElectionStrategy implements PrincipalElectionStrategy {

    private static final long serialVersionUID = 6704726217030836315L;

    private final PrincipalFactory principalFactory;

    public DefaultPrincipalElectionStrategy() {
        this(PrincipalFactoryUtils.newPrincipalFactory());
    }

    @Override
    public Principal nominate(final Collection<Authentication> authentications,
                              final Map<String, Object> principalAttributes) {
        final Principal principal = getPrincipalFromAuthentication(authentications);
        final Principal finalPrincipal = this.principalFactory.createPrincipal(principal.getId(), principalAttributes);
        LOGGER.debug("Nominated [{}] as the primary principal", finalPrincipal);
        return finalPrincipal;
    }

    /**
     * Gets principal from authentication.
     *
     * @param authentications the authentications
     * @return the principal from authentication
     */
    protected Principal getPrincipalFromAuthentication(final Collection<Authentication> authentications) {
        return authentications.iterator().next().getPrincipal();
    }
}
