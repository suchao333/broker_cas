package org.apereo.cas.authentication.metadata;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.authentication.AuthenticationBuilder;
import org.apereo.cas.authentication.AuthenticationHandler;
import org.apereo.cas.authentication.AuthenticationTransaction;
import org.apereo.cas.authentication.Credential;
import org.apereo.cas.util.CollectionUtils;
import java.util.HashSet;
import java.util.Set;

/**
 * Sets an authentication attribute containing the collection of authentication handlers (by name) that successfully
 * authenticated credential. The attribute name is given by {@link AuthenticationHandler#SUCCESSFUL_AUTHENTICATION_HANDLERS}.
 * This component provides a simple method to inject successful handlers into the CAS ticket validation
 * response to support level of assurance and MFA use cases.
 **设置一个身份验证属性，该属性包含成功的身份验证处理程序的集合（按名称）
 *
 * *已验证凭据。属性名由{@link AuthenticationHandler\SUCCESSFUL_authenticationhandlers}给出。
 *
 * *这个组件提供了一个简单的方法来将成功的处理程序注入到CAS票证验证中
 *
 * *响应保障水平和多功能分析用例。
 * @author Marvin S. Addison
 * @author Alaa Nassef
 * @since 4.0.0
 */
@Slf4j
@ToString(callSuper = true)
public class SuccessfulHandlerMetaDataPopulator extends BaseAuthenticationMetaDataPopulator {

    @Override
    public void populateAttributes(final AuthenticationBuilder builder, final AuthenticationTransaction transaction) {
        Set<String> successes = builder.getSuccesses().keySet();
        if (successes.isEmpty()) {
            successes = new HashSet(successes);
        }
        builder.mergeAttribute(AuthenticationHandler.SUCCESSFUL_AUTHENTICATION_HANDLERS, CollectionUtils.wrap(successes));
    }

    @Override
    public boolean supports(final Credential credential) {
        return true;
    }
}
