package org.apereo.cas.authentication;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import lombok.ToString;
import lombok.Getter;
import lombok.Setter;

/**
 * This is {@link BasicIdentifiableCredential}, a simple credential implementation
 * that is only recognized by its id. The id generally represents an authentication token
 * encrypted in some fashion.
 * 这是{@link BasicIdentifiableCredential}，一个简单的凭证实现
 * 它只能由其id识别。该id通常表示身份验证令牌
 * 以某种方式加密。
 * @author Misagh Moayyed
 * @since 4.2.0
 */
@Slf4j
@ToString
@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class BasicIdentifiableCredential implements Credential {

    private static final long serialVersionUID = -700605020472810939L;

    private String id;
}
