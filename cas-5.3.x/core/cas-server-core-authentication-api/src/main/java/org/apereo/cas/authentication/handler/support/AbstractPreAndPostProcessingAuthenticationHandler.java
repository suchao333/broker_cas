package org.apereo.cas.authentication.handler.support;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.authentication.AbstractAuthenticationHandler;
import org.apereo.cas.authentication.AuthenticationHandlerExecutionResult;
import org.apereo.cas.authentication.BasicCredentialMetaData;
import org.apereo.cas.authentication.Credential;
import org.apereo.cas.authentication.DefaultAuthenticationHandlerExecutionResult;
import org.apereo.cas.authentication.MessageDescriptor;
import org.apereo.cas.authentication.PrePostAuthenticationHandler;
import org.apereo.cas.authentication.PreventedException;
import org.apereo.cas.authentication.principal.Principal;
import org.apereo.cas.authentication.principal.PrincipalFactory;
import org.apereo.cas.services.ServicesManager;

import javax.security.auth.login.FailedLoginException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract authentication handler that allows deployers to utilize the bundled
 * AuthenticationHandlers while providing a mechanism to perform tasks before
 * and after authentication.
 *抽象身份验证处理程序，允许部署人员利用捆绑的
 *AuthenticationHandlers，同时提供执行任务的机制
 *在认证之后。
 * @author Scott Battaglia
 * @author Marvin S. Addison
 * @since 3.1
 */
@Slf4j
public abstract class AbstractPreAndPostProcessingAuthenticationHandler extends AbstractAuthenticationHandler implements PrePostAuthenticationHandler {

    public AbstractPreAndPostProcessingAuthenticationHandler(final String name, final ServicesManager servicesManager, final PrincipalFactory principalFactory,
                                                             final Integer order) {
        super(name, servicesManager, principalFactory, order);
    }

    @Override
    public AuthenticationHandlerExecutionResult authenticate(final Credential credential) throws GeneralSecurityException, PreventedException {
        if (!preAuthenticate(credential)) {
            throw new FailedLoginException();
        }
        return postAuthenticate(credential, doAuthentication(credential));
    }

    /**
     * Performs the details of authentication and returns an authentication handler result on success.
     *执行身份验证的详细信息，并在成功时返回身份验证处理程序结果。
     * @param credential Credential to authenticate. 要验证的凭据。
     * @return Authentication handler result on success.
     * @throws GeneralSecurityException On authentication failure that is thrown out to the caller of
     *                                  {@link #authenticate(Credential)}.
     * @throws PreventedException       On the indeterminate case when authentication is prevented.
     */
    protected abstract AuthenticationHandlerExecutionResult doAuthentication(Credential credential) throws GeneralSecurityException, PreventedException;

    /**
     * Helper method to construct a handler result on successful authentication events.
     *Helper方法，以在成功的身份验证事件上构造处理程序结果。
     *
     * @param credential the credential on which the authentication was successfully performed.
     *                   Note that this credential instance may be different from what was originally provided
     *                   as transformation of the username may have occurred, if one is in fact defined.
     *                   成功执行身份验证的凭据。请注意，此凭据实例可能与最初提供的不同因为用户名的转换可能已经发生，如果实际上定义了一个用户名。
     * @param principal  the resolved principal 解析的委托人
     * @param warnings   the warnings 警告
     * @return the constructed handler result 操作处理结果
     */
    protected AuthenticationHandlerExecutionResult createHandlerResult(@NonNull final Credential credential,
                                                                       @NonNull final Principal principal,
                                                                       final List<MessageDescriptor> warnings) {
        return new DefaultAuthenticationHandlerExecutionResult(this, new BasicCredentialMetaData(credential), principal, warnings);
    }

    /**
     * Helper method to construct a handler result
     * on successful authentication events.
     **构造处理程序结果的助手方法
     * *在成功的身份验证事件上。
     * @param credential the credential on which the authentication was successfully performed.
     *                   Note that this credential instance may be different from what was originally provided
     *                   as transformation of the username may have occurred, if one is in fact defined.
     *                   成功执行身份验证的凭据。请注意，此凭据实例可能与最初提供的不同因为用户名的转换可能已经发生，如果实际上定义了一个用户名。
     * @param principal  the resolved principal
     * @return the constructed handler result
     */
    protected AuthenticationHandlerExecutionResult createHandlerResult(@NonNull final Credential credential,
                                                                       @NonNull final Principal principal) {
        return new DefaultAuthenticationHandlerExecutionResult(this, new BasicCredentialMetaData(credential),
            principal, new ArrayList<>(0));
    }
}
