package org.apereo.cas.authentication.policy;

import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.authentication.Authentication;
import org.apereo.cas.authentication.ContextualAuthenticationPolicy;
import org.apereo.cas.authentication.ContextualAuthenticationPolicyFactory;
import org.apereo.cas.services.RegisteredService;
import org.apereo.cas.services.ServiceContext;

/** Contextual（上下文）
 * Produces {@link ContextualAuthenticationPolicy} instances that are satisfied(满足) iff the given {@link Authentication}
 * was created by authenticating credentials by all handlers named in
 * {@link RegisteredService#getRequiredHandlers()}.
 * 生成{@link ContextualAuthenticationPolicy}实例，这些实例在给定的{@link Authentication}时得到满足
 * 是通过对中名为的所有处理程序验证凭据而创建的
 *{@link RegisteredService\getRequiredHandlers（）}。
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@Slf4j
public class RequiredHandlerAuthenticationPolicyFactory implements ContextualAuthenticationPolicyFactory<ServiceContext> {

    @Override
    public ContextualAuthenticationPolicy<ServiceContext> createPolicy(final ServiceContext context) {
        return new ContextualAuthenticationPolicy<ServiceContext>() {

            @Override
            public ServiceContext getContext() {
                return context;
            }

            @Override
            public boolean isSatisfiedBy(final Authentication authentication) {
                return context.getRegisteredService().getRequiredHandlers().stream()
                        .allMatch(required -> authentication.getSuccesses().containsKey(required));
            }
        };
    }
}
