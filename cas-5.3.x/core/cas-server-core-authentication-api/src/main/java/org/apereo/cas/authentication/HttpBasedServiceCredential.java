package org.apereo.cas.authentication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apereo.cas.services.RegisteredService;

import java.net.URL;

/**
 * A credential representing an HTTP endpoint given by a URL. Authenticating the credential usually involves
 * contacting the endpoint via the URL and observing the resulting connection (e.g. SSL certificate) and response
 * (e.g. status, headers).
 *表示由URL给定的HTTP端点的凭据。认证凭证通常包括
 *通过URL联系端点并观察结果连接（例如SSL证书）和响应
 *（例如状态、标题）。
 * @author Scott Battaglia
 * @author Marvin S. Addison
 * @since 3.0.0
 */
@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class HttpBasedServiceCredential extends AbstractCredential {

    /**
     * Unique Serializable ID.
     */
    private static final long serialVersionUID = 1492607216336354503L;

    /**
     * The callbackURL to check that identifies the application.
     * 要检查的用于标识应用程序的回调URL。
     */
    private URL callbackUrl;

    /**
     * The registered service associated with this callback.
     * 与此回调关联的已注册服务。
     **/
    private RegisteredService service;

    @JsonCreator
    @SneakyThrows
    public HttpBasedServiceCredential(@JsonProperty("callbackUrl") final String callbackUrl,
                                      @JsonProperty("service") final RegisteredService service) {
        this.callbackUrl = new URL(callbackUrl);
        this.service = service;
    }

    @JsonIgnore
    @Override
    public String getId() {
        return this.callbackUrl.toExternalForm();
    }
}
