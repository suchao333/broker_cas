package org.apereo.cas.authentication;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * Basic credential metadata implementation that stores the original credential ID and the original credential type.
 * This can be used as a simple converter for any {@link Credential} that doesn't implement {@link CredentialMetaData}.
 **存储原始凭证ID和原始凭证类型的基本凭证元数据实现。
 * *这可以作为任何{@link Credential}
 * 不实现{@link CredentialMetaData}的简单转换器。
 * @author Marvin S. Addison
 * @since 4.0.0
 */
@Slf4j
@Getter
@NoArgsConstructor(force = true)
@EqualsAndHashCode
public class BasicCredentialMetaData implements CredentialMetaData, Serializable {

    /**
     * Serialization version marker.
     */
    private static final long serialVersionUID = 4929579849241505377L;

    /**
     * Credential type unique identifier.
     * 凭证类型唯一标识符
     */
    private final String id;

    /**
     * Type of original credential.
     * 原始凭证的类型。
     */
    private final Class<? extends Credential> credentialClass;

    /**
     * Creates a new instance from the given credential.
     * 从给定凭据创建新实例。
     *
     * @param credential Credential for which metadata should be created.
     */
    public BasicCredentialMetaData(final Credential credential) {
        this.id = credential.getId();
        this.credentialClass = credential.getClass();
    }

}
