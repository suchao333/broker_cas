package org.apereo.cas.authentication.exceptions;

import lombok.extern.slf4j.Slf4j;
import javax.security.auth.login.CredentialExpiredException;
import lombok.NoArgsConstructor;

/**
 * Describes an authentication error condition where a user account's password must change before login.
 *描述在登录前必须更改用户帐户密码的身份验证错误情况。
 * @author Misagh Moayyed
 * @since 4.0.0
 */
@Slf4j
@NoArgsConstructor
public class AccountPasswordMustChangeException extends CredentialExpiredException {

    /** Serialization metadata. */
    private static final long serialVersionUID = 7487835035108753209L;

    /**
     * Instantiates a new account password must change exception.
     *实例化新帐户密码必须更改异常。
     * @param msg the msg
     */
    public AccountPasswordMustChangeException(final String msg) {
        super(msg);
    }
}
