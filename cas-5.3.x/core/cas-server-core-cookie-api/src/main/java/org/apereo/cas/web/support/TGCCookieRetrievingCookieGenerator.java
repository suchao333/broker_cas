package org.apereo.cas.web.support;

import lombok.extern.slf4j.Slf4j;

/**
 * Generates the tgc cookie.
 * Retrieving （找回，索取）
 * @author Misagh Moayyed
 * @since 4.2
 */
@Slf4j
public class TGCCookieRetrievingCookieGenerator extends CookieRetrievingCookieGenerator {

    /**
     * Instantiates a new TGC cookie retrieving cookie generator.
     *生成器将实例化检索新cookie的TGC。
     * @param casCookieValueManager the cookie manager
     * @param name                  cookie name
     * @param path                  cookie path
     * @param domain                cookie domain
     * @param rememberMeMaxAge      cookie rememberMe max age
     * @param secure                if cookie is only for HTTPS
     * @param maxAge                cookie max age
     * @param httpOnly              the http only
     */
    public TGCCookieRetrievingCookieGenerator(final CookieValueManager casCookieValueManager, final String name,
                                              final String path, final String domain,
                                              final int rememberMeMaxAge, final boolean secure,
                                              final int maxAge, final boolean httpOnly) {
        super(name, path, maxAge, secure, domain, casCookieValueManager, rememberMeMaxAge, httpOnly);
    }
}
