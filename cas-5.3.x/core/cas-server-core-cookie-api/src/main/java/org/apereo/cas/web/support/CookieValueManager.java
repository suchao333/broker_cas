package org.apereo.cas.web.support;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * The {@link CookieValueManager} is responsible for
 * managing all cookies and their value structure for CAS. Implementations
 * may choose to encode and sign the cookie value and optionally perform
 * additional checks to ensure the integrity of the cookie.
 **{@link CookieValueManager}负责
 *管理CA的所有Cookie及其价值结构。启动位置
  可以选择对cookie值进行编码和签名，也可以选择执行
 * 额外检查以确保cookie的完整性
 * @author Misagh Moayyed
 * @since 4.1
 */
public interface CookieValueManager {

    /**
     * Build cookie value.
     * 构架cookie value
     * @param givenCookieValue the given cookie value
     * @param request          the request
     * @return the original cookie value
     */
    String buildCookieValue(String givenCookieValue, HttpServletRequest request);

    /**
     * Obtain cookie value.
     * 获得cookie value
     * @param cookie  the cookie
     * @param request the request
     * @return the cookie value or null
     */
    default String obtainCookieValue(final Cookie cookie, final HttpServletRequest request) {
        return obtainCookieValue(cookie.getValue(), request);
    }

    /**
     * Obtain cookie value as string.
     * 获得cookie 值为string
     * @param cookie  the cookie
     * @param request the request
     * @return the string
     */
    String obtainCookieValue(String cookie, HttpServletRequest request);
}
