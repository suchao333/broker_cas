package org.apereo.cas.web.support;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * Default cookie value builder that simply returns the given cookie value
 * and does not perform any additional checks.
 * 默认的cookie值生成器，只返回给定的cookie值
 * 不执行任何附加检查。
 * @author Misagh Moayyed
 * @since 4.1
 */
@Slf4j
public class NoOpCookieValueManager implements CookieValueManager {

    @Override
    public String buildCookieValue(final String givenCookieValue, final HttpServletRequest request) {
        return givenCookieValue;
    }

    @Override
    public String obtainCookieValue(final String cookie, final HttpServletRequest request) {
        return cookie;
    }
}
