package org.apereo.cas.ticket;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * This is {@link DefaultTicketDefinitionProperties}.
 *
 * @author Misagh Moayyed
 * @since 5.1.0
 */
@Slf4j
@ToString
@Getter
@EqualsAndHashCode
@Setter
public class DefaultTicketDefinitionProperties implements TicketDefinitionProperties {

    /**
     * Whether ticket operations require cascading down in the storage.
     * 票证操作是否需要在存储中级联。
     */
    private boolean cascade;

    /**
     * Storage/cache name that holds this ticket.
     * 保存此票证的存储/缓存名称。
     */
    private String storageName;

    /**
     * Timeout for this ticket.
     * 此票证超时。
     */
    private long storageTimeout;

    /**
     * Password for this ticket storage, if any.
     * 此票证存储的密码（如果有）。
     */
    private String storagePassword;
}
