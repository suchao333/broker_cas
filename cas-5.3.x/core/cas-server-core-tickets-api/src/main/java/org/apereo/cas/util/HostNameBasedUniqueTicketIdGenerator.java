package org.apereo.cas.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apereo.cas.ticket.UniqueTicketIdGenerator;

/**
 * An implementation of {@link UniqueTicketIdGenerator} that is able auto-configure
 * the suffix based on the underlying host name.
 * <p>In order to assist with multi-node deployments, in scenarios where CAS configuration
 * and specially {@code application.properties} file is externalized, it would be ideal to simply just have one set
 * of configuration files for all nodes, such that there would for instance be one {@code application.properties} file
 * for all nodes. This would remove the need to copy/sync config files over across nodes, again in a
 * situation where they are externalized.
 * <p>The drawback is that in keeping only one {@code application.properties} file, we'd lose the ability
 * to define unique {@code host.name} property values for each node as the suffix, which would assist with troubleshooting
 * and diagnostics. To provide a remedy, this ticket generator is able to retrieve the {@code host.name} value directly from
 * the actual node name, rather than relying on the configuration, only if one isn't specified in
 * the {@code application.properties} file. </p>
 **{@link UniqueTicketIdGenerator}的一个实现，可以自动配置基于基础主机名的后缀。
 * *<p>为了协助多节点部署，在CAS配置的情况下特别是{@code application.properties}文件是外部化的，只需要一个集合就可以了
 *对于一个这样的配置，例如，会有一个文件{@code application.properties}
 * 对于所有节点。这将消除跨节点复制/同步配置文件的需要它们被外部化的情况。
 * 缺点是只保留一个{@code application.properties}档案，我们会失去能力的
 * 定义唯一的{@code host.name}属性值作为后缀，这将有助于故障排除
 * 和诊断。为了提供补救措施，这个票证生成器能够检索{@code host.name}直接从
 * 实际的节点名，而不是依赖于配置，只有在
 * {@code application.properties}文件。在</p>
 * @author Misagh Moayyed
 * @since 4.1.0
 */
@Slf4j
public class HostNameBasedUniqueTicketIdGenerator extends DefaultUniqueTicketIdGenerator {


    /**
     * Instantiates a new Host name based unique ticket id generator.
     * 实例化新的基于主机名的唯一票证id生成器。
     * @param maxLength the max length
     * @param suffix    the suffix
     */
    public HostNameBasedUniqueTicketIdGenerator(final int maxLength, final String suffix) {
        super(maxLength, determineTicketSuffixByHostName(suffix));
    }

    /**
     * Appends the first part of the host name to the ticket id,
     * so as to moderately provide a relevant unique value mapped to
     * the host name AND not auto-leak infrastructure data out into the configuration and logs.
     * <ul>
     * <li>If the CAS node name is {@code cas-01.sso.edu} then, the suffix
     * determined would just be {@code cas-01}</li>
     * <li>If the CAS node name is {@code cas-01} then, the suffix
     * determined would just be {@code cas-01}</li>
     * </ul>
     *将主机名的第一部分附加到票证id，
     *以便适度提供映射到的相关唯一值
     *主机名而不是自动将基础结构数据泄漏到配置和日志中。
     *<ul>
     *<li>如果CAS节点名是{@code CAS-01。cas-01.sso.edu}那么，后缀
     *确定的结果就是{@code cas-01}</li>
     *<li>如果CAS节点名是{@code CAS-01}，那么后缀
     *确定的结果就是{@code cas-01}</li>
     *</ul>
     * @param suffix the suffix
     * @return the shortened ticket suffix based on the hostname
     * @since 4.1.0
     */
    private static String determineTicketSuffixByHostName(final String suffix) {
        if (StringUtils.isNotBlank(suffix)) {
            return suffix;
        }
        return InetAddressUtils.getCasServerHostName();
    }

    /**
     * The type Ticket granting ticket id generator.
     * 类型票证授予票证id生成器。
     */
    public static class TicketGrantingTicketIdGenerator extends HostNameBasedUniqueTicketIdGenerator {

        /**
         * Instantiates a new Ticket granting ticket id generator.
         * 实例化新的票证授予票证id生成器。
         * @param maxLength the max length
         * @param suffix    the suffix
         */
        public TicketGrantingTicketIdGenerator(final int maxLength,
                                               final String suffix) {
            super(maxLength, suffix);
        }
    }

    /**
     * The type Service ticket id generator.
     * 类型服务票证id生成器。
     */
    public static class ServiceTicketIdGenerator extends HostNameBasedUniqueTicketIdGenerator {

        /**
         * Instantiates a new Service ticket id generator.
         * 实例化新的服务票证id生成器。
         *
         * @param maxLength the max length
         * @param suffix    the suffix
         */
        public ServiceTicketIdGenerator(final int maxLength,
                                        final String suffix) {
            super(maxLength, suffix);
        }
    }

    /**
     * The type Proxy ticket id generator.
     * 类型代理票证id生成器。
     */
    public static class ProxyTicketIdGenerator extends HostNameBasedUniqueTicketIdGenerator {
        /**
         * Instantiates a new Proxy ticket id generator.
         * 实例化新的代理票证id生成器。
         * @param maxLength the max length
         * @param suffix    the suffix
         */
        public ProxyTicketIdGenerator(final int maxLength,
                                      final String suffix) {
            super(maxLength, suffix);
        }
    }

}
