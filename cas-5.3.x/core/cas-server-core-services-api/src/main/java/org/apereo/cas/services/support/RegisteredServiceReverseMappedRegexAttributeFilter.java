package org.apereo.cas.services.support;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A filtering policy that selectively applies patterns to attributes mapped in the config.
 * If an attribute is mapped, it's only allowed to be released if it does not match the linked pattern.
 * If an attribute is not mapped, it may optionally be excluded from the released set of attributes.
 * 一种筛选策略，有选择地将模式应用于配置中映射的属性。
 * 如果一个属性被映射，只有当它与链接的模式不匹配时才允许释放它。
 * 如果某个属性未映射，则可以选择将其从已发布的属性集中排除。
 * @author Misagh Moayyed
 * @since 5.1.0
 */
@Slf4j
public class RegisteredServiceReverseMappedRegexAttributeFilter extends RegisteredServiceMappedRegexAttributeFilter {

    private static final long serialVersionUID = 852145306984610128L;



    @Override
    protected List<Object> filterAttributeValuesByPattern(final Set<Object> attributeValues, final Pattern pattern) {
        return attributeValues.stream()
                .filter(v -> {
                    LOGGER.debug("Matching attribute value [{}] against pattern [{}]", v, pattern.pattern());
                    final Matcher matcher = pattern.matcher(v.toString());
                    if (isCompleteMatch()) {
                        return !matcher.matches();
                    }
                    return !matcher.find();
                })
                .collect(Collectors.toList());
    }
}
