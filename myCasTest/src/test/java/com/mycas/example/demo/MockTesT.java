package com.mycas.example.demo;
import com.mycas.example.demo.Obj.Org;
import com.mycas.example.demo.mock.Person;
import com.mycas.example.demo.mock.PersonDao;
import com.mycas.example.demo.mock.PersonService;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MockTesT {

    private PersonDao personDao;

    private PersonService personService;

    @Before
    public  void setup(){
        personDao = mock(PersonDao.class);
        when(personDao.getPerson(1)).thenReturn(new Person(1,"person1"));
        //　isA（）：Object argument that implements the given class.
        //　eq（）：int argument that is equal to the given value
        when(personDao.update(isA(Person.class),"asd")).thenReturn(true);
        personService = new PersonService(personDao);

    }

}
