package com.mycas.example.demo;

import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TreeMapTest {
    public static void main(String[] args) {
        final TreeMap map = new TreeMap();
        map.put("test1", "newTest1");
        map.put("test2", Stream.of("newTest2", "DaTest2").collect(Collectors.toList()));

        System.out.println(map.containsKey("DaTest2"));
    }
}
