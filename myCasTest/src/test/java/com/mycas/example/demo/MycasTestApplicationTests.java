package com.mycas.example.demo;

import com.mycas.example.demo.Obj.Org;
import com.mycas.example.demo.Obj.OrgInt;
import com.mycas.example.demo.config.MyConfig;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@SpringBootTest(classes = {MyConfig.class
})
@RunWith(SpringRunner.class)
class MycasTestApplicationTests {

    @Autowired
    @Qualifier("myOrg")
    private OrgInt orgInt;

    @Test
    void contextLoads() {
        OrgInt test = mock(OrgInt.class);

        when(test.getName()).thenReturn("卧槽居然说话了");
        System.out.println(  test.getName());

        orgInt.say();
    }



}
