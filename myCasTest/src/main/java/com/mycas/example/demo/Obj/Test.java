package com.mycas.example.demo.Obj;

import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {
    public static void main(String[] args) {
        final Pattern pattern = Pattern.compile("::");
        final String usersProperty = "uid::password";
        Map<String, String> map= Stream.of(usersProperty.split(","))
                .map(pattern::split)
                .collect(Collectors.toMap(userAndPassword -> userAndPassword[0], userAndPassword -> userAndPassword[1]));

        System.out.println(map);
    }


}
