package com.mycas.example.demo;

import com.mycas.example.demo.Obj.OrgInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycasTestApplication {



    public static void main(String[] args) {
        SpringApplication.run(MycasTestApplication.class, args);

    }

}
