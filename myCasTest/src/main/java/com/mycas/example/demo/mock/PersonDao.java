package com.mycas.example.demo.mock;

public interface PersonDao {

    Person getPerson(int d);
    boolean update(Person person,String name);
}
