package com.mycas.example.demo.mock;

import lombok.AllArgsConstructor;

import lombok.Getter;

@Getter
@AllArgsConstructor
public class Person {

    private final int id;

    private final  String name;

}
