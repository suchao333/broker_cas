package com.mycas.example.demo.config;

import com.mycas.example.demo.Obj.FunTestHappy;
import com.mycas.example.demo.Obj.Org;
import com.mycas.example.demo.Obj.OrgInt;
import com.mycas.example.demo.Obj.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration("myConfig")
public class MyConfig {

    @Bean(name = "myOrg")
    public OrgInt myOrg(List<User> configurers){
        Org org = new Org();
        org.setOrgName("中软22");
        System.out.println("myOrg集合大小"+configurers.size());
        return org;
    }

    //@Autowired
    @ConditionalOnMissingBean(name = "myOrg")
    @Bean
    public OrgInt myOrg2(List<User> configurers){
        Org org = new Org();
        org.setOrgName("微软国际2");
        System.out.println("myOrg2集合大小"+configurers.size());

        return org;
    }

    @Bean
    public List<User>  getUser(){
        List<User> configurers = new ArrayList<>();
        configurers.add(new User());
        return configurers;
    }

    @Bean
    public FunTestHappy getHappy(){

        return  fun -> {
            fun.setOrgName("aa");
        };
    }




}
