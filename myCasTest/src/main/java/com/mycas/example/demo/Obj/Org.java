package com.mycas.example.demo.Obj;

import lombok.Data;

import java.util.List;

@Data
public class Org implements OrgInt {

    private String orgName;

    private List<User> userList;

    @Override
    public void say() {
        System.out.println("我的组织:"+this.orgName);
    }

    @Override
    public String getName() {
        return null;
    }
}
