package com.hugeo.cas;


import com.hugeo.cas.capcha.UsernamePasswordCaptchaCredential;
import org.apereo.cas.authentication.Credential;
import org.apereo.cas.authentication.HandlerResult;
import org.apereo.cas.authentication.PreventedException;
import org.apereo.cas.authentication.handler.support.AbstractPreAndPostProcessingAuthenticationHandler;
import org.apereo.cas.authentication.principal.PrincipalFactory;
import org.apereo.cas.services.ServicesManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.security.auth.login.FailedLoginException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

public class Login extends AbstractPreAndPostProcessingAuthenticationHandler {

    public Login(String name, ServicesManager servicesManager, PrincipalFactory principalFactory, Integer order) {
        super(name, servicesManager, principalFactory, order);
    }

    @Override
    protected HandlerResult doAuthentication(Credential credential) throws GeneralSecurityException, PreventedException {
        UsernamePasswordCaptchaCredential mycredential1 = (UsernamePasswordCaptchaCredential) credential;

        String capcha = mycredential1.getCapcha();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String right = attributes.getRequest().getSession().getAttribute("capcha").toString();
        if(!capcha.equals(right)){
            throw new FailedLoginException("验证码错误");
        }

        DriverManagerDataSource d=new DriverManagerDataSource();
        d.setDriverClassName("com.mysql.jdbc.Driver");
        d.setUrl("jdbc:mysql://127.0.0.1:3306/orange");
        d.setUsername("root");
        d.setPassword("123456");
        JdbcTemplate template=new JdbcTemplate();
        template.setDataSource(d);




        String username=mycredential1.getUsername();
        //查询数据库加密的的密码
        Map<String,Object> user = template.queryForMap("SELECT `password` FROM sys_user WHERE username = ?", mycredential1.getUsername());

        if(user==null){
            throw new FailedLoginException("没有该用户");
        }

        //返回多属性（暂时不知道怎么用，没研究）
        Map<String, Object> map=new HashMap<>();
        map.put("email", "3105747142@qq.com");

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if(encoder.matches(mycredential1.getPassword(),user.get("password").toString())){
            return createHandlerResult(mycredential1, principalFactory.createPrincipal(username, map), null);
        }
        throw new FailedLoginException("Sorry, login attemp failed.");
    }

    @Override
    public boolean supports(Credential credential) {
        return credential instanceof UsernamePasswordCaptchaCredential;
    }
}
