package com.hugeo.cas;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.hugeo.cas")
public class SpringConfig {
}
