package com.hugeo.cas;

import com.hugeo.cas.capcha.Capcha;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class RegController {

    @GetMapping("/reg")
    public String test(){
        return "register";
    }


    @GetMapping(value = "/capcha")
    public String capcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 设置响应的类型格式为图片格式
        response.setContentType("image/jpeg");
        //禁止图像缓存。
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        HttpSession session = request.getSession();

        Capcha capcha = new Capcha(120, 40, 5, 100);
        session.setAttribute("capcha", capcha.getCode());
        capcha.write(response.getOutputStream());
        return null;
    }
}
